﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Mash_graph_lab1
{
    public partial class Form1 : Form
    {
        // для графики
        BufferedGraphics buffer;    // класс буффера (чтобы рендерить)
        BufferedGraphicsContext buffer_context;     // класс для создания буффера
        Graphics current_g;
        Pen pen_for_result;
        Pen pen_for_usual;
        Brush brush_for_usual;
        Brush brush_for_text;
        Brush brush_for_selected_point;
        Brush brush_for_clear;
        Pen pen_for_o;
        Pen pen_for_v;
        Font font_for_text;
        Rectangle paint_rectangle;
        //private double scale_koef;
        private SizeF painting_area;
        private PointF begin_of_painting_area;
        private int index_of_selected_point;
        private List<PointF> main_list;
        float scale;// соотношение размера picturebox к реальному
        public Form1()
        {
            InitializeComponent();

        }

        // добавление точки
        private void button2_Click(object sender, EventArgs e)
        {
            float x,y;
            x = Convert.ToSingle(textBox1.Text.ToString());
            y = Convert.ToSingle(textBox2.Text.ToString());
            main_list.Add(new PointF(x,y));
            DGV.Rows.Add(DGV.Rows.Count, x, y);
            painting_area = ReSizePaintingArea ( main_list, out begin_of_painting_area );
            if ( main_list[main_list.Count-1].X < begin_of_painting_area.X ||
                main_list [main_list.Count-1].X > painting_area.Width + begin_of_painting_area.X ||
                main_list [main_list.Count-1].Y < begin_of_painting_area.Y ||
                main_list [main_list.Count-1].Y > painting_area.Height + begin_of_painting_area.Y )
                painting_area = ReSizePaintingArea ( main_list, out begin_of_painting_area );
            RePaint ( painting_area, begin_of_painting_area ); 
        }

        // корректировка точки
        private void button1_Click(object sender, EventArgs e)
        {
            if (DGV.CurrentRow.Index != -1)
            {
                main_list[DGV.CurrentRow.Index] = new PointF (Convert.ToSingle(textBox1.Text),Convert.ToSingle(textBox2.Text));
                DGV.CurrentRow.Cells[1].Value = main_list[DGV.CurrentRow.Index].X;
                DGV.CurrentRow.Cells[2].Value = main_list[DGV.CurrentRow.Index].Y;
                if (main_list[DGV.CurrentRow.Index].X<begin_of_painting_area.X ||
                    main_list[DGV.CurrentRow.Index].X>painting_area.Width+begin_of_painting_area.X ||
                    main_list[DGV.CurrentRow.Index].Y<begin_of_painting_area.Y ||
                    main_list[DGV.CurrentRow.Index].Y>painting_area.Height+begin_of_painting_area.Y)
                    painting_area = ReSizePaintingArea ( main_list, out begin_of_painting_area );
                RePaint ( painting_area, begin_of_painting_area ); 
            }
        }

        // удаление определенной точки
        private void button7_Click(object sender, EventArgs e)
        {
            if (DGV.CurrentRow.Index != -1)
            {
                if ( index_of_selected_point == DGV.CurrentRow.Index )
                    index_of_selected_point = -1;
                main_list.RemoveAt(DGV.CurrentRow.Index);
                DGV.Rows.RemoveAt(DGV.CurrentRow.Index);
            }
            RePaint ( painting_area, begin_of_painting_area ); 
        }

        //генерация точек
        private void button3_Click(object sender, EventArgs e)
        {
            DGV.Rows.Clear();
            main_list.Clear();
            index_of_selected_point = -1;
            float x, y;
            Random my_rand = new Random();
            for (int i = 0; i < 10; i++)
            {
                x = Convert.ToSingle(my_rand.NextDouble() + my_rand.Next(40)-20);
                y = Convert.ToSingle(my_rand.NextDouble() + my_rand.Next(40)-20);
                main_list.Add(new PointF(x, y));
                DGV.Rows.Add(main_list.Count - 1, x, y);
            }
            painting_area = ReSizePaintingArea (main_list, out begin_of_painting_area );
            RePaint ( painting_area, begin_of_painting_area ); 
        }

        // выход
        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        // клик в область таблицы
        private void DGV_Click(object sender, EventArgs e)
        {
            if (DGV.CurrentCell.Selected == true && DGV.CurrentRow.Index < main_list.Count)
            {
                textBox1.Text = DGV.CurrentRow.Cells[1].Value.ToString();
                textBox2.Text = DGV.CurrentRow.Cells[2].Value.ToString();
                index_of_selected_point = DGV.CurrentRow.Index;
                painting_area = ReSizePaintingArea ( main_list, out begin_of_painting_area );
                RePaint ( painting_area, begin_of_painting_area ); 
            }
        }

        #region реализация алгоритма

        // площадь треугольника
        private float Square_Triangle(PointF p1, PointF p2, PointF p3)
        {
            float S = ((p1.X - p3.X) * (p2.Y - p3.Y) - (p1.Y - p3.Y) * (p2.X - p3.X));
            S = Math.Abs(S) / 2;
            return S;
        }

        // перевод точки из реальной системы координат в систему координат на форме
        public void Transform_PointF ( ref PointF p )
        {
            p.X = ( p.X - begin_of_painting_area.X ) * scale;
            p.Y = paint_rectangle.Height - ( p.Y - begin_of_painting_area.Y ) * scale;
        }

        // длина линии
        private float LineLenght(PointF p1, PointF p2)
        {
            return Convert.ToSingle( Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2)));
        }

        public bool IsPointInTriangle(PointF p3, PointF[] pl)
        {
            float r1,r2,r3;
            r1 = (pl[0].X-p3.X)*(pl[1].Y-pl[0].Y)-(pl[1].X-pl[0].X)*(pl[0].Y-p3.Y);
            r2 = (pl[1].X-p3.X)*(pl[2].Y-pl[1].Y)-(pl[2].X-pl[1].X)*(pl[1].Y-p3.Y);
            r3 = (pl[2].X-p3.X)*(pl[0].Y-pl[2].Y)-(pl[0].X-pl[2].X)*(pl[2].Y-p3.Y);
            if ((r1>0 && r2>0 && r3>0)||(r1<=0 && r2<=0 && r3<=0))
                return true;
            return false;
        }

        // главная задача!!!
        private void button5_Click(object sender, EventArgs e)
        {
            PointF center_o, center_v;
            float r_v, r_o, min_delta=float.PositiveInfinity, S=0, a, b, c;// радиус вписанной и описанной, максимальный и минимальный, площадь треугольника
            int [] index = new int [3];
            #region Нахождение результирующего треугольника
            PointF[] result = new PointF[3];
            index[0] = -1;
            for (int i = 0; i < main_list.Count; i++)
            {
                for (int j = 0; j < main_list.Count; j++)
                {
                    if (j == i)
                        continue;
                    for (int k = 0; k < main_list.Count; k++)
                    {
                        if (k == j || k == i)
                            continue;
                        a = LineLenght(main_list[i], main_list[j]);
                        b = LineLenght(main_list[j], main_list[k]);
                        c = LineLenght(main_list[k], main_list[i]);
                        S = Square_Triangle(main_list[i], main_list[j], main_list[k]);
                        if (S != 0)
                        {
                            r_v = 2 * S / (a + b + c);
                            r_o = a * b * c / 4 / S;
                            if (r_o - r_v < min_delta)
                            {
                                index [0] = i;
                                index [1] = j;
                                index [2] = k;
                                min_delta = r_o-r_v;
                            }
                        }
                    } 
                }
            }
            #endregion
            
            if ( index [0] == -1)
            {
                if (main_list.Count<3)
                    MessageBox.Show(" Мало точек !!!");
                else
                    MessageBox.Show("Точки находятся на одной прямой !!!");
            }
            else
            {
                result [0] = main_list [index [0]];
                result [1] = main_list [index [1]];
                result [2] = main_list [index [2]];
                float buf_angle1, buf_angle2, buf_angle3;
                MyLine l1 = new MyLine(result[0], result[1]);
                MyLine l2 = new MyLine(result[1], result[2]);
                MyLine l3 = new MyLine(result[2], result[0]);
                a = LineLenght ( result [0], result [1] );
                b = LineLenght ( result [1], result [2] );
                c = LineLenght ( result [2], result [0] );
                // строим два серединных перпендикуляра. точка их пересечения есть центр описанной окружности
                l1.Rotation(90, new PointF((result[0].X+result[1].X)/2, (result[0].Y+result[1].Y)/2), result[0]);
                l2.Rotation(90, new PointF((result[1].X + result[2].X) / 2, (result[1].Y + result[2].Y) / 2), result[1]);
                center_o =  MyLine.Intersection(l1, l2);
                // находим центр вписанной окружности (точка пересечения биссектрис)
                l1 = new MyLine(result[0], result[1]);
                l2 = new MyLine(result[1], result[2]);
                float angle1, angle2, angle3;
                angle1 = MyLine.AngleBetwenTwoLine(l1, l2);
                angle2 = MyLine.AngleBetwenTwoLine(l2, l3);
                angle3 = MyLine.AngleBetwenTwoLine ( l3, l1 );
                // проверка на тупость угла
                buf_angle1 = (float) ( Math.Acos ( Convert.ToSingle ( ( b * b + a * a - c * c ) / 2 / b / a ) ) / Math.PI * 180 );
                buf_angle2 = (float) ( Math.Acos ( Convert.ToSingle ( ( c * c + b * b - a * a ) / 2 / b / c ) ) / Math.PI * 180 );
                buf_angle3 = (float) ( Math.Acos ( Convert.ToSingle ( ( c * c + a * a - b * b ) / 2 / a / c ) ) / Math.PI * 180 );

                l1.Rotation(-angle1 / 2, result[1], result[0]);
                l2.Rotation(-angle2 / 2, result[2], result[1]);
                l3.Rotation ( -angle3 / 2, result [0], result [2] );
                center_v = MyLine.Intersection(l1, l2);
                if ( IsPointInTriangle ( MyLine.Intersection ( l1, l3 ), result ) )
                    center_v = MyLine.Intersection ( l1, l3 );
                else
                if ( IsPointInTriangle ( MyLine.Intersection ( l2, l3 ), result ) )
                    center_v = MyLine.Intersection ( l2, l3 );
                S = Square_Triangle(result[0], result[1], result[2]);
                

                r_v = 2 * S / (a + b + c);
                r_o = a * b * c / 4 / S;


                List<PointF> buf_p_list = new List<PointF>();
                buf_p_list.Add(new PointF(center_o.X-r_o, center_o.Y));
                buf_p_list.Add(new PointF(center_o.X+r_o, center_o.Y));
                buf_p_list.Add(new PointF(center_o.X, center_o.Y-r_o));
                buf_p_list.Add(new PointF(center_o.X, center_o.Y+r_o));
                painting_area = ReSizePaintingArea ( buf_p_list, out begin_of_painting_area );
                RePaint ( painting_area, begin_of_painting_area );
                label4.Text = "Результат:" + " Искомый треугольник образуют 3 точки  " + "\n" + result [0].ToString ( ) + "\n" + result [1].ToString ( ) + "\n" + result [2].ToString ( ) + "\n";
                label4.Text += "Описанная окружность, центр имеет координаты:" + center_o.ToString ( ) + ", радиус: " + r_o + "\n";
                label4.Text += "Вписанная окружность, центр имеет координаты:" + center_v.ToString() + ", радиус: " + r_v;
                for (int i=0; i<3; i++)
                {
                    Transform_PointF ( ref result [i] );
                    buffer.Graphics.DrawString ( "<" +index [i].ToString ( ) + ">", font_for_text, brush_for_text, result [i] );
                }
                
                buffer.Graphics.DrawLine ( pen_for_result, result [0], result [1] );
                buffer.Graphics.DrawLine ( pen_for_result, result [1], result [2] );
                buffer.Graphics.DrawLine ( pen_for_result, result [2], result [0] );
                Transform_PointF ( ref center_o );
                Transform_PointF ( ref center_v );
                r_o *= scale;
                r_v *= scale;
                buffer.Graphics.DrawEllipse ( pen_for_o, center_o.X - r_o , center_o.Y - r_o , 2*r_o, 2*r_o );
                buffer.Graphics.DrawEllipse ( pen_for_v, center_v.X - r_v , center_v.Y - r_v , 2*r_v, 2*r_v );
                buffer.Render ( current_g );

            }
        }
        #endregion


        public void RePaint(SizeF painting_area, PointF begin_of_area)
        {
            PointF buf_p = new PointF ( );
            buffer.Graphics.FillRectangle ( brush_for_clear, paint_rectangle );
            buffer.Graphics.DrawRectangle ( pen_for_usual, paint_rectangle );
            if ( main_list.Count > 0 )
            {
                scale = paint_rectangle.Height / painting_area.Height;
                var points_in_area = from n in main_list
                                     where ( n.X > begin_of_area.X && n.X < painting_area.Width + begin_of_area.X &&
                                     n.Y > begin_of_area.Y && n.Y < painting_area.Height + begin_of_area.Y )
                                     select n;
                // заливка всех точек
                foreach ( PointF p in points_in_area )
                {
                    buf_p.X =  ( p.X - begin_of_area.X ) * scale;
                //    if ( p.X < 0 )
                 //       buf_p.X = paint_rectangle.Width - buf_p.X;
                    buf_p.Y = paint_rectangle.Height - ( p.Y - begin_of_area.Y ) * scale;
                //    if ( p.Y > 0 )
                //        buf_p.Y = paint_rectangle.Height - buf_p.Y;
                    buffer.Graphics.DrawEllipse ( pen_for_usual, buf_p.X - 2, buf_p.Y - 2, 4, 4 );
                    buffer.Graphics.FillEllipse ( brush_for_usual, buf_p.X - 2, buf_p.Y - 2 , 4, 4 );

                }
                // заливка выбранной точки
                if (index_of_selected_point!=-1 && points_in_area.Contains(main_list[index_of_selected_point]))
                    buffer.Graphics.FillEllipse ( brush_for_selected_point, ( main_list [index_of_selected_point].X - begin_of_area.X ) * scale - 2,
                                    paint_rectangle.Height - ( main_list [index_of_selected_point].Y - begin_of_area.Y ) * scale - 2, 4, 4 );
            }
            buffer.Render(current_g);
        }

        // на вход подаем список точек. на выходе размер области этих точек и начало координат новой системы
        public SizeF ReSizePaintingArea ( List<PointF> pl , out PointF begin_of_area)
        {
            SizeF res = new SizeF ( );
            float max_x=pl[0].X;
            float min_x=pl[0].X;
            float max_y=pl[0].Y;
            float min_y=pl[0].Y;
            for ( int i = 0; i < pl.Count; i++ )
            {
                if ( pl [i].X > max_x )
                    max_x = pl [i].X;
                if ( pl [i].X < min_x )
                    min_x = pl [i].X;
                if ( pl [i].Y > max_y )
                    max_y = pl [i].Y;
                if ( pl [i].Y < min_y )
                    min_y = pl [i].Y;
            }
            
            if (max_x<0)
                max_x *= (float) 0.8;
            else
                max_x *= (float) 1.2;
            if ( min_x < 0 )
                min_x *= (float) 1.2;
            else
                min_x *= (float) 0.8;
            if ( max_y > 0 )
                max_y *= (float) 1.2;
            else
                max_y *= (float) 0.8;
            if ( min_y > 0 )
                min_y *= (float) 0.8;
            else
                min_y *= (float) 1.2;
            res.Height = Math.Abs (max_y - min_y);
            res.Width = Math.Abs ( max_x - min_x );
            if ( res.Width > res.Height )
                res.Height = res.Width;
            else
                res.Width = res.Height;
            begin_of_area = new PointF ( min_x, min_y );
            return res;
        }

        private void Form1_Load ( object sender, EventArgs e )
        {
            main_list = new List<PointF>();
            //scale_koef = 1;
            DGV.RowCount = 1;
            index_of_selected_point = -1;
            
            paint_rectangle = pictureBox1.ClientRectangle;
            //
            current_g = pictureBox1.CreateGraphics ( );
            buffer_context = new BufferedGraphicsContext ( );
            buffer = buffer_context.Allocate ( current_g, paint_rectangle );
            font_for_text = new Font ( ( "Microsoft Sans Serif" ), 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 204 ) ) );
            //
            brush_for_text = new SolidBrush ( Color.Black );
            brush_for_usual = new SolidBrush ( Color.Green );
            brush_for_selected_point = new SolidBrush ( Color.Red ); 
            brush_for_clear = new SolidBrush ( Color.White );
            //
            pen_for_usual =  new Pen ( Color.Black, 1 );
            pen_for_o = new Pen ( Color.Coral, 1 );
            pen_for_v = new Pen ( Color.Brown, 1 );
            pen_for_result = new Pen ( Color.Blue );
            //
         //   button3_Click ( button3, new EventArgs ( ) );
        }

        // удалить все точки
        private void button6_Click(object sender, EventArgs e)
        {
            main_list.Clear();
            DGV.Rows.Clear();
            index_of_selected_point = -1;
            RePaint ( painting_area, begin_of_painting_area ); 
        }

        private void button8_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Дано множество точек. Найти треугольник с минимальной разностью площадей вписанной и описанной окружностей.");
        }

        private void pictureBox1_MouseMove ( object sender, MouseEventArgs e )
        {
            float x=e.X/scale+begin_of_painting_area.X;
            float y=(paint_rectangle.Height - e.Y)/scale+begin_of_painting_area.Y;
            label3.Text = "Курсор: <" + x + "," + y + ">";
        }

        private void pictureBox1_MouseDown ( object sender, MouseEventArgs e )
        {
            label4.Text = "";
            painting_area = ReSizePaintingArea ( main_list, out begin_of_painting_area );
            RePaint ( painting_area, begin_of_painting_area ); 
        }


    }
}
