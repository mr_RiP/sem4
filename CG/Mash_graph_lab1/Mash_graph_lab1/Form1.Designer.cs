﻿namespace Mash_graph_lab1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox ( );
            this.groupBox1 = new System.Windows.Forms.GroupBox ( );
            this.label3 = new System.Windows.Forms.Label ( );
            this.button7 = new System.Windows.Forms.Button ( );
            this.button2 = new System.Windows.Forms.Button ( );
            this.button1 = new System.Windows.Forms.Button ( );
            this.label2 = new System.Windows.Forms.Label ( );
            this.textBox2 = new System.Windows.Forms.TextBox ( );
            this.label1 = new System.Windows.Forms.Label ( );
            this.textBox1 = new System.Windows.Forms.TextBox ( );
            this.DGV = new System.Windows.Forms.DataGridView ( );
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn ( );
            this.X = new System.Windows.Forms.DataGridViewTextBoxColumn ( );
            this.Y = new System.Windows.Forms.DataGridViewTextBoxColumn ( );
            this.button3 = new System.Windows.Forms.Button ( );
            this.button4 = new System.Windows.Forms.Button ( );
            this.button5 = new System.Windows.Forms.Button ( );
            this.button6 = new System.Windows.Forms.Button ( );
            this.button8 = new System.Windows.Forms.Button ( );
            this.label4 = new System.Windows.Forms.Label ( );
            ( (System.ComponentModel.ISupportInitialize) ( this.pictureBox1 ) ).BeginInit ( );
            this.groupBox1.SuspendLayout ( );
            ( (System.ComponentModel.ISupportInitialize) ( this.DGV ) ).BeginInit ( );
            this.SuspendLayout ( );
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point ( 0, 0 );
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size ( 600, 600 );
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler ( this.pictureBox1_MouseDown );
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler ( this.pictureBox1_MouseMove );
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add ( this.label3 );
            this.groupBox1.Controls.Add ( this.button7 );
            this.groupBox1.Controls.Add ( this.button2 );
            this.groupBox1.Controls.Add ( this.button1 );
            this.groupBox1.Controls.Add ( this.label2 );
            this.groupBox1.Controls.Add ( this.textBox2 );
            this.groupBox1.Controls.Add ( this.label1 );
            this.groupBox1.Controls.Add ( this.textBox1 );
            this.groupBox1.Location = new System.Drawing.Point ( 606, 12 );
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size ( 173, 168 );
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Добавление";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point ( 6, 135 );
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size ( 46, 13 );
            this.label3.TabIndex = 8;
            this.label3.Text = "Курсор:";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point ( 6, 97 );
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size ( 75, 23 );
            this.button7.TabIndex = 7;
            this.button7.Text = "Удалить";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler ( this.button7_Click );
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point ( 6, 68 );
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size ( 75, 23 );
            this.button2.TabIndex = 6;
            this.button2.Text = "Добавить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler ( this.button2_Click );
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point ( 87, 68 );
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size ( 75, 23 );
            this.button1.TabIndex = 0;
            this.button1.Text = "Изменить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler ( this.button1_Click );
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point ( 6, 45 );
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size ( 17, 13 );
            this.label2.TabIndex = 5;
            this.label2.Text = "Y:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point ( 29, 42 );
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size ( 133, 20 );
            this.textBox2.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point ( 6, 19 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size ( 17, 13 );
            this.label1.TabIndex = 3;
            this.label1.Text = "X:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point ( 29, 16 );
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size ( 133, 20 );
            this.textBox1.TabIndex = 2;
            // 
            // DGV
            // 
            this.DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DGV.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV.Columns.AddRange ( new System.Windows.Forms.DataGridViewColumn [] {
            this.Number,
            this.X,
            this.Y} );
            this.DGV.Location = new System.Drawing.Point ( 606, 186 );
            this.DGV.MultiSelect = false;
            this.DGV.Name = "DGV";
            this.DGV.RowHeadersVisible = false;
            this.DGV.Size = new System.Drawing.Size ( 162, 299 );
            this.DGV.TabIndex = 2;
            this.DGV.Click += new System.EventHandler ( this.DGV_Click );
            // 
            // Number
            // 
            this.Number.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Number.HeaderText = "№";
            this.Number.Name = "Number";
            this.Number.ReadOnly = true;
            this.Number.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Number.Width = 43;
            // 
            // X
            // 
            this.X.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.X.HeaderText = "X";
            this.X.Name = "X";
            this.X.ReadOnly = true;
            this.X.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.X.Width = 39;
            // 
            // Y
            // 
            this.Y.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Y.HeaderText = "Y";
            this.Y.Name = "Y";
            this.Y.ReadOnly = true;
            this.Y.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Y.Width = 39;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point ( 606, 491 );
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size ( 164, 23 );
            this.button3.TabIndex = 3;
            this.button3.Text = "Генерация точек";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler ( this.button3_Click );
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point ( 606, 607 );
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size ( 164, 23 );
            this.button4.TabIndex = 4;
            this.button4.Text = "Выход";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler ( this.button4_Click );
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point ( 606, 578 );
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size ( 164, 23 );
            this.button5.TabIndex = 5;
            this.button5.Text = "Решить задачу";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler ( this.button5_Click );
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point ( 606, 520 );
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size ( 164, 23 );
            this.button6.TabIndex = 6;
            this.button6.Text = "Удалить все точки";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler ( this.button6_Click );
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point ( 606, 549 );
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size ( 164, 23 );
            this.button8.TabIndex = 7;
            this.button8.Text = "Условие задачи";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler ( this.button8_Click );
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point ( 12, 607 );
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size ( 0, 13 );
            this.label4.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF ( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size ( 782, 701 );
            this.Controls.Add ( this.label4 );
            this.Controls.Add ( this.button8 );
            this.Controls.Add ( this.button6 );
            this.Controls.Add ( this.button5 );
            this.Controls.Add ( this.button4 );
            this.Controls.Add ( this.button3 );
            this.Controls.Add ( this.DGV );
            this.Controls.Add ( this.groupBox1 );
            this.Controls.Add ( this.pictureBox1 );
            this.Name = "Form1";
            this.Text = "Лаб. раб. 1 Треугольник с наибольшей разностью площадей вписанной и описанной окр" +
                "ужностей";
            this.Load += new System.EventHandler ( this.Form1_Load );
            ( (System.ComponentModel.ISupportInitialize) ( this.pictureBox1 ) ).EndInit ( );
            this.groupBox1.ResumeLayout ( false );
            this.groupBox1.PerformLayout ( );
            ( (System.ComponentModel.ISupportInitialize) ( this.DGV ) ).EndInit ( );
            this.ResumeLayout ( false );
            this.PerformLayout ( );

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView DGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn X;
        private System.Windows.Forms.DataGridViewTextBoxColumn Y;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

