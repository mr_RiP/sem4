﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Mash_graph_lab3
{
    public partial class Form1 : Form
    {
        Graphics currentGraphics;
        Brush brushForUsual;
        BufferedGraphics buffer;    // класс буффера (чтобы рендерить)
        BufferedGraphicsContext bufferContext;     // класс для создания буффера
        Pen penForUsual;
        Brush brushForClear;

        public Form1()
        {
            InitializeComponent();
            currentGraphics = pictureBox1.CreateGraphics();
            bufferContext = new BufferedGraphicsContext();
            buffer = bufferContext.Allocate(currentGraphics, pictureBox1.ClientRectangle);
            brushForUsual = new SolidBrush(Color.Black);
            brushForClear = new SolidBrush(Color.White);
            penForUsual = new Pen(Color.Black);
            buffer.Graphics.FillRectangle(brushForClear, 0, 0, 900, 550);
            buffer.Render(currentGraphics);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = button1.BackColor;
            colorDialog1.ShowDialog();
            penForUsual.Color = colorDialog1.Color;
            (brushForUsual as SolidBrush).Color = colorDialog1.Color;
            button1.ForeColor = colorDialog1.Color;
            button1.BackColor = colorDialog1.Color;
            button1.FlatAppearance.MouseOverBackColor = colorDialog1.Color;
            button1.FlatAppearance.MouseDownBackColor = colorDialog1.Color;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            buffer.Graphics.FillRectangle(brushForClear, pictureBox1.ClientRectangle);
            buffer.Render(currentGraphics);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Реализовать алгоритмы растрового построения прямой. Размер прямоугольника 900*550 пикселей. OY вниз. Начало координат в (0,0)");
        }

        private int CDA(Point p1, Point p2)
        {
            int numSteps = 0;
            Stopwatch timer = new Stopwatch();
            timer.Start();
            double dx = (p2.X - p1.X), dy = (p2.Y - p1.Y);
            int l;
            double xt = p1.X, yt = p1.Y;
            if (Math.Abs(dx) > Math.Abs(dy))
            {
                numSteps = (int)Math.Abs(dy) + 1;
                l = (int)Math.Abs(dx);
            }
            else
            {
                numSteps = (int)Math.Abs(dx) + 1;
                l = (int)Math.Abs(dy);
            }
            dx /= l;
            dy /= l;
            for (int i = 0; i < l + 1; i++)
            {
                timer.Stop();
                buffer.Graphics.FillRectangle(brushForUsual, (int)xt, (int)yt, 1, 1);
                timer.Start();
                xt += dx;
                yt += dy;
            }
            timer.Stop();
            //MessageBox.Show(timer.ElapsedTicks.ToString());
            buffer.Render(currentGraphics);
            return (int)timer.ElapsedTicks;
        }

        private int BrezenheimSmoothing(Point p1, Point p2)
        {
            int numSteps = 1;
            Color currentColor = (brushForUsual as SolidBrush).Color;
            Stopwatch timer = new Stopwatch();
            timer.Start();
            int dx = (p2.X - p1.X), dy = (p2.Y - p1.Y);
            int sx = Math.Sign(dx);
            int sy = Math.Sign(dy);
            int xt = p1.X, yt = p1.Y;
            dx = Math.Abs(dx);
            dy = Math.Abs(dy);
            bool swap = false;
            if (dx <= dy)
            {
                swap = true;
                int t = dx;
                dx = dy;
                dy = t;
            }
            int Intens = 256;
            int e = Intens * dx;
            int w = Intens * (dx - dy) * 2;

            for (int i = 1; i <= dx + 1; i++)
            {
                timer.Stop();
                brushForUsual = new SolidBrush(Color.FromArgb((e / (2 * dx)), currentColor));
                buffer.Graphics.FillRectangle(brushForUsual, xt, yt, 1, 1);
                timer.Start();
                if (e >= w)
                {
                    yt += sy;
                    xt += sx;
                    e -= w;
                    numSteps++;
                }
                else
                {
                    if (swap == false)
                        xt += sx;
                    else
                        yt += sy;
                    e += dy * 2 * Intens;
                }
            }
            timer.Stop();
            //MessageBox.Show(timer.ElapsedTicks.ToString());
            buffer.Render(currentGraphics);
            brushForUsual = new SolidBrush(currentColor);
            return (int)timer.ElapsedTicks;
        }

        private int BrezenheimFloat(Point p1, Point p2)
        {
            int numSteps = 1;
            Stopwatch timer = new Stopwatch();
            timer.Start();
            int dx = (p2.X - p1.X), dy = (p2.Y - p1.Y);
            int sx = Math.Sign(dx);
            int sy = Math.Sign(dy);
            int xt = p1.X, yt = p1.Y;
            dx = Math.Abs(dx);
            dy = Math.Abs(dy);
            bool swap = false;
            if (dx <= dy)
            {
                swap = true;
                int t = dx;
                dx = dy;
                dy = t;
            }
            double m = (dy+0.0) / dx;
            double e = m - 0.5;
            for (int i = 1; i <= dx + 1; i++)
            {
                timer.Stop();
                buffer.Graphics.FillRectangle(brushForUsual, xt, yt, 1, 1);
                timer.Start();
                if (e >= 0)
                {
                    if (swap == false)
                        yt += sy;
                    else
                        xt += sx;
                    e--;
                    numSteps++;
                }
                if (swap == false)
                    xt += sx;
                else
                    yt += sy;
                e += m;
            }
            timer.Stop();
            //MessageBox.Show(timer.ElapsedTicks.ToString());
            buffer.Render(currentGraphics);
            return (int)timer.ElapsedTicks;
        }

        private int BrezenheimInt(Point p1, Point p2)
        {
            int numSteps = 1;
            Stopwatch timer = new Stopwatch();
            timer.Start();
            int dx = (p2.X - p1.X), dy = (p2.Y - p1.Y);
            int sx = Math.Sign(dx);
            int sy = Math.Sign(dy);
            int xt = p1.X, yt = p1.Y;
            dx = Math.Abs(dx);
            dy = Math.Abs(dy);
            bool swap = false;
            if (dx <= dy)
            {
                swap = true;
                int t = dx;
                dx = dy;
                dy = t;
            }
            int e = 2 * dy - dx;
            for (int i = 1; i <= dx + 1; i++)
            {
                timer.Stop();
                buffer.Graphics.FillRectangle(brushForUsual, xt, yt, 1, 1);
                timer.Start();
                if (e >= 0)
                {
                    if (swap == false)
                        yt += sy;
                    else
                        xt += sx;
                    e -= 2 * dx;
                    numSteps++;
                }
                if (swap == false)
                    xt += sx;
                else
                    yt += sy;
                e += 2 * dy;
            }
            timer.Stop();
            //MessageBox.Show(timer.ElapsedTicks.ToString());
            buffer.Render(currentGraphics);
            return (int)timer.ElapsedTicks;
        }

        delegate int DrawingAlgorithm(Point p1, Point p2);
        private void button3_Click(object sender, EventArgs e)
        {
            Point p1, p2;
            p1 = new Point((int)numericUpDown1.Value, (int)numericUpDown3.Value);
            p2 = new Point((int)numericUpDown4.Value, (int)numericUpDown2.Value);
            if (p1 != p2)
            {
                switch (tabControl1.SelectedIndex)
                {
                    case 0:
                        switch (ComboBox1.SelectedIndex)
                        {
                            case 0:
                                CDA(p1, p2);
                                break;
                            case 1:
                                BrezenheimFloat(p1, p2);
                                break;
                            case 2:
                                BrezenheimInt(p1, p2);
                                break;
                            case 3:
                                BrezenheimSmoothing(p1, p2);
                                break;
                            case 4:
                                buffer.Graphics.DrawLine(penForUsual, p1, p2);
                                buffer.Render(currentGraphics);
                                break;
                        }
                        break;
                    case 1:
                        int numOfLines = (int)numericUpDown5.Value;
                        double angle = 360.0 / numOfLines, currentAngle = 0;
                        p1 = new Point(450, 275);
                        Point[] points = new Point[numOfLines];
                        int radius = 200;
                        for (int i = 0; i < numOfLines; i++)
                        {
                            points[i].X = p1.X + (int)(radius * Math.Cos(currentAngle * Math.PI / 180));
                            points[i].Y = p1.Y + (int)(radius * Math.Sin(currentAngle * Math.PI / 180));
                            currentAngle += angle;
                        }
                        DrawingAlgorithm func = null;
                        switch (ComboBox1.SelectedIndex)
                        {
                            case 0:
                                func = CDA;
                                break;
                            case 1:
                                func = BrezenheimFloat;
                                break;
                            case 2:
                                func = BrezenheimInt;
                                break;
                            case 3:
                                func = BrezenheimSmoothing;
                                break;
                            case 4:
                                for (int i = 0; i < numOfLines; i++)
                                {
                                    buffer.Graphics.DrawLine(penForUsual, p1, points[i]);
                                }
                                buffer.Render(currentGraphics);
                                break;
                        }
                        if (func != null)
                        {
                            for (int i = 0; i < numOfLines; i++)
                            {
                                func(p1, points[i]);
                            }
                        }
                        break;
                }
            }
            else
                MessageBox.Show("Совпадение точек!");
          
            
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            colorDialog1.Color = button2.BackColor;
            colorDialog1.ShowDialog();
            
            (brushForClear as SolidBrush).Color = colorDialog1.Color;
            button2.ForeColor = colorDialog1.Color;
            button2.BackColor = colorDialog1.Color;
            button2.FlatAppearance.MouseOverBackColor = colorDialog1.Color;
            button2.FlatAppearance.MouseDownBackColor = colorDialog1.Color;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Point p1, p2;
            p1= new Point(300, 300);
            p2= new Point(500, 300);
            StreamWriter writer = new StreamWriter("1.txt");
            int i = 0;
            while (i<=90)
            {
                p2.X = p1.X + (int)(200 * Math.Cos(i * Math.PI / 180));
                p2.Y = p1.Y + (int)(200 * Math.Sin(i * Math.PI / 180));
                switch (ComboBox1.SelectedIndex)
                {
                    case 0:
                        writer.Write(CDA(p1, p2));
                        break;
                    case 1:
                        writer.Write(BrezenheimFloat(p1, p2));
                        break;
                    case 2:
                        writer.Write(BrezenheimInt(p1, p2));
                        break;
                    case 3:
                        writer.Write(BrezenheimSmoothing(p1, p2));
                        break;
                }
                writer.WriteLine(" ");
                i++;
            }
            writer.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            {
                Point p1, p2;
                p1 = new Point(300, 300);
                p2 = new Point(500, 500);
                int i = 0;
                int middleArifm = 0;
                while (i <= 1000)
                {
                    switch (ComboBox1.SelectedIndex)
                    {
                        case 0:
                            middleArifm+=(CDA(p1, p2));
                            break;
                        case 1:
                            middleArifm+=(BrezenheimFloat(p1, p2));
                            break;
                        case 2:
                            middleArifm+=(BrezenheimInt(p1, p2));
                            break;
                        case 3:
                            middleArifm+=(BrezenheimSmoothing(p1, p2));
                            break;
                    }
                    i++;
                }
                middleArifm /= 1000;
                MessageBox.Show(middleArifm.ToString ());
            }
        }
    }
}
