﻿namespace Mash_graph_lab8
{
    partial class mainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddLine = new System.Windows.Forms.Button();
            this.xLabel = new System.Windows.Forms.Label();
            this.x1NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.lbxCutter = new System.Windows.Forms.ListBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnCut = new System.Windows.Forms.Button();
            this.gpbxFigure = new System.Windows.Forms.GroupBox();
            this.rbtnCutter = new System.Windows.Forms.RadioButton();
            this.rbtnPolygons = new System.Windows.Forms.RadioButton();
            this.canvasPictureBox = new System.Windows.Forms.PictureBox();
            this.yLabel = new System.Windows.Forms.Label();
            this.y1NumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.x1NumericUpDown)).BeginInit();
            this.gpbxFigure.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.y1NumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddLine
            // 
            this.btnAddLine.Location = new System.Drawing.Point(722, 189);
            this.btnAddLine.Name = "btnAddLine";
            this.btnAddLine.Size = new System.Drawing.Size(161, 37);
            this.btnAddLine.TabIndex = 41;
            this.btnAddLine.Text = "Добавить точку в полигон";
            this.btnAddLine.UseVisualStyleBackColor = true;
            this.btnAddLine.Click += new System.EventHandler(this.btnAddLine_Click);
            // 
            // xLabel
            // 
            this.xLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xLabel.AutoSize = true;
            this.xLabel.Location = new System.Drawing.Point(722, 143);
            this.xLabel.Name = "xLabel";
            this.xLabel.Size = new System.Drawing.Size(17, 13);
            this.xLabel.TabIndex = 39;
            this.xLabel.Text = "X:";
            // 
            // x1NumericUpDown
            // 
            this.x1NumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.x1NumericUpDown.Location = new System.Drawing.Point(722, 159);
            this.x1NumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.x1NumericUpDown.Name = "x1NumericUpDown";
            this.x1NumericUpDown.Size = new System.Drawing.Size(72, 20);
            this.x1NumericUpDown.TabIndex = 38;
            this.x1NumericUpDown.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // lbxCutter
            // 
            this.lbxCutter.FormattingEnabled = true;
            this.lbxCutter.Location = new System.Drawing.Point(722, 232);
            this.lbxCutter.Name = "lbxCutter";
            this.lbxCutter.Size = new System.Drawing.Size(161, 147);
            this.lbxCutter.TabIndex = 37;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(722, 117);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(161, 23);
            this.btnClear.TabIndex = 33;
            this.btnClear.Text = "Очистить все";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnCut
            // 
            this.btnCut.Location = new System.Drawing.Point(723, 88);
            this.btnCut.Name = "btnCut";
            this.btnCut.Size = new System.Drawing.Size(160, 23);
            this.btnCut.TabIndex = 36;
            this.btnCut.Text = "Отсечь";
            this.btnCut.UseVisualStyleBackColor = true;
            this.btnCut.Click += new System.EventHandler(this.btnCut_Click);
            // 
            // gpbxFigure
            // 
            this.gpbxFigure.Controls.Add(this.rbtnCutter);
            this.gpbxFigure.Controls.Add(this.rbtnPolygons);
            this.gpbxFigure.Location = new System.Drawing.Point(723, 12);
            this.gpbxFigure.Name = "gpbxFigure";
            this.gpbxFigure.Size = new System.Drawing.Size(160, 70);
            this.gpbxFigure.TabIndex = 35;
            this.gpbxFigure.TabStop = false;
            this.gpbxFigure.Text = "Выбор фигуры";
            // 
            // rbtnCutter
            // 
            this.rbtnCutter.AutoSize = true;
            this.rbtnCutter.Location = new System.Drawing.Point(6, 42);
            this.rbtnCutter.Name = "rbtnCutter";
            this.rbtnCutter.Size = new System.Drawing.Size(85, 17);
            this.rbtnCutter.TabIndex = 1;
            this.rbtnCutter.TabStop = true;
            this.rbtnCutter.Text = "Отсекатель";
            this.rbtnCutter.UseVisualStyleBackColor = true;
            // 
            // rbtnPolygons
            // 
            this.rbtnPolygons.AutoSize = true;
            this.rbtnPolygons.Location = new System.Drawing.Point(6, 19);
            this.rbtnPolygons.Name = "rbtnPolygons";
            this.rbtnPolygons.Size = new System.Drawing.Size(76, 17);
            this.rbtnPolygons.TabIndex = 0;
            this.rbtnPolygons.TabStop = true;
            this.rbtnPolygons.Text = "Полигоны";
            this.rbtnPolygons.UseVisualStyleBackColor = true;
            // 
            // canvasPictureBox
            // 
            this.canvasPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.canvasPictureBox.BackColor = System.Drawing.Color.White;
            this.canvasPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.canvasPictureBox.Location = new System.Drawing.Point(2, 12);
            this.canvasPictureBox.Name = "canvasPictureBox";
            this.canvasPictureBox.Size = new System.Drawing.Size(714, 518);
            this.canvasPictureBox.TabIndex = 34;
            this.canvasPictureBox.TabStop = false;
            this.canvasPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvasPictureBox_MouseDown);
            // 
            // yLabel
            // 
            this.yLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.yLabel.AutoSize = true;
            this.yLabel.Location = new System.Drawing.Point(806, 143);
            this.yLabel.Name = "yLabel";
            this.yLabel.Size = new System.Drawing.Size(17, 13);
            this.yLabel.TabIndex = 43;
            this.yLabel.Text = "Y:";
            // 
            // y1NumericUpDown
            // 
            this.y1NumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.y1NumericUpDown.Location = new System.Drawing.Point(806, 159);
            this.y1NumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.y1NumericUpDown.Name = "y1NumericUpDown";
            this.y1NumericUpDown.Size = new System.Drawing.Size(72, 20);
            this.y1NumericUpDown.TabIndex = 42;
            this.y1NumericUpDown.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 542);
            this.Controls.Add(this.yLabel);
            this.Controls.Add(this.y1NumericUpDown);
            this.Controls.Add(this.btnAddLine);
            this.Controls.Add(this.xLabel);
            this.Controls.Add(this.x1NumericUpDown);
            this.Controls.Add(this.lbxCutter);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCut);
            this.Controls.Add(this.gpbxFigure);
            this.Controls.Add(this.canvasPictureBox);
            this.Name = "mainForm";
            this.Text = "Лаб8";
            ((System.ComponentModel.ISupportInitialize)(this.x1NumericUpDown)).EndInit();
            this.gpbxFigure.ResumeLayout(false);
            this.gpbxFigure.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.y1NumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddLine;
        private System.Windows.Forms.Label xLabel;
        private System.Windows.Forms.NumericUpDown x1NumericUpDown;
        private System.Windows.Forms.ListBox lbxCutter;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnCut;
        private System.Windows.Forms.GroupBox gpbxFigure;
        private System.Windows.Forms.RadioButton rbtnCutter;
        private System.Windows.Forms.RadioButton rbtnPolygons;
        private System.Windows.Forms.PictureBox canvasPictureBox;
        private System.Windows.Forms.Label yLabel;
        private System.Windows.Forms.NumericUpDown y1NumericUpDown;

    }
}

