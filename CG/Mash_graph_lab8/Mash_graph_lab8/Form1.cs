﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mash_graph_lab8
{
    public partial class mainForm : Form
    {
        BufferedGraphics buffer;    // класс буффера (чтобы рендерить)
        BufferedGraphicsContext bufferContext;     // класс для создания буффера
        Graphics currentGraphics;
        Brush brushForClear;
        Pen penForCutter;
        Pen penForPolygons;
        Pen penForSegments;
        List<Polygon> polygons;
        Polygon cutter;

        public mainForm()
        {
            InitializeComponent();
            rbtnCutter.Checked = true;
            currentGraphics = canvasPictureBox.CreateGraphics();
            bufferContext = new BufferedGraphicsContext();
            buffer = bufferContext.Allocate(currentGraphics, canvasPictureBox.ClientRectangle);
            brushForClear = new SolidBrush(Color.White);
            penForCutter = new Pen(Color.Red);
            penForPolygons = new Pen(Color.Black);
            penForSegments = new Pen(Color.Blue, 3);

            x1NumericUpDown.Maximum = canvasPictureBox.Width;
            y1NumericUpDown.Maximum = canvasPictureBox.Height;
            x1NumericUpDown.Minimum = 0;
            y1NumericUpDown.Minimum = 0;

            polygons = new List<Polygon>();
            polygons.Add(new Polygon());
            cutter = new Polygon();
        }

        private void repaint()
        {
            buffer.Graphics.FillRectangle(brushForClear, canvasPictureBox.ClientRectangle);
            foreach (Polygon i in polygons)
                i.draw(buffer.Graphics, penForPolygons);
            cutter.draw(buffer.Graphics, penForCutter);
            buffer.Render(currentGraphics);
        }

        private void btnCut_Click(object sender, EventArgs e)
        {
            if (!cutter.isClosed)
            {
                MessageBox.Show("Не замкнутый отсекатель!");
                return;
            }

            if (!cutter.isConvex())
            {
                MessageBox.Show("Отсекатель не выпуклый!");
                return;
            }

            foreach (Polygon i in polygons)
            {
                if (i.isClosed)
                {
                    Polygon res = i.getCutPolygon(cutter);
                    if (res != null)
                        res.draw(buffer.Graphics, penForSegments);
                }
            }
            buffer.Render(currentGraphics);
        }


        private void btnClear_Click(object sender, EventArgs e)
        {
            polygons.Clear();
            polygons.Add(new Polygon());
            cutter.clear();
            lbxCutter.Items.Clear();
            repaint();
        }

        private void btnAddLine_Click(object sender, EventArgs e)
        {
            try
            {
                Point p1 = new Point((int)x1NumericUpDown.Value, (int)y1NumericUpDown.Value);
                if (rbtnPolygons.Checked)
                {
                    if (polygons.Last().isClosed)
                        polygons.Add(new Polygon());
                    polygons.Last().add(p1);
                }

                if (rbtnCutter.Checked)
                {
                    if (cutter.isClosed)
                        cutter = new Polygon();
                    cutter.add(p1);
                    if (!cutter.isClosed)
                        lbxCutter.Items.Add(cutter[cutter.Count() - 1].ToString());
                }
                repaint();
            }
            catch (System.Exception ex)
            {
            	
            }
        }

        private void canvasPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            Point p1 = new Point(e.X, e.Y);
            if (rbtnPolygons.Checked)
            {
                if (polygons.Last().isClosed)
                    polygons.Add(new Polygon());
                polygons.Last().add(p1);
            }

            if (rbtnCutter.Checked)
            {
                if (cutter.isClosed)
                {
                    cutter = new Polygon();
                    lbxCutter.Items.Clear();
                }
                cutter.add(p1);
                if (!cutter.isClosed)
                    lbxCutter.Items.Add(cutter[cutter.Count() - 1].ToString());
            }
            repaint();
        }
    }
}
