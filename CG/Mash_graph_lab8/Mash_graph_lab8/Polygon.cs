﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Mash_graph_lab8
{
    class Polygon
    {
        private List<Point> points = new List<Point>();
        public bool isClosed = false;

        public void add(Point p)
        {
            if (points.Contains(p))
            {
                if (points.Count >= 3)
                    isClosed = true;   
            }
            else
                points.Add(p);
        }

        public int Count() { return points.Count; }

        public Point this[int index]
        {
            get
            {
                return points[index];
            }
            set
            {
                points[index] = value;
            }
        }




        public void clear() { points = new List<Point>(); }

        public void drawLine(Graphics graphics, Pen pen, Bitmap map) 
        {

        } 

        public void draw(Graphics graphics, Pen pen)
        {
            if (!isClosed)
                for (int i = 0; i < points.Count - 1; i++)
                    graphics.DrawLine(pen, points[i], points[i + 1]);
            else
                for (int i = 0; i < points.Count; i++)
                    graphics.DrawLine(pen, points[i], points[(i + 1) % points.Count]);
        }

        public bool contain(Point p1)
        {
            return points.Contains(p1);
        }

        public MathVector[] getVectors ()
        {
            List<MathVector> vectors = new List<MathVector>();
            if (!isClosed)
                for (int i = 0; i < points.Count - 1; i++)
                    vectors.Add(new MathVector(points[i], points[i + 1]));
            else
                for (int i = 0; i < points.Count; i++)
                    vectors.Add(new MathVector(points[i], points[(i + 1) % points.Count]));
            return vectors.ToArray();
        }

        public bool isConvex()
        {
            MathVector[] vectors = getVectors();

            bool res = true;
            if (vectors.Count() < 3)
                return false;
            bool mark = MathVector.VectorMultiplicationMark(vectors[0], vectors[1]) > 0;
            for (int i = 1; i < vectors.Count() && res; i++)
                if (MathVector.VectorMultiplicationMark(vectors[i], vectors[(i + 1)%vectors.Length]) > 0 != mark)
                    res = false;
            return res;
        }

        public List<Point> getPoints() { return points; }

        public Polygon getCutPolygon(Polygon cutter)
        {
            Polygon res = this;
            MathVector[] cutterVectors = cutter.getVectors();
            MathVector[] vectors;

            for (int i = 0; i < cutterVectors.Count(); i++)
            {
                vectors = res.getVectors();
                MathVector n = cutterVectors[i].getNormal();
                if (n * cutterVectors[(i + 1) % cutterVectors.Count()] < 0)
                    n *= -1;
                Polygon buf = new Polygon();
                buf.isClosed = true;
                for (int j = 0; j < vectors.Count(); j++)
                {
                    MathVector w1 = new MathVector(cutterVectors[i].getSecondPoint(), vectors[j].getSecondPoint());
                    PointF intersectBuf = MyLine.Intersection(vectors[j], cutterVectors[i]);
                    Point intersect = new Point((int)intersectBuf.X, (int)intersectBuf.Y);
                    if (intersect.X != -1 && vectors[j].inRectagle(intersect))
                        buf.add(intersect);
                    if (n*w1 >= 0)
                        buf.add(vectors[j].getSecondPoint());
                }
                if (buf.Count() == 0)
                    return null;
                res = buf;
            }
            return res;
        }
    }
}
