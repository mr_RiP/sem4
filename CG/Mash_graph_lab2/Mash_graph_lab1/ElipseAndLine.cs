﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Mash_graph_lab1
{
    enum TypeOfFigure {Ellipse, Line};
    class MyPolygon
    {
        private static PointF[] GenetateEllipsePoints(PointF[] ellipse )
        {
            PointF[] ellipse_points= new PointF[360];
            float a = MyLine.LineLenght(ellipse[0],
                ellipse[2]) / 2;
            float b = MyLine.LineLenght(ellipse[1],
                ellipse[3]) / 2;
            PointF center_of_ellipse = new PointF(
                (ellipse[0].X + ellipse[2].X) / 2,
                (ellipse[0].Y + ellipse[2].Y) / 2);
            for (int i = 0; i < 90; i++)
            {
                ellipse_points[i] = new PointF(ellipse[0].X,
                    (float)Math.Sqrt((a * b * a * b - (ellipse[0].X - center_of_ellipse.X)
                    * (ellipse[0].X - center_of_ellipse.X) * b * b)) / a + center_of_ellipse.Y);
                ellipse_points[359-i]=new PointF(ellipse[0].X,
                    -(float)Math.Sqrt((a * b * a * b - (ellipse[0].X - center_of_ellipse.X)
                    * (ellipse[0].X - center_of_ellipse.X) * b * b)) / a + center_of_ellipse.Y);
                ellipse_points[179 - i] = new PointF(ellipse[2].X,
                    (float)Math.Sqrt((a * b * a * b - (ellipse[2].X - center_of_ellipse.X)
                    * (ellipse[2].X - center_of_ellipse.X) * b * b)) / a + center_of_ellipse.Y);
                ellipse_points[180+i]=new PointF(ellipse[2].X,
                    -(float)Math.Sqrt((a * b * a * b - (ellipse[2].X - center_of_ellipse.X)
                    * (ellipse[2].X - center_of_ellipse.X) * b * b)) / a + center_of_ellipse.Y);
                MyPointFActions.Rotate(center_of_ellipse, 1, ellipse);
            }
            return ellipse_points;
        }

        private TypeOfFigure type;

        private PointF[] final_points;

        public PointF[]  GetFinalPoints()
        {
            return final_points;
        }

        public TypeOfFigure GetTypeOfFigure()
        {
            return type;
        }

        public MyPolygon(PointF[] points, TypeOfFigure buf_type)
        {
            int n = 0;
            type = buf_type;
            switch (type)
            {
                case TypeOfFigure.Line:
                    final_points = new PointF[2];
                    n = 2;
                    for (int i = 0; i < n; i++)
                    {
                        final_points[i].X = points[i].X;
                        final_points[i].Y = points[i].Y;
                    }
                    break;
                case TypeOfFigure.Ellipse:
                    
                    n = 360;
                    if (points.Length == 4)
                        final_points = GenetateEllipsePoints(points);
                    else
                    {
                        final_points = new PointF[360];
                        for (int i = 0; i < n; i++)
                        {
                            final_points[i].X = points[i].X;
                            final_points[i].Y = points[i].Y;
                        }
                    }
                    break;
            }

        }

        public MyPolygon(MyPolygon copy): this(copy.GetFinalPoints(), copy.GetTypeOfFigure())
        {
            
        }

        public void RotatePolygon(PointF p, float angle)
        {
            MyPointFActions.Rotate(p, angle, final_points);
        }

        public void ScalePolygon(PointF p, float cx, float cy)
        {
            MyPointFActions.Scale(p, cx, cy, final_points);
        }

        public void OffsetPolygon(float dx, float dy)
        {
            MyPointFActions.Offset(dx, dy, final_points);
        }
    }
}
