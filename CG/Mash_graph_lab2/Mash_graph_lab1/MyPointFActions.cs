﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;


namespace Mash_graph_lab1
{
    
    class MyPointFActions
    {
        
        public static void Offset(float dx, float dy, PointF[] points)
        {
            // смещение
            Matrix matrix = new Matrix(1, 0, 0, 1, dx, dy);
            matrix.TransformPoints(points);
        }

        public static void Rotate(PointF p, float angle, PointF[] points)
        {
            const float radian = (float)(Math.PI/180);
            //поворот относительно точки
            Offset(-p.X, -p.Y, points);
            Matrix matrix = new Matrix((float)Math.Cos(angle * radian), -(float)Math.Sin(angle * radian),
                (float)Math.Sin(angle * radian), (float)Math.Cos(angle * radian), 0, 0);
            matrix.TransformPoints(points);
            Offset(p.X, p.Y, points); 
        }

        public static void Scale(PointF p, float cx, float cy, PointF[] points)
        {
            // масштабирование относительно точки
            Offset(-p.X, -p.Y, points); 
            Matrix matrix = new Matrix(cx, 0, 0, cy, 0, 0);
            matrix.TransformPoints(points); 
            Offset(p.X, p.Y, points);
        }

    }
}
