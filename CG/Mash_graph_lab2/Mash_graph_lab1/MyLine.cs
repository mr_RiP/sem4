﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Mash_graph_lab1
{
    class MyLine
    {
        private float A, B, C;
        public float GetA()
        {
            return A;
        }
        public float GetB()
        {
            return B;
        }
        public float GetC()
        {
            return C;
        }
        private void SetLine(PointF p1, PointF p2)
        {
            A = p1.Y - p2.Y;
            B = p2.X - p1.X;
            C = p1.X * p2.Y - p2.X * p1.Y;
        }


        public float GetX(float y)
        {
            if (A == 0)
                return 0;
            return (y * B + C) / (-A);
        }
        public float GetY(float x)
        {
            if (B == 0)
                return 0;
            return (x * A + C) / (-B);
        }

        public MyLine(float a, float b, float c)
        {
            A = a;
            B = b;
            C = c;
        }
        public MyLine(PointF p1, PointF p2)
        {
            SetLine(p1, p2);
        }

        /// <summary>
        /// Пересечение между двумя прямыми
        /// </summary>
        /// <param name="l1"></param>
        /// <param name="l2"></param>
        /// <returns></returns>
        public static PointF Intersection(MyLine l1, MyLine l2)
        {
            PointF res = new PointF(-1,-1);
            if ((l1.GetA() == 0 && l2.GetA() == 0) || (l1.GetB() == 0 && l2.GetB() == 0))
            {
                return res;
            }
            else
            {
                res.X = -(l1.GetC() * l2.GetB() - l2.GetC() * l1.GetB()) / (l1.GetA() * l2.GetB() - l2.GetA() * l1.GetB());
                res.Y = -(l1.GetA() * l2.GetC() - l2.GetA() * l1.GetC()) / (l1.GetA() * l2.GetB() - l2.GetA() * l1.GetB());
            }
            return res;
        }

        /// <summary>
        /// Поворот на угол angle относительно точки поворота base_p
        /// </summary>
        /// <param name="angle"></param>
        /// <param name="base_p"></param>
        public void Rotation(float angle, PointF base_p, PointF second_p)
        {
            
            PointF[] buf_p = new PointF[1];
            float cosa = Convert.ToSingle(Math.Cos(angle / 180 * Math.PI));
            float sina = Convert.ToSingle(Math.Sin(angle / 180 * Math.PI));
            Matrix rotate_matrix = new Matrix(cosa, -sina, sina, cosa, 0, 0);
            buf_p [0] = second_p;
            buf_p [0].X -= base_p.X;
            buf_p [0].Y -= base_p.Y;
            rotate_matrix.TransformPoints(buf_p);
            buf_p[0].X += base_p.X;
            buf_p[0].Y += base_p.Y;
            SetLine(base_p, buf_p[0]);
        }

        /// <summary>
        /// Угол между двумя линиями l1 и l2
        /// </summary>
        /// <param name="l1"></param>
        /// <param name="l2"></param>
        /// <returns></returns>
        public static float AngleBetwenTwoLine(MyLine l1, MyLine l2)
        {
            double tan_angle = 0;
            if ((l1.GetA() == 0 && l2.GetA() == 0) || (l1.GetB() == 0 && l2.GetB() == 0))
                return 0;
            tan_angle = (l1.GetA() * l2.GetB() - l2.GetA() * l1.GetB()) / (l1.GetA() * l2.GetA() + l1.GetB() * l2.GetB());
            return Convert.ToSingle(Math.Atan(tan_angle)/Math.PI*180);
        }

        public static float LineLenght(PointF p1, PointF p2)
        {
            return Convert.ToSingle(Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2)));
        }
    }
}
