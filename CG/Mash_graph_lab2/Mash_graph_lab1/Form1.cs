﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Mash_graph_lab1
{

    public partial class Form1 : Form
    {
        // для графики
        BufferedGraphics buffer;    // класс буффера (чтобы рендерить)
        BufferedGraphicsContext buffer_context;     // класс для создания буффера
        Graphics current_g;
        Pen pen_for_usual;
        Brush brush_for_usual;
        Brush brush_for_clear;
        Rectangle paint_rectangle;
        //private double scale_koef;
        private SizeF painting_area;
        private PointF begin_of_painting_area;
        private int index_of_selected_point;
        private List<PointF> main_list;
        float scale;// соотношение размера picturebox к реальному
        MyFigure figure;
        MyFigure back_figure;
        bool can_back;
        public Form1()
        {
            InitializeComponent();
            MyFigure.begin_of_painting_area = new PointF(-300, -300);
            MyFigure.paint_rectangle = new SizeF(600, 600);
            figure = new MyFigure();
            can_back = false;
        }

        public void RePaint(SizeF painting_area, PointF begin_of_area)
        {
            buffer.Graphics.FillRectangle ( brush_for_clear, paint_rectangle );
            buffer.Graphics.DrawRectangle ( pen_for_usual, paint_rectangle );
            MyFigure buf_figure = figure.Transform_PointF();
            buf_figure.RePaint(buffer, pen_for_usual);
            buffer.Render(current_g);
        }

        private void Form1_Load ( object sender, EventArgs e )
        {
            
            index_of_selected_point = -1;
            
            paint_rectangle = pictureBox1.ClientRectangle;
            //
            current_g = pictureBox1.CreateGraphics ( );
            buffer_context = new BufferedGraphicsContext ( );
            buffer = buffer_context.Allocate ( current_g, paint_rectangle );
            //
            brush_for_usual = new SolidBrush ( Color.Green );
            brush_for_clear = new SolidBrush ( Color.White );
            //
            pen_for_usual =  new Pen ( Color.Black, 1 );
            RePaint(MyFigure.paint_rectangle, MyFigure.begin_of_painting_area);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Дана фигура. Реализовать масштабирование, поворот, сдвиг.");
        }

        private void pictureBox1_MouseMove ( object sender, MouseEventArgs e )
        {
            float x=e.X+MyFigure.begin_of_painting_area.X;
            float y = (MyFigure.paint_rectangle.Height - e.Y) + MyFigure.begin_of_painting_area.Y;
            label4.Text = "Курсор: <" + x + "," + y + ">";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                back_figure = new MyFigure(figure);
                figure.OffsetFigure(float.Parse(textBox1.Text), float.Parse(textBox2.Text));
                can_back = true;
            }
            catch
            {
            }
            RePaint(MyFigure.paint_rectangle, MyFigure.begin_of_painting_area);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                back_figure = new MyFigure(figure);
                figure.RotateFigure(new PointF(float.Parse(textBox4.Text), float.Parse(textBox3.Text)), float.Parse(textBox5.Text));
                can_back = true;
            }
            catch
            {
            }
            RePaint(MyFigure.paint_rectangle, MyFigure.begin_of_painting_area);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                back_figure = new MyFigure(figure);
                figure.ScaleFigure(new PointF(float.Parse(textBox7.Text), float.Parse(textBox7.Text)), 
                    float.Parse(textBox9.Text), float.Parse(textBox10.Text));
                can_back = true;
                
            }
            catch
            {
            }
            RePaint(MyFigure.paint_rectangle, MyFigure.begin_of_painting_area);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (can_back)
            {
                can_back = false;
                figure = new MyFigure(back_figure);
                RePaint(MyFigure.paint_rectangle, MyFigure.begin_of_painting_area);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            figure = new MyFigure();
            RePaint(MyFigure.paint_rectangle, MyFigure.begin_of_painting_area);
        }

    }
}
