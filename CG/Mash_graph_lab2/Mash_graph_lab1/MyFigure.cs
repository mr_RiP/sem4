﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Mash_graph_lab1
{
    class MyFigure
    {
        private MyPolygon[] elements;
        // 0- круг в центре
        // 1- линия 1 из центра
        // 2- линия 2 из центра
        // 3- линия 3 из центра
        // 4- линия 4 из центра
        // 5- первый круг вокруг центра
        public static PointF begin_of_painting_area;
        public static SizeF paint_rectangle;

        public void OffsetFigure(float dx, float dy)
        {
            for (int i = 0; i < elements.Length; i++)
                elements[i].OffsetPolygon(dx, dy);
        }
        public void ScaleFigure(PointF p, float cx, float cy)
        {
            for (int i = 0; i < elements.Length; i++)
                elements[i].ScalePolygon(p, cx, cy);
        }
        public void RotateFigure(PointF p, float angle)
        {
            for (int i = 0; i < elements.Length; i++)
                elements[i].RotatePolygon(p, angle);
        }

        public void RePaint(BufferedGraphics buffer, Pen my_pen)
        {
            for (int i = 0; i < elements.Length; i++)
            {
                buffer.Graphics.DrawLines(my_pen, elements[i].GetFinalPoints());
            }
        }

        public MyPolygon[] GetPolygonsArray()
        {
            return elements;
        }

        // перевод точки из реальной системы координат в систему координат на форме ДОДЕЛАТЬ
        public MyFigure Transform_PointF( )
        {
            MyFigure buf_figure = new MyFigure(this);
            PointF[] points_array;
            for (int i = 0; i < elements.Length; i++)
            {
                points_array = buf_figure.GetPolygonsArray()[i].GetFinalPoints();
                for(int j=0; j<points_array.Length; j++)
                {
                    points_array[j].X = (points_array[j].X - begin_of_painting_area.X);
                    points_array[j].Y = paint_rectangle.Height - (points_array[j].Y - begin_of_painting_area.Y);
                }
            }
            return buf_figure;
        }

        public MyFigure(MyFigure buf)
        {
            elements = new MyPolygon[buf.GetPolygonsArray ().Length];
            for (int i = 0; i < buf.GetPolygonsArray().Length; i++)
            {
                elements[i] = new MyPolygon(buf.GetPolygonsArray()[i]);
            }
        }

        public MyFigure()
        {
            elements = new MyPolygon[25];
            PointF[] points = new PointF[4];
            points[0] = new PointF(-10, 0);
            points[1] = new PointF(0, 10);
            points[2] = new PointF(10, 0);
            points[3] = new PointF(0, -10);
            elements[0]= new MyPolygon(points, TypeOfFigure.Ellipse);
            points = new PointF[4];
            points[0] = new PointF(-50, 0);
            points[1] = new PointF(0, 50);
            points[2] = new PointF(50, 0);
            points[3] = new PointF(0, -50);
            elements[5] = new MyPolygon(points, TypeOfFigure.Ellipse);
            points[0] = new PointF(-5, 55);
            points[1] = new PointF(0, 60);
            points[2] = new PointF(5, 55);
            points[3] = new PointF(0, 50);
            elements[6] = new MyPolygon(points, TypeOfFigure.Ellipse);
            points[0] = new PointF(-60, 0);
            points[1] = new PointF(-55, 5);
            points[2] = new PointF(-50, 0);
            points[3] = new PointF(-55, -5);
            elements[7] = new MyPolygon(points, TypeOfFigure.Ellipse);
            points[0] = new PointF(-5, -55);
            points[1] = new PointF(0, -50);
            points[2] = new PointF(5, -55);
            points[3] = new PointF(0, -60);
            elements[8] = new MyPolygon(points, TypeOfFigure.Ellipse);
            points[0] = new PointF(50, 0);
            points[1] = new PointF(55, 5);
            points[2] = new PointF(60, 0);
            points[3] = new PointF(55, -5);
            elements[9] = new MyPolygon(points, TypeOfFigure.Ellipse);
            points[0] = new PointF(-60, 0);
            points[1] = new PointF(0, 60);
            points[2] = new PointF(60, 0);
            points[3] = new PointF(0, -60);
            elements[10] = new MyPolygon(points, TypeOfFigure.Ellipse);
            points = new PointF[2];
            points[0] = new PointF(-10 / (float)Math.Sqrt(2),  10/ (float)Math.Sqrt(2));
            points[1] = new PointF(-50 / (float)Math.Sqrt(2), 50 / (float)Math.Sqrt(2));
            elements[1] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF( 10 / (float)Math.Sqrt(2), 10 / (float)Math.Sqrt(2));
            points[1] = new PointF( 50 / (float)Math.Sqrt(2), 50 / (float)Math.Sqrt(2));
            elements[2] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(10 / (float)Math.Sqrt(2), -10 / (float)Math.Sqrt(2));
            points[1] = new PointF(50 / (float)Math.Sqrt(2), -50 / (float)Math.Sqrt(2));
            elements[3] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(-10 / (float)Math.Sqrt(2), -10 / (float)Math.Sqrt(2));
            points[1] = new PointF(-50 / (float)Math.Sqrt(2), -50 / (float)Math.Sqrt(2));
            elements[4] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(-5 / (float)Math.Sqrt(2), 5 / (float)Math.Sqrt(2)+55);
            points[1] = new PointF(5 / (float)Math.Sqrt(2), -5 / (float)Math.Sqrt(2)+55);
            elements[11] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(-5 / (float)Math.Sqrt(2),-5 / (float)Math.Sqrt(2) + 55);
            points[1] = new PointF(5 / (float)Math.Sqrt(2), 5 / (float)Math.Sqrt(2) + 55);
            elements[12] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(-5 / (float)Math.Sqrt(2), 5 / (float)Math.Sqrt(2) - 55);
            points[1] = new PointF(5 / (float)Math.Sqrt(2), -5 / (float)Math.Sqrt(2) - 55);
            elements[13] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(-5 / (float)Math.Sqrt(2), -5 / (float)Math.Sqrt(2) - 55);
            points[1] = new PointF(5 / (float)Math.Sqrt(2), 5 / (float)Math.Sqrt(2) - 55);
            elements[14] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(-5 / (float)Math.Sqrt(2)-55, -5 / (float)Math.Sqrt(2));
            points[1] = new PointF(5 / (float)Math.Sqrt(2)-55, 5 / (float)Math.Sqrt(2) );
            elements[15] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(-5 / (float)Math.Sqrt(2) - 55, 5 / (float)Math.Sqrt(2));
            points[1] = new PointF(5 / (float)Math.Sqrt(2) - 55, -5 / (float)Math.Sqrt(2));
            elements[16] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(-5 / (float)Math.Sqrt(2) + 55, -5 / (float)Math.Sqrt(2));
            points[1] = new PointF(5 / (float)Math.Sqrt(2) + 55, 5 / (float)Math.Sqrt(2));
            elements[17] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(-5 / (float)Math.Sqrt(2) + 55, 5 / (float)Math.Sqrt(2));
            points[1] = new PointF(5 / (float)Math.Sqrt(2) + 55, -5 / (float)Math.Sqrt(2));
            elements[18] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(0, 0);
            points[1] = new PointF(-60, -90);
            elements[19] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(-60, -90);
            points[1] = new PointF(-30, -90);
            elements[20] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(0, 0);
            points[1] = new PointF(-30, -90);
            elements[21] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(0, 0);
            points[1] = new PointF(60, -90);
            elements[22] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(60, -90);
            points[1] = new PointF(30, -90);
            elements[23] = new MyPolygon(points, TypeOfFigure.Line);
            points[0] = new PointF(0, 0);
            points[1] = new PointF(30, -90);
            elements[24] = new MyPolygon(points, TypeOfFigure.Line);
        }
    }
}
