﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mash_graph_lab9
{
    class Matrix3D
    {
        double[,] m = new double[4, 4];

        public Matrix3D()
        {
            for (int i=0; i<4; i++)
            {
                for (int j = 0; j < 4; j++)
                    m[i, j] = 0;
                m[i, i] = 1;
            }
        }
        
        public Matrix3D getRotationZMatrix(double angle)
        {
            angle *= Math.PI / 180; 
            m[0, 0] = Math.Cos(angle);
            m[0, 1] = -Math.Sin(angle);
            m[1, 0] = Math.Sin(angle);
            m[1, 1] = Math.Cos(angle);
            return this;
        }

        public Matrix3D getRotationYMatrix(double angle)
        {
            angle *= Math.PI / 180; 
            m[0, 0] = Math.Cos(angle);
            m[0, 2] = Math.Sin(angle);
            m[2, 0] = -Math.Sin(angle);
            m[2, 2] = Math.Cos(angle);
            return this;
        }

        public Matrix3D getRotationXMatrix(double angle)
        {
            angle *= Math.PI / 180; 
            m[1, 1] = Math.Cos(angle);
            m[1, 2] = -Math.Sin(angle);
            m[2, 1] = Math.Sin(angle);
            m[2, 2] = Math.Cos(angle);
            return this;
        }

        public Matrix3D getOffsetMatrix(double x, double y, double z)
        {
            m[3, 0] = x;
            m[3, 1] = y;
            m[3, 2] = z;
            return this;
        }

        public Matrix3D getScaleMatrix(double koef)
        {
            m[0, 0] = koef;
            m[1, 1] = koef;
            m[2, 2] = koef;
            return this;
        }

        public static Matrix3D operator* (Matrix3D m1, Matrix3D m2)
        {
            Matrix3D res = new Matrix3D();
            
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    res.m[i,j] = 0;
                    for (int k = 0; k < 4; k++)
                        res.m[i,j] += m1.m[i,k] * m2.m[k,j];
                }
            return res;
        }

        public static Point3D operator *(Point3D p, Matrix3D matr)
        {
            Point3D res = new Point3D();
            double[] resCor = res.coordinates, cor = p.coordinates;
            for (int i=0; i<4; i++)
                for (int j=0; j<4; j++)
                    resCor[i] += matr.m[j,i] * cor[j];
            return res;
        }
    }
}
