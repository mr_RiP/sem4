﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mash_graph_lab9
{
    class Point3D
    {
        public double[] coordinates = new double[4];

        public Point3D(double x, double y, double z, double k)
        {
            coordinates[0] = x;
            coordinates[1] = y;
            coordinates[2] = z;
            coordinates[3] = k;
        }

        public Point3D()
        {
            coordinates[0] = 0;
            coordinates[1] = 0;
            coordinates[2] = 0;
            coordinates[3] = 1;
        }

        public Point3D(double x, double y, double z)
        {
            coordinates[0] = x;
            coordinates[1] = y;
            coordinates[2] = z;
            coordinates[3] = 1;
        }

        public Point3D(Point3D p)
        {
            coordinates[0] = p.coordinates[0];
            coordinates[1] = p.coordinates[1];
            coordinates[2] = p.coordinates[2];
            coordinates[3] = 1;
        }

        public double X
        {
            get { return coordinates[0]; }
            set { coordinates[0] = value; }
        }

        public double Y
        {
            get { return coordinates[1]; }
            set { coordinates[1] = value; }
        }

        public double Z
        {
            get { return coordinates[2]; }
            set { coordinates[2] = value; }
        }

        public static Point3D operator + (Point3D p1, Point3D p2)
        {
            Point3D newP = new Point3D(p1);
            newP.coordinates[0] += p2.coordinates[0];
            newP.coordinates[1] += p2.coordinates[1];
            newP.coordinates[2] += p2.coordinates[2];
            return newP;
        }

        public static Point3D operator -(Point3D p1, Point3D p2)
        {
            Point3D newP = new Point3D(p1);
            newP.coordinates[0] -= p2.coordinates[0];
            newP.coordinates[1] -= p2.coordinates[1];
            newP.coordinates[2] -= p2.coordinates[2];
            return newP;
        }
    }
}
