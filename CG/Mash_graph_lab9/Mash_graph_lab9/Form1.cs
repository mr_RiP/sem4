﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mash_graph_lab9
{
    public partial class Form1 : Form
    {

        BufferedGraphics buffer;    // класс буффера (чтобы рендерить)
        BufferedGraphicsContext bufferContext;     // класс для создания буффера
        Graphics currentGraphics;
        Brush brushForClear;
        Pen penForLines;

        double xAngle = 0, yAngle = 0, zAngle = 0;
        double xAngleStep = 5, yAngleStep = 5, zAngleStep = 5;

        delegate double Function2(double x, double z); // result y

        private static Dictionary<string, Function2> functions = new Dictionary<string, Function2>
        {
            {"y = sin(x) * cos(z)", (x, z) => Math.Sin(x) * Math.Cos(z)},
            {"y = x^3+z^3", (x, z) => x * x * x + z * z * z},
            {"y = cos(x*z)*z", (x, z) => Math.Cos(x*z)*z}
        };

        public Form1()
        {
            InitializeComponent();
            currentGraphics = canvasPictureBox.CreateGraphics();
            bufferContext = new BufferedGraphicsContext();
            buffer = bufferContext.Allocate(currentGraphics, canvasPictureBox.ClientRectangle);
            brushForClear = new SolidBrush(Color.White);
            penForLines = new Pen(Color.Red);

            functionsComboBox.Items.AddRange(functions.Keys.ToArray());
            functionsComboBox.SelectedIndex = 0;
            rePaint();
        }

        private Point3D rotatePoint(Point3D p)
        {
            Matrix3D m1 = new Matrix3D(), m2 = new Matrix3D(), m3 = new Matrix3D();
            m1.getRotationXMatrix(xAngle);
            m2.getRotationYMatrix(yAngle);
            m3.getRotationZMatrix(zAngle);
            return p * m1 * m2 * m3;
        }

        private Point transformPointToCanvas(Point3D p)
        {
            double scale = 100;
            return new Point
            (
                (int)(canvasPictureBox.Width / 2.0 + (p.X * scale)),
                (int)(canvasPictureBox.Height / 2.0 - (p.Y * scale))
            );
        }

        Brush brushForPoints = new SolidBrush(Color.Black);

        private void drawPoint(int x, int y)
        {
            buffer.Graphics.FillRectangle(brushForPoints, x, y, 1, 1);
        }

        public void update(Point p1, Point p2, double[] yMin, double[] yMax)
        {
            double dX = p2.X - p1.X, dY = p2.Y - p1.Y;
            double l = (int)Math.Max(dX, dY);
            dX /= l; dY /= l;
            double x = p1.X, y = p1.Y;

            for (int i = 0; i <= l; i++, x += dX, y += dY)
            {
                if (x > 0 && x < canvasPictureBox.Width)
                {
                    if (yMax[(int)x] <= y)
                    {
                        yMax[(int)x] = y;
                        drawPoint((int)x, (int)y);
                    }

                    if (yMin[(int)x] >= y)
                    {
                        yMin[(int)x] = y;
                        drawPoint((int)x, (int)y);
                    }
                    

                }
            }

        }

        private void rePaint()
        {
            buffer.Graphics.FillRectangle(brushForClear, canvasPictureBox.ClientRectangle);

            Function2 f = functions[(string)functionsComboBox.SelectedItem];

            double xMin = (double)xMinNumericUpDown.Value, xMax = (double)xMaxNumericUpDown.Value, xStep = (double)xStepNumericUpDown.Value;
            double zMin = (double)zMinNumericUpDown.Value, zMax = (double)zMaxNumericUpDown.Value, zStep = (double)zStepNumericUpDown.Value;
            if (xMin >= xMax || zMin >= zMax)
                return;

            double[] yMax = new double[canvasPictureBox.Width], yMin = new double[canvasPictureBox.Width];
            for (int i=0; i<canvasPictureBox.Width; i++)
            {
                yMax[i] = double.MinValue;
                yMin[i] = double.MaxValue;
            }

            Point3D p1;

            for (double z = zMax; z >= zMin; z -= zStep)
            {
                p1 = new Point3D(xMin, f(xMin, z), z);
                p1 = rotatePoint(p1);

                for (double x = xMin + xStep; x <= xMax; x += xStep)
                {
                    Point p1buf = transformPointToCanvas(p1);

                    Point3D p2 = new Point3D(x, f(x, z), z);
                    p2 = rotatePoint(p2);
                    Point p2buf = transformPointToCanvas(p2);

                    update(p1buf, p2buf, yMin, yMax);

                    p1 = p2;
                }
            }
            buffer.Render();
        }

        private void zMinNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            rePaint();
        }

        private void xRotateL_Click(object sender, EventArgs e)
        {
            xAngle -= xAngleStep;
            rePaint();
        }

        private void yRotateL_Click(object sender, EventArgs e)
        {
            yAngle -= yAngleStep;
            rePaint();
        }

        private void yRotateR_Click(object sender, EventArgs e)
        {
            yAngle += yAngleStep;
            rePaint();
        }

        private void xRotateR_Click(object sender, EventArgs e)
        {
            xAngle += xAngleStep;
            rePaint();
        }

        private void zMaxNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            rePaint();
        }

        private void zStepNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            rePaint();
        }

        private void xMinNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            rePaint();
        }

        private void xMaxNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            rePaint();
        }

        private void zRotateL_Click(object sender, EventArgs e)
        {
            zAngle -= zAngleStep;
            rePaint();
        }

        private void zRotateR_Click(object sender, EventArgs e)
        {
            zAngle += zAngleStep;
            rePaint();
        }

        private void functionsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            rePaint();
        }

        private void xStepNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            rePaint();
        }



    }
}
