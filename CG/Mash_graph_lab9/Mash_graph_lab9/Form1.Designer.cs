﻿namespace Mash_graph_lab9
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.canvasPictureBox = new System.Windows.Forms.PictureBox();
            this.xMinLabel = new System.Windows.Forms.Label();
            this.xMinNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.xMaxLabel = new System.Windows.Forms.Label();
            this.xMaxNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.zMinLabel = new System.Windows.Forms.Label();
            this.zMinNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.zStepLabel = new System.Windows.Forms.Label();
            this.zMaxLabel = new System.Windows.Forms.Label();
            this.zMaxNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.zStepNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.functionsLabel = new System.Windows.Forms.Label();
            this.functionsComboBox = new System.Windows.Forms.ComboBox();
            this.yRotateL = new System.Windows.Forms.Button();
            this.yRotateR = new System.Windows.Forms.Button();
            this.xRotateL = new System.Windows.Forms.Button();
            this.xRotateR = new System.Windows.Forms.Button();
            this.zRotateL = new System.Windows.Forms.Button();
            this.zRotateR = new System.Windows.Forms.Button();
            this.xStepNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.xStepLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xMinNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xMaxNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zMinNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zMaxNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zStepNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xStepNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // canvasPictureBox
            // 
            this.canvasPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.canvasPictureBox.BackColor = System.Drawing.Color.White;
            this.canvasPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.canvasPictureBox.Location = new System.Drawing.Point(12, 12);
            this.canvasPictureBox.Name = "canvasPictureBox";
            this.canvasPictureBox.Size = new System.Drawing.Size(700, 500);
            this.canvasPictureBox.TabIndex = 35;
            this.canvasPictureBox.TabStop = false;
            // 
            // xMinLabel
            // 
            this.xMinLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xMinLabel.AutoSize = true;
            this.xMinLabel.Location = new System.Drawing.Point(730, 89);
            this.xMinLabel.Name = "xMinLabel";
            this.xMinLabel.Size = new System.Drawing.Size(32, 13);
            this.xMinLabel.TabIndex = 46;
            this.xMinLabel.Text = "xMin:";
            // 
            // xMinNumericUpDown
            // 
            this.xMinNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xMinNumericUpDown.DecimalPlaces = 2;
            this.xMinNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.xMinNumericUpDown.Location = new System.Drawing.Point(733, 105);
            this.xMinNumericUpDown.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.xMinNumericUpDown.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.xMinNumericUpDown.Name = "xMinNumericUpDown";
            this.xMinNumericUpDown.Size = new System.Drawing.Size(66, 20);
            this.xMinNumericUpDown.TabIndex = 44;
            this.xMinNumericUpDown.Value = new decimal(new int[] {
            3,
            0,
            0,
            -2147483648});
            this.xMinNumericUpDown.ValueChanged += new System.EventHandler(this.xMinNumericUpDown_ValueChanged);
            // 
            // xMaxLabel
            // 
            this.xMaxLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xMaxLabel.AutoSize = true;
            this.xMaxLabel.Location = new System.Drawing.Point(799, 89);
            this.xMaxLabel.Name = "xMaxLabel";
            this.xMaxLabel.Size = new System.Drawing.Size(35, 13);
            this.xMaxLabel.TabIndex = 47;
            this.xMaxLabel.Text = "xMax:";
            // 
            // xMaxNumericUpDown
            // 
            this.xMaxNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xMaxNumericUpDown.DecimalPlaces = 2;
            this.xMaxNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.xMaxNumericUpDown.Location = new System.Drawing.Point(802, 105);
            this.xMaxNumericUpDown.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.xMaxNumericUpDown.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.xMaxNumericUpDown.Name = "xMaxNumericUpDown";
            this.xMaxNumericUpDown.Size = new System.Drawing.Size(66, 20);
            this.xMaxNumericUpDown.TabIndex = 45;
            this.xMaxNumericUpDown.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.xMaxNumericUpDown.ValueChanged += new System.EventHandler(this.xMaxNumericUpDown_ValueChanged);
            // 
            // zMinLabel
            // 
            this.zMinLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zMinLabel.AutoSize = true;
            this.zMinLabel.Location = new System.Drawing.Point(730, 50);
            this.zMinLabel.Name = "zMinLabel";
            this.zMinLabel.Size = new System.Drawing.Size(32, 13);
            this.zMinLabel.TabIndex = 41;
            this.zMinLabel.Text = "zMin:";
            // 
            // zMinNumericUpDown
            // 
            this.zMinNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zMinNumericUpDown.DecimalPlaces = 2;
            this.zMinNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.zMinNumericUpDown.Location = new System.Drawing.Point(733, 66);
            this.zMinNumericUpDown.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.zMinNumericUpDown.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.zMinNumericUpDown.Name = "zMinNumericUpDown";
            this.zMinNumericUpDown.Size = new System.Drawing.Size(66, 20);
            this.zMinNumericUpDown.TabIndex = 39;
            this.zMinNumericUpDown.Value = new decimal(new int[] {
            3,
            0,
            0,
            -2147483648});
            this.zMinNumericUpDown.ValueChanged += new System.EventHandler(this.zMinNumericUpDown_ValueChanged);
            // 
            // zStepLabel
            // 
            this.zStepLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zStepLabel.AutoSize = true;
            this.zStepLabel.Location = new System.Drawing.Point(871, 50);
            this.zStepLabel.Name = "zStepLabel";
            this.zStepLabel.Size = new System.Drawing.Size(37, 13);
            this.zStepLabel.TabIndex = 43;
            this.zStepLabel.Text = "zStep:";
            // 
            // zMaxLabel
            // 
            this.zMaxLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zMaxLabel.AutoSize = true;
            this.zMaxLabel.Location = new System.Drawing.Point(799, 50);
            this.zMaxLabel.Name = "zMaxLabel";
            this.zMaxLabel.Size = new System.Drawing.Size(35, 13);
            this.zMaxLabel.TabIndex = 42;
            this.zMaxLabel.Text = "zMax:";
            // 
            // zMaxNumericUpDown
            // 
            this.zMaxNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zMaxNumericUpDown.DecimalPlaces = 2;
            this.zMaxNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.zMaxNumericUpDown.Location = new System.Drawing.Point(802, 66);
            this.zMaxNumericUpDown.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.zMaxNumericUpDown.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.zMaxNumericUpDown.Name = "zMaxNumericUpDown";
            this.zMaxNumericUpDown.Size = new System.Drawing.Size(66, 20);
            this.zMaxNumericUpDown.TabIndex = 40;
            this.zMaxNumericUpDown.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.zMaxNumericUpDown.ValueChanged += new System.EventHandler(this.zMaxNumericUpDown_ValueChanged);
            // 
            // zStepNumericUpDown
            // 
            this.zStepNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zStepNumericUpDown.DecimalPlaces = 2;
            this.zStepNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.zStepNumericUpDown.Location = new System.Drawing.Point(874, 66);
            this.zStepNumericUpDown.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.zStepNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.zStepNumericUpDown.Name = "zStepNumericUpDown";
            this.zStepNumericUpDown.Size = new System.Drawing.Size(66, 20);
            this.zStepNumericUpDown.TabIndex = 38;
            this.zStepNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.zStepNumericUpDown.ValueChanged += new System.EventHandler(this.zStepNumericUpDown_ValueChanged);
            // 
            // functionsLabel
            // 
            this.functionsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.functionsLabel.AutoSize = true;
            this.functionsLabel.Location = new System.Drawing.Point(730, 10);
            this.functionsLabel.Name = "functionsLabel";
            this.functionsLabel.Size = new System.Drawing.Size(56, 13);
            this.functionsLabel.TabIndex = 37;
            this.functionsLabel.Text = "Функция:";
            // 
            // functionsComboBox
            // 
            this.functionsComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.functionsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.functionsComboBox.FormattingEnabled = true;
            this.functionsComboBox.Location = new System.Drawing.Point(730, 26);
            this.functionsComboBox.Name = "functionsComboBox";
            this.functionsComboBox.Size = new System.Drawing.Size(210, 21);
            this.functionsComboBox.TabIndex = 36;
            this.functionsComboBox.SelectedIndexChanged += new System.EventHandler(this.functionsComboBox_SelectedIndexChanged);
            // 
            // yRotateL
            // 
            this.yRotateL.Location = new System.Drawing.Point(730, 242);
            this.yRotateL.Name = "yRotateL";
            this.yRotateL.Size = new System.Drawing.Size(66, 23);
            this.yRotateL.TabIndex = 48;
            this.yRotateL.Text = "<-Y";
            this.yRotateL.UseVisualStyleBackColor = true;
            this.yRotateL.Click += new System.EventHandler(this.yRotateL_Click);
            // 
            // yRotateR
            // 
            this.yRotateR.Location = new System.Drawing.Point(833, 242);
            this.yRotateR.Name = "yRotateR";
            this.yRotateR.Size = new System.Drawing.Size(66, 23);
            this.yRotateR.TabIndex = 49;
            this.yRotateR.Text = "Y->";
            this.yRotateR.UseVisualStyleBackColor = true;
            this.yRotateR.Click += new System.EventHandler(this.yRotateR_Click);
            // 
            // xRotateL
            // 
            this.xRotateL.Location = new System.Drawing.Point(802, 178);
            this.xRotateL.Name = "xRotateL";
            this.xRotateL.Size = new System.Drawing.Size(25, 66);
            this.xRotateL.TabIndex = 50;
            this.xRotateL.Text = "/\\| X";
            this.xRotateL.UseVisualStyleBackColor = true;
            this.xRotateL.Click += new System.EventHandler(this.xRotateL_Click);
            // 
            // xRotateR
            // 
            this.xRotateR.Location = new System.Drawing.Point(802, 263);
            this.xRotateR.Name = "xRotateR";
            this.xRotateR.Size = new System.Drawing.Size(25, 66);
            this.xRotateR.TabIndex = 51;
            this.xRotateR.Text = "X | \\/";
            this.xRotateR.UseVisualStyleBackColor = true;
            this.xRotateR.Click += new System.EventHandler(this.xRotateR_Click);
            // 
            // zRotateL
            // 
            this.zRotateL.Location = new System.Drawing.Point(766, 357);
            this.zRotateL.Name = "zRotateL";
            this.zRotateL.Size = new System.Drawing.Size(42, 56);
            this.zRotateL.TabIndex = 54;
            this.zRotateL.Text = "    --Z    |      \\/";
            this.zRotateL.UseVisualStyleBackColor = true;
            this.zRotateL.Click += new System.EventHandler(this.zRotateL_Click);
            // 
            // zRotateR
            // 
            this.zRotateR.Location = new System.Drawing.Point(826, 357);
            this.zRotateR.Name = "zRotateR";
            this.zRotateR.Size = new System.Drawing.Size(42, 56);
            this.zRotateR.TabIndex = 55;
            this.zRotateR.Text = "Z--        |     \\/";
            this.zRotateR.UseVisualStyleBackColor = true;
            this.zRotateR.Click += new System.EventHandler(this.zRotateR_Click);
            // 
            // xStepNumericUpDown
            // 
            this.xStepNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xStepNumericUpDown.DecimalPlaces = 2;
            this.xStepNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.xStepNumericUpDown.Location = new System.Drawing.Point(874, 105);
            this.xStepNumericUpDown.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.xStepNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.xStepNumericUpDown.Name = "xStepNumericUpDown";
            this.xStepNumericUpDown.Size = new System.Drawing.Size(66, 20);
            this.xStepNumericUpDown.TabIndex = 56;
            this.xStepNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.xStepNumericUpDown.ValueChanged += new System.EventHandler(this.xStepNumericUpDown_ValueChanged);
            // 
            // xStepLabel
            // 
            this.xStepLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xStepLabel.AutoSize = true;
            this.xStepLabel.Location = new System.Drawing.Point(871, 89);
            this.xStepLabel.Name = "xStepLabel";
            this.xStepLabel.Size = new System.Drawing.Size(37, 13);
            this.xStepLabel.TabIndex = 57;
            this.xStepLabel.Text = "xStep:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 516);
            this.Controls.Add(this.xStepLabel);
            this.Controls.Add(this.xStepNumericUpDown);
            this.Controls.Add(this.zRotateR);
            this.Controls.Add(this.zRotateL);
            this.Controls.Add(this.xRotateR);
            this.Controls.Add(this.xRotateL);
            this.Controls.Add(this.yRotateR);
            this.Controls.Add(this.yRotateL);
            this.Controls.Add(this.xMinLabel);
            this.Controls.Add(this.xMinNumericUpDown);
            this.Controls.Add(this.xMaxLabel);
            this.Controls.Add(this.xMaxNumericUpDown);
            this.Controls.Add(this.zMinLabel);
            this.Controls.Add(this.zMinNumericUpDown);
            this.Controls.Add(this.zStepLabel);
            this.Controls.Add(this.zMaxLabel);
            this.Controls.Add(this.zMaxNumericUpDown);
            this.Controls.Add(this.zStepNumericUpDown);
            this.Controls.Add(this.functionsLabel);
            this.Controls.Add(this.functionsComboBox);
            this.Controls.Add(this.canvasPictureBox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xMinNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xMaxNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zMinNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zMaxNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zStepNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xStepNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox canvasPictureBox;
        private System.Windows.Forms.Label xMinLabel;
        private System.Windows.Forms.NumericUpDown xMinNumericUpDown;
        private System.Windows.Forms.Label xMaxLabel;
        private System.Windows.Forms.NumericUpDown xMaxNumericUpDown;
        private System.Windows.Forms.Label zMinLabel;
        private System.Windows.Forms.NumericUpDown zMinNumericUpDown;
        private System.Windows.Forms.Label zStepLabel;
        private System.Windows.Forms.Label zMaxLabel;
        private System.Windows.Forms.NumericUpDown zMaxNumericUpDown;
        private System.Windows.Forms.NumericUpDown zStepNumericUpDown;
        private System.Windows.Forms.Label functionsLabel;
        private System.Windows.Forms.ComboBox functionsComboBox;
        private System.Windows.Forms.Button yRotateL;
        private System.Windows.Forms.Button yRotateR;
        private System.Windows.Forms.Button xRotateL;
        private System.Windows.Forms.Button xRotateR;
        private System.Windows.Forms.Button zRotateL;
        private System.Windows.Forms.Button zRotateR;
        private System.Windows.Forms.NumericUpDown xStepNumericUpDown;
        private System.Windows.Forms.Label xStepLabel;
    }
}

