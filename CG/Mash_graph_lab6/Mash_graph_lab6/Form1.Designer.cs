﻿namespace Mash_graph_lab6
{
    partial class mainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.canvasPictureBox = new System.Windows.Forms.PictureBox();
            this.gpbxFigure = new System.Windows.Forms.GroupBox();
            this.rbtnRectangle = new System.Windows.Forms.RadioButton();
            this.rbtnLine = new System.Windows.Forms.RadioButton();
            this.btnCut = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).BeginInit();
            this.gpbxFigure.SuspendLayout();
            this.SuspendLayout();
            // 
            // canvasPictureBox
            // 
            this.canvasPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.canvasPictureBox.BackColor = System.Drawing.Color.White;
            this.canvasPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.canvasPictureBox.Location = new System.Drawing.Point(12, 12);
            this.canvasPictureBox.Name = "canvasPictureBox";
            this.canvasPictureBox.Size = new System.Drawing.Size(715, 370);
            this.canvasPictureBox.TabIndex = 18;
            this.canvasPictureBox.TabStop = false;
            this.canvasPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvasPictureBox_MouseDown);
            this.canvasPictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvasPictureBox_MouseMove);
            this.canvasPictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvasPictureBox_MouseUp);
            // 
            // gpbxFigure
            // 
            this.gpbxFigure.Controls.Add(this.rbtnRectangle);
            this.gpbxFigure.Controls.Add(this.rbtnLine);
            this.gpbxFigure.Location = new System.Drawing.Point(733, 12);
            this.gpbxFigure.Name = "gpbxFigure";
            this.gpbxFigure.Size = new System.Drawing.Size(161, 70);
            this.gpbxFigure.TabIndex = 19;
            this.gpbxFigure.TabStop = false;
            this.gpbxFigure.Text = "Выбор фигуры";
            // 
            // rbtnRectangle
            // 
            this.rbtnRectangle.AutoSize = true;
            this.rbtnRectangle.Location = new System.Drawing.Point(6, 42);
            this.rbtnRectangle.Name = "rbtnRectangle";
            this.rbtnRectangle.Size = new System.Drawing.Size(85, 17);
            this.rbtnRectangle.TabIndex = 1;
            this.rbtnRectangle.TabStop = true;
            this.rbtnRectangle.Text = "Отсекатель";
            this.rbtnRectangle.UseVisualStyleBackColor = true;
            // 
            // rbtnLine
            // 
            this.rbtnLine.AutoSize = true;
            this.rbtnLine.Location = new System.Drawing.Point(6, 19);
            this.rbtnLine.Name = "rbtnLine";
            this.rbtnLine.Size = new System.Drawing.Size(68, 17);
            this.rbtnLine.TabIndex = 0;
            this.rbtnLine.TabStop = true;
            this.rbtnLine.Text = "Отрезок";
            this.rbtnLine.UseVisualStyleBackColor = true;
            this.rbtnLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbtnLine_KeyDown);
            this.rbtnLine.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rbtnLine_KeyUp);
            // 
            // btnCut
            // 
            this.btnCut.Location = new System.Drawing.Point(733, 88);
            this.btnCut.Name = "btnCut";
            this.btnCut.Size = new System.Drawing.Size(161, 23);
            this.btnCut.TabIndex = 20;
            this.btnCut.Text = "Отсечь";
            this.btnCut.UseVisualStyleBackColor = true;
            this.btnCut.Click += new System.EventHandler(this.btnCut_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(732, 117);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(162, 23);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Очистить все";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 394);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCut);
            this.Controls.Add(this.gpbxFigure);
            this.Controls.Add(this.canvasPictureBox);
            this.Name = "mainForm";
            this.Text = "Отсечение";
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mainForm_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mainForm_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).EndInit();
            this.gpbxFigure.ResumeLayout(false);
            this.gpbxFigure.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox canvasPictureBox;
        private System.Windows.Forms.GroupBox gpbxFigure;
        private System.Windows.Forms.RadioButton rbtnRectangle;
        private System.Windows.Forms.RadioButton rbtnLine;
        private System.Windows.Forms.Button btnCut;
        private System.Windows.Forms.Button btnClear;
    }
}

