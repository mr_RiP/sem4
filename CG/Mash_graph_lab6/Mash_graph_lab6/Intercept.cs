﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;

namespace Mash_graph_lab6
{
    class Intercept : MyLine
    {
        private Point p1, p2;

        public Intercept(Point p1, Point p2)
            : base(p1, p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }

        public void setFirstPoint(int x, int y)
        {
            p1 = new Point(x, y);
        }

        public void setSecondPoint(int x, int y)
        {
            p2 = new Point(x, y);
        }

        public Point getFirstPoint()
        {
            return p1;
        }

        public Point getSecondPoint()
        {
            return p2;
        }


        public bool inRectagle(Point point)
        {
            if (point.X > Math.Max(p1.X, p2.X) || point.X < Math.Min(p1.X, p2.X)
                || point.Y > Math.Max(p1.Y, p2.Y) || point.Y < Math.Min(p1.Y, p2.Y))
                return false;
            return true;
        }

        public static Point Intersection(Intercept l1, Intercept l2)
        {
            PointF bufRes = MyLine.Intersection(l1, l2);
            Point res = new Point ((int)bufRes.X, (int)bufRes.Y);
            if (bufRes.X != -1 && bufRes.Y != -1 && l1.inRectagle(res) && l2.inRectagle(res))
                return res;
            return new Point(-1, -1);
        }

        public void reCalculationABC()
        {
            SetLine(p1, p2);
        }

        public void drawIntercept(BufferedGraphics buffer, Pen penForLines)
        {
            buffer.Graphics.DrawLine(penForLines, p1, p2);
        }
    }
}
