﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mash_graph_lab6
{

    struct mys
    {
        string a;
    };

    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            rbtnRectangle.Checked = true;
            currentGraphics = canvasPictureBox.CreateGraphics();
            bufferContext = new BufferedGraphicsContext();
            buffer = bufferContext.Allocate(currentGraphics, canvasPictureBox.ClientRectangle);
            brushForClear = new SolidBrush(Color.White);
            penForLines = new Pen(Color.Red);
            penForRectangle = new Pen(Color.Black);
            penForSegments = new Pen(Color.Blue, 3);

            intercepts = new List<Intercept>();
            rectagle = new List<Intercept>();
        }

        BufferedGraphics buffer;    // класс буффера (чтобы рендерить)
        BufferedGraphicsContext bufferContext;     // класс для создания буффера
        Graphics currentGraphics;
        Brush brushForClear;
        Pen penForLines;
        Pen penForRectangle;
        Pen penForSegments;
        List<Intercept> intercepts;
        List<Intercept> rectagle;
        bool isDown = false;
        bool shiftIsDown = false;

        private void repaint()
        {
            buffer.Graphics.FillRectangle(brushForClear, canvasPictureBox.ClientRectangle);

            foreach (Intercept i in intercepts)
                i.drawIntercept(buffer, penForLines);
            foreach (Intercept i in rectagle)
                i.drawIntercept(buffer, penForRectangle);

            buffer.Render(currentGraphics);
        }


        private void canvasPictureBox_MouseMove(object sender, MouseEventArgs e)
        {

            if (rbtnLine.Checked == true && isDown)
            {
                if (shiftIsDown)
                    if (Math.Abs(e.X - intercepts.Last().getFirstPoint().X) > Math.Abs(e.Y - intercepts.Last().getFirstPoint().Y))
                        intercepts.Last().setSecondPoint(e.X, intercepts.Last().getFirstPoint().Y);
                    else
                        intercepts.Last().setSecondPoint(intercepts.Last().getFirstPoint().X, e.Y);
                else
                    intercepts.Last().setSecondPoint(e.X, e.Y);
                repaint();
            }
            if (rbtnRectangle.Checked == true && isDown)
            {
                rectagle[0].setSecondPoint(rectagle[0].getSecondPoint().X, e.Y);
                rectagle[1].setFirstPoint(rectagle[0].getSecondPoint().X, e.Y);
                rectagle[1].setSecondPoint(e.X, e.Y);
                rectagle[2].setSecondPoint(e.X, rectagle[0].getFirstPoint().Y);
                rectagle[2].setFirstPoint(e.X, e.Y);
                rectagle[3].setFirstPoint(e.X, rectagle[0].getFirstPoint().Y);
                repaint();
            }

        }

        private void canvasPictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (rbtnLine.Checked == true)
            {
                intercepts.Last().reCalculationABC();
            }
            if (rbtnRectangle.Checked == true)
            {
                rectagle[0].reCalculationABC();
                rectagle[1].reCalculationABC();
                rectagle[2].reCalculationABC();
                rectagle[3].reCalculationABC();
            }
            isDown = false;
        }

        private void canvasPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (rbtnLine.Checked == true)
                intercepts.Add(new Intercept(new Point(e.X, e.Y), new Point(e.X, e.Y)));
            if (rbtnRectangle.Checked == true)
            {
                rectagle.Clear();
                rectagle.Add(new Intercept(new Point(e.X, e.Y), new Point(e.X, e.Y)));
                rectagle.Add(new Intercept(new Point(e.X, e.Y), new Point(e.X, e.Y)));
                rectagle.Add(new Intercept(new Point(e.X, e.Y), new Point(e.X, e.Y)));
                rectagle.Add(new Intercept(new Point(e.X, e.Y), new Point(e.X, e.Y)));
            }
            repaint();
            isDown = true;
        }

        private void btnCut_Click(object sender, EventArgs e)
        {
            if (rectagle.Count != 4 || intercepts.Count==0)
            {
                MessageBox.Show("Недостаточно данных!");
                return;
            }
            int[] T1 = new int[4];
            int[] T2 = new int[4];
            int S1, S2, P;
            Point p1, p2;
            Point[] pRes = new Point[2];
            int flag = 0;
            int xl = Math.Min(rectagle[0].getFirstPoint().X,rectagle[2].getFirstPoint().X); // x left
            int yt = Math.Min(rectagle[0].getFirstPoint().Y, rectagle[2].getFirstPoint().Y); // y top
            int xr = Math.Max(rectagle[0].getFirstPoint().X, rectagle[2].getFirstPoint().X);// x right
            int yb = Math.Max(rectagle[0].getFirstPoint().Y, rectagle[2].getFirstPoint().Y); // y bottom



            foreach (Intercept line in intercepts)
            {
                pRes = new Point[2];
                flag = 0;
                p1 = line.getFirstPoint();
                p2 = line.getSecondPoint();
                if (p1.X >= xl) T1[3] = 0; else T1[3] = 1;
                if (p1.X <= xr) T1[2] = 0; else T1[2] = 1;
                if (p1.Y >= yt) T1[0] = 0; else T1[0] = 1;
                if (p1.Y <= yb) T1[1] = 0; else T1[1] = 1;
                if (p2.X >= xl) T2[3] = 0; else T2[3] = 1;
                if (p2.X <= xr) T2[2] = 0; else T2[2] = 1;
                if (p2.Y >= yt) T2[0] = 0; else T2[0] = 1;
                if (p2.Y <= yb) T2[1] = 0; else T2[1] = 1;
                S1 = 0; S2 = 0; P = 0;
                for (int i = 0; i < 4; i++)
                {
                    S1 += T1[i];
                    S2 += T2[i];
                    P += T1[i] * T2[i];
                }
                if (S1 == 0 && S2 == 0)
                {
                    buffer.Graphics.DrawLine(penForSegments, p1, p2);
                    continue;
                }
                if (P != 0) continue;
                if (S1 == 0) { pRes[0] = p1; flag = 1; }
                if (S2 == 0) { pRes[0] = p2; flag = 1; }
                foreach (Intercept rectSide in rectagle)
                {
                    if (flag == 2)
                        break;
                    Point bufPoint = Intercept.Intersection(rectSide, line);
                    if (bufPoint.X != -1)
                        pRes[flag++] = bufPoint;
                }
                if (flag == 2)
                    buffer.Graphics.DrawLine(penForSegments, pRes[0], pRes[1]);

            }
            buffer.Render(currentGraphics);
            if (rbtnLine.Checked)
                rbtnLine.Focus();
        }

        private void mainForm_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void mainForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Shift)
                shiftIsDown = false;
        }

        private void rbtnLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Shift)
                shiftIsDown = true;
        }

        private void rbtnLine_KeyUp(object sender, KeyEventArgs e)
        {
            shiftIsDown = false;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            rectagle.Clear();
            intercepts.Clear();
            buffer.Graphics.FillRectangle(brushForClear, canvasPictureBox.ClientRectangle);
            buffer.Render(currentGraphics);
            
        }
    }
}
