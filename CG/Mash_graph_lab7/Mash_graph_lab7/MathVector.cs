﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;

namespace Mash_graph_lab7
{
    class MathVector:Intercept
    {
        private int dx;
        private int dy;

        public int getdx() { return dx; }
        public int getdy() { return dy; }

        public MathVector(Point p1, Point p2)
            : base(p1, p2)
        {
            dx = p2.X - p1.X;
            dy = p2.Y - p1.Y;
        }

        public MathVector(int dx, int dy)
            : base(new Point(0, 0), new Point(dx, dy))
        {
            this.dx = dx;
            this.dy = dy;
        }

        public static double operator *(MathVector vector1, MathVector vector2)
        {
            return vector1.dx * vector2.dx + vector1.dy * vector2.dy;
        }

        public static MathVector operator *(MathVector vector1, double koef)
        {
            return new MathVector((int)(vector1.dx * koef), (int)(vector1.dy * koef));
        }

        public static MathVector operator +(MathVector vector1, MathVector vector2)
        {
            return new MathVector(vector1.getdx() + vector2.getdx(), vector1.getdy() + vector2.getdy());
        }

        public static MathVector operator -(MathVector vector1, MathVector vector2)
        {
            return new MathVector(vector1.getdx() - vector2.getdx(), vector1.getdy() - vector2.getdy());
        }

        public static double VectorMultiplicationMark(MathVector vector1, MathVector vector2)
        {
            return (vector1.dx * vector2.dy - vector1.dy * vector2.dx);
        }

        public MathVector getNormal()
        {
            MathVector res;
            if (dx != 0)
                res = new MathVector(-dy * 1000 / dx, 1000);
            else
                res = new MathVector(1, 0);
            return res;
        }

        public static double angleBetweenTwoVector(MathVector vector1, MathVector vector2)
        {
            return Math.Acos((vector1 * vector2) / (vector1.length() * vector2.length()));
        }
    }
}
