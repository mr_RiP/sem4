﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mash_graph_lab7
{
    public partial class mainForm : Form
    {
        BufferedGraphics buffer;    // класс буффера (чтобы рендерить)
        BufferedGraphicsContext bufferContext;     // класс для создания буффера
        Graphics currentGraphics;
        Brush brushForClear;
        Pen penForLines;
        Pen penForPolygon;
        Pen penForSegments;
        List<Intercept> intercepts;
        List<Point> polygon;

        public mainForm()
        {
            InitializeComponent();
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            rbtnPolygon.Checked = true;
            currentGraphics = canvasPictureBox.CreateGraphics();
            bufferContext = new BufferedGraphicsContext();
            buffer = bufferContext.Allocate(currentGraphics, canvasPictureBox.ClientRectangle);
            brushForClear = new SolidBrush(Color.White);
            penForLines = new Pen(Color.Red);
            penForPolygon = new Pen(Color.Black);
            penForSegments = new Pen(Color.Blue, 3);

            intercepts = new List<Intercept>();
            polygon = new List<Point>();

            x1NumericUpDown.Maximum = canvasPictureBox.Width;
            x2NumericUpDown.Maximum = canvasPictureBox.Width;
            y1NumericUpDown.Maximum = canvasPictureBox.Height;
            y2NumericUpDown.Maximum = canvasPictureBox.Height;
            x1NumericUpDown.Minimum = 0;
            x2NumericUpDown.Minimum = 0;
            y1NumericUpDown.Minimum = 0;
            y2NumericUpDown.Minimum = 0;
            
        }

        private void btnCut_Click(object sender, EventArgs e)
        {
            // внимание впереди сплошной быдлокод.
            
            if (polygon.Count<2)
            {
                MessageBox.Show("Введите больше точек!");
                return;
            }
            // подготовка векторов
            List<MathVector> polygonVectors = new List<MathVector>(),
                             interceptsVectors = new List<MathVector>();
            foreach (Intercept i in intercepts)
                interceptsVectors.Add(new MathVector(i.getFirstPoint(), i.getSecondPoint()));
            for (int i = 0; i < polygon.Count; i++)
                polygonVectors.Add(new MathVector(polygon[i], polygon[(i + 1) % (polygon.Count)]));

            // проверка многоугольника на выпуклость 
            bool mark = MathVector.VectorMultiplicationMark(polygonVectors[0], polygonVectors[1]) > 0;
            for (int i=1; i<polygonVectors.Count-1; i++)
                if (MathVector.VectorMultiplicationMark(polygonVectors[i], polygonVectors[i + 1]) > 0 != mark)
                {
                    MessageBox.Show("Многоугольник не выпуклый!");
                    return;
                }

            // главное тело алгоритма
            foreach (MathVector vector in interceptsVectors)
            {
                double tn = 0, tv = 1;
                bool flag = true;
                for (int i = 0; i < polygonVectors.Count && flag; i++)
                {
                    MathVector w = new MathVector(polygonVectors[i].getSecondPoint(), vector.getFirstPoint());
                    MathVector n = polygonVectors[i].getNormal();

                    if (n * polygonVectors[(i + 1) % polygonVectors.Count] < 0)
                        n *= -1;
                    double Dsk = n * vector, Wsk = n * w;

                    if (Dsk == 0)
                    {
                        if (Wsk < 0)
                            flag = false;
                    }
                    else
                    {
                        double t = -Wsk / Dsk;
                        if (Dsk > 0)
                        {
                            if (t <= 1)
                                tn = Math.Max(tn, t);
                            else
                                flag = false;

                        }
                        else
                        {
                            if (t >= 0)
                                tv = Math.Min(tv, t);
                            else
                                flag = false;
                        }
                    }
                }
                if (tn <= tv && flag)
                {
                    Point r1 = new Point((int)(vector.getFirstPoint().X + vector.getdx() * tn), (int)(vector.getFirstPoint().Y + vector.getdy() * tn));
                    Point r2 = new Point((int)(vector.getFirstPoint().X + vector.getdx() * tv), (int)(vector.getFirstPoint().Y + vector.getdy() * tv));
                    buffer.Graphics.DrawLine(penForSegments, r1, r2);
                    buffer.Render(currentGraphics);
                }
            }
        }

        private void repaint()
        {
            buffer.Graphics.FillRectangle(brushForClear, canvasPictureBox.ClientRectangle);
            foreach (Intercept i in intercepts)
                i.drawIntercept(buffer, penForLines);
            for (int i = 0; i < polygon.Count; i++)
                buffer.Graphics.DrawLine(penForPolygon, polygon[i], polygon[(i + 1) % (polygon.Count)]);
            buffer.Render(currentGraphics);
        }

        bool isDown = false;
        bool shiftIsDown = false;

        private void canvasPictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (rbtnLine.Checked)
                isDown = false;
            if (rbtnPolygon.Checked)
            {
                polygon.Add(new Point(e.X, e.Y));
                lbxPolygon.Items.Add(polygon.Last().ToString());
            }
            repaint();
        }

        private void canvasPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (rbtnLine.Checked == true)
                intercepts.Add(new Intercept(new Point(e.X, e.Y), new Point(e.X, e.Y)));
            repaint();
            isDown = true;
        }

        private void rbtnLine_KeyUp(object sender, KeyEventArgs e)
        {
            shiftIsDown = false;
        }

        private void rbtnLine_KeyDown(object sender, KeyEventArgs e)
        {
            shiftIsDown = true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            polygon.Clear();
            intercepts.Clear();
            lbxPolygon.Items.Clear();
            repaint();
        }

        private void canvasPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (rbtnLine.Checked == true && isDown)
            {
                if (shiftIsDown)
                    if (Math.Abs(e.X - intercepts.Last().getFirstPoint().X) > Math.Abs(e.Y - intercepts.Last().getFirstPoint().Y))
                        intercepts.Last().setSecondPoint(e.X, intercepts.Last().getFirstPoint().Y);
                    else
                        intercepts.Last().setSecondPoint(intercepts.Last().getFirstPoint().X, e.Y);
                else
                    if (intercepts.Count != 0)
                        intercepts.Last().setSecondPoint(e.X, e.Y);
                repaint();
            }
        }

        private void btnAddLine_Click(object sender, EventArgs e)
        {
            try
            {
                Point p1 = new Point((int)x1NumericUpDown.Value, (int)y1NumericUpDown.Value);
                Point p2 = new Point((int)x2NumericUpDown.Value, (int)y2NumericUpDown.Value);
                intercepts.Add(new Intercept(p1, p2));
                repaint();
            }
            catch (System.Exception ex)
            {
            	
            }
        }


    }
}
