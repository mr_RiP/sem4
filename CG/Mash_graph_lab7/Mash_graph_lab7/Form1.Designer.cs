﻿namespace Mash_graph_lab7
{
    partial class mainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClear = new System.Windows.Forms.Button();
            this.btnCut = new System.Windows.Forms.Button();
            this.gpbxFigure = new System.Windows.Forms.GroupBox();
            this.rbtnPolygon = new System.Windows.Forms.RadioButton();
            this.rbtnLine = new System.Windows.Forms.RadioButton();
            this.canvasPictureBox = new System.Windows.Forms.PictureBox();
            this.lbxPolygon = new System.Windows.Forms.ListBox();
            this.yLabel = new System.Windows.Forms.Label();
            this.xLabel = new System.Windows.Forms.Label();
            this.y1NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.x1NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.y2NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.x2NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.btnAddLine = new System.Windows.Forms.Button();
            this.gpbxFigure.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.y1NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.x1NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.y2NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.x2NumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(732, 117);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(161, 23);
            this.btnClear.TabIndex = 21;
            this.btnClear.Text = "Очистить все";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnCut
            // 
            this.btnCut.Location = new System.Drawing.Point(733, 88);
            this.btnCut.Name = "btnCut";
            this.btnCut.Size = new System.Drawing.Size(160, 23);
            this.btnCut.TabIndex = 24;
            this.btnCut.Text = "Отсечь";
            this.btnCut.UseVisualStyleBackColor = true;
            this.btnCut.Click += new System.EventHandler(this.btnCut_Click);
            // 
            // gpbxFigure
            // 
            this.gpbxFigure.Controls.Add(this.rbtnPolygon);
            this.gpbxFigure.Controls.Add(this.rbtnLine);
            this.gpbxFigure.Location = new System.Drawing.Point(733, 12);
            this.gpbxFigure.Name = "gpbxFigure";
            this.gpbxFigure.Size = new System.Drawing.Size(160, 70);
            this.gpbxFigure.TabIndex = 23;
            this.gpbxFigure.TabStop = false;
            this.gpbxFigure.Text = "Выбор фигуры";
            // 
            // rbtnPolygon
            // 
            this.rbtnPolygon.AutoSize = true;
            this.rbtnPolygon.Location = new System.Drawing.Point(6, 42);
            this.rbtnPolygon.Name = "rbtnPolygon";
            this.rbtnPolygon.Size = new System.Drawing.Size(85, 17);
            this.rbtnPolygon.TabIndex = 1;
            this.rbtnPolygon.TabStop = true;
            this.rbtnPolygon.Text = "Отсекатель";
            this.rbtnPolygon.UseVisualStyleBackColor = true;
            // 
            // rbtnLine
            // 
            this.rbtnLine.AutoSize = true;
            this.rbtnLine.Location = new System.Drawing.Point(6, 19);
            this.rbtnLine.Name = "rbtnLine";
            this.rbtnLine.Size = new System.Drawing.Size(68, 17);
            this.rbtnLine.TabIndex = 0;
            this.rbtnLine.TabStop = true;
            this.rbtnLine.Text = "Отрезок";
            this.rbtnLine.UseVisualStyleBackColor = true;
            this.rbtnLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbtnLine_KeyDown);
            this.rbtnLine.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rbtnLine_KeyUp);
            // 
            // canvasPictureBox
            // 
            this.canvasPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.canvasPictureBox.BackColor = System.Drawing.Color.White;
            this.canvasPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.canvasPictureBox.Location = new System.Drawing.Point(12, 12);
            this.canvasPictureBox.Name = "canvasPictureBox";
            this.canvasPictureBox.Size = new System.Drawing.Size(700, 400);
            this.canvasPictureBox.TabIndex = 22;
            this.canvasPictureBox.TabStop = false;
            this.canvasPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvasPictureBox_MouseDown);
            this.canvasPictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvasPictureBox_MouseMove);
            this.canvasPictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvasPictureBox_MouseUp);
            // 
            // lbxPolygon
            // 
            this.lbxPolygon.FormattingEnabled = true;
            this.lbxPolygon.Location = new System.Drawing.Point(732, 247);
            this.lbxPolygon.Name = "lbxPolygon";
            this.lbxPolygon.Size = new System.Drawing.Size(161, 147);
            this.lbxPolygon.TabIndex = 25;
            // 
            // yLabel
            // 
            this.yLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.yLabel.AutoSize = true;
            this.yLabel.Location = new System.Drawing.Point(819, 146);
            this.yLabel.Name = "yLabel";
            this.yLabel.Size = new System.Drawing.Size(17, 13);
            this.yLabel.TabIndex = 29;
            this.yLabel.Text = "Y:";
            // 
            // xLabel
            // 
            this.xLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xLabel.AutoSize = true;
            this.xLabel.Location = new System.Drawing.Point(741, 145);
            this.xLabel.Name = "xLabel";
            this.xLabel.Size = new System.Drawing.Size(17, 13);
            this.xLabel.TabIndex = 28;
            this.xLabel.Text = "X:";
            // 
            // y1NumericUpDown
            // 
            this.y1NumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.y1NumericUpDown.Location = new System.Drawing.Point(819, 162);
            this.y1NumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.y1NumericUpDown.Name = "y1NumericUpDown";
            this.y1NumericUpDown.Size = new System.Drawing.Size(72, 20);
            this.y1NumericUpDown.TabIndex = 27;
            this.y1NumericUpDown.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            // 
            // x1NumericUpDown
            // 
            this.x1NumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.x1NumericUpDown.Location = new System.Drawing.Point(741, 161);
            this.x1NumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.x1NumericUpDown.Name = "x1NumericUpDown";
            this.x1NumericUpDown.Size = new System.Drawing.Size(72, 20);
            this.x1NumericUpDown.TabIndex = 26;
            this.x1NumericUpDown.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // y2NumericUpDown
            // 
            this.y2NumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.y2NumericUpDown.Location = new System.Drawing.Point(819, 188);
            this.y2NumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.y2NumericUpDown.Name = "y2NumericUpDown";
            this.y2NumericUpDown.Size = new System.Drawing.Size(72, 20);
            this.y2NumericUpDown.TabIndex = 31;
            this.y2NumericUpDown.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            // 
            // x2NumericUpDown
            // 
            this.x2NumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.x2NumericUpDown.Location = new System.Drawing.Point(741, 187);
            this.x2NumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.x2NumericUpDown.Name = "x2NumericUpDown";
            this.x2NumericUpDown.Size = new System.Drawing.Size(72, 20);
            this.x2NumericUpDown.TabIndex = 30;
            this.x2NumericUpDown.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // btnAddLine
            // 
            this.btnAddLine.Location = new System.Drawing.Point(732, 211);
            this.btnAddLine.Name = "btnAddLine";
            this.btnAddLine.Size = new System.Drawing.Size(161, 23);
            this.btnAddLine.TabIndex = 32;
            this.btnAddLine.Text = "Добавить отрезок";
            this.btnAddLine.UseVisualStyleBackColor = true;
            this.btnAddLine.Click += new System.EventHandler(this.btnAddLine_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 418);
            this.Controls.Add(this.btnAddLine);
            this.Controls.Add(this.y2NumericUpDown);
            this.Controls.Add(this.x2NumericUpDown);
            this.Controls.Add(this.yLabel);
            this.Controls.Add(this.xLabel);
            this.Controls.Add(this.y1NumericUpDown);
            this.Controls.Add(this.x1NumericUpDown);
            this.Controls.Add(this.lbxPolygon);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCut);
            this.Controls.Add(this.gpbxFigure);
            this.Controls.Add(this.canvasPictureBox);
            this.Name = "mainForm";
            this.Text = "Отсечение";
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.gpbxFigure.ResumeLayout(false);
            this.gpbxFigure.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.y1NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.x1NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.y2NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.x2NumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnCut;
        private System.Windows.Forms.GroupBox gpbxFigure;
        private System.Windows.Forms.RadioButton rbtnPolygon;
        private System.Windows.Forms.RadioButton rbtnLine;
        private System.Windows.Forms.PictureBox canvasPictureBox;
        private System.Windows.Forms.ListBox lbxPolygon;
        private System.Windows.Forms.Label yLabel;
        private System.Windows.Forms.Label xLabel;
        private System.Windows.Forms.NumericUpDown y1NumericUpDown;
        private System.Windows.Forms.NumericUpDown x1NumericUpDown;
        private System.Windows.Forms.NumericUpDown y2NumericUpDown;
        private System.Windows.Forms.NumericUpDown x2NumericUpDown;
        private System.Windows.Forms.Button btnAddLine;
    }
}

