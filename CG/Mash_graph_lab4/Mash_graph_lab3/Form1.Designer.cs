﻿namespace Mash_graph_lab3
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.clrDialog = new System.Windows.Forms.ColorDialog();
            this.gpbColors = new System.Windows.Forms.GroupBox();
            this.rbColor = new System.Windows.Forms.RadioButton();
            this.rbColorBackground = new System.Windows.Forms.RadioButton();
            this.btnColorDialog = new System.Windows.Forms.Button();
            this.cmbAlgotihms = new System.Windows.Forms.ComboBox();
            this.lblAlgorithms = new System.Windows.Forms.Label();
            this.btnImage = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnTimeAnaliz = new System.Windows.Forms.Button();
            this.tcItems = new System.Windows.Forms.TabControl();
            this.tpEllipse = new System.Windows.Forms.TabPage();
            this.lblEllipseHeight = new System.Windows.Forms.Label();
            this.lblEllipseCenterX = new System.Windows.Forms.Label();
            this.lblEllipseWidth = new System.Windows.Forms.Label();
            this.lblEllipseCenterY = new System.Windows.Forms.Label();
            this.nupEllipseCenterX = new System.Windows.Forms.NumericUpDown();
            this.nupEllipseWidth = new System.Windows.Forms.NumericUpDown();
            this.nupEllipseCenterY = new System.Windows.Forms.NumericUpDown();
            this.nupEllipseHeight = new System.Windows.Forms.NumericUpDown();
            this.tpCircle = new System.Windows.Forms.TabPage();
            this.lblCircleRadius = new System.Windows.Forms.Label();
            this.nupCircleRadius = new System.Windows.Forms.NumericUpDown();
            this.lblCircleCenterX = new System.Windows.Forms.Label();
            this.nupCircleCenterX = new System.Windows.Forms.NumericUpDown();
            this.nupCircleCenterY = new System.Windows.Forms.NumericUpDown();
            this.lblCircleCenterY = new System.Windows.Forms.Label();
            this.tpCircleSpectre = new System.Windows.Forms.TabPage();
            this.lblCircleSpectreRadiusStep = new System.Windows.Forms.Label();
            this.nupCircleSpectreRadiusStep = new System.Windows.Forms.NumericUpDown();
            this.lblCircleSpectreMinRadius = new System.Windows.Forms.Label();
            this.nupCircleSpectreMinRadius = new System.Windows.Forms.NumericUpDown();
            this.nupCircleSpectreMaxRadius = new System.Windows.Forms.NumericUpDown();
            this.lblCircleSpectreMaxRadius = new System.Windows.Forms.Label();
            this.tpEllipseSpectre = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.nupEllipseSpectreStepWidth = new System.Windows.Forms.NumericUpDown();
            this.lblMinA = new System.Windows.Forms.Label();
            this.nupEllipseSpectreMinWidth = new System.Windows.Forms.NumericUpDown();
            this.nupEllipseSpectreMaxWidth = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nupEllipseSpectreMinHeight = new System.Windows.Forms.NumericUpDown();
            this.nupEllipseSpectreMaxHeight = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.gpbColors.SuspendLayout();
            this.tcItems.SuspendLayout();
            this.tpEllipse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseCenterX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseCenterY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseHeight)).BeginInit();
            this.tpCircle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleCenterX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleCenterY)).BeginInit();
            this.tpCircleSpectre.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleSpectreRadiusStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleSpectreMinRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleSpectreMaxRadius)).BeginInit();
            this.tpEllipseSpectre.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseSpectreStepWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseSpectreMinWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseSpectreMaxWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseSpectreMinHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseSpectreMaxHeight)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(900, 550);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // gpbColors
            // 
            this.gpbColors.Controls.Add(this.rbColor);
            this.gpbColors.Controls.Add(this.rbColorBackground);
            this.gpbColors.Controls.Add(this.btnColorDialog);
            this.gpbColors.Location = new System.Drawing.Point(906, 138);
            this.gpbColors.Name = "gpbColors";
            this.gpbColors.Size = new System.Drawing.Size(228, 80);
            this.gpbColors.TabIndex = 9;
            this.gpbColors.TabStop = false;
            this.gpbColors.Text = "Цвет";
            // 
            // rbColor
            // 
            this.rbColor.AutoSize = true;
            this.rbColor.Checked = true;
            this.rbColor.Location = new System.Drawing.Point(12, 25);
            this.rbColor.Name = "rbColor";
            this.rbColor.Size = new System.Drawing.Size(14, 13);
            this.rbColor.TabIndex = 5;
            this.rbColor.TabStop = true;
            this.rbColor.UseVisualStyleBackColor = true;
            this.rbColor.CheckedChanged += new System.EventHandler(this.rbColor_CheckedChanged);
            // 
            // rbColorBackground
            // 
            this.rbColorBackground.AutoSize = true;
            this.rbColorBackground.Location = new System.Drawing.Point(12, 47);
            this.rbColorBackground.Name = "rbColorBackground";
            this.rbColorBackground.Size = new System.Drawing.Size(48, 17);
            this.rbColorBackground.TabIndex = 4;
            this.rbColorBackground.Text = "Фон";
            this.rbColorBackground.UseVisualStyleBackColor = true;
            this.rbColorBackground.CheckedChanged += new System.EventHandler(this.rbColorBackground_CheckedChanged);
            // 
            // btnColorDialog
            // 
            this.btnColorDialog.BackColor = System.Drawing.Color.Black;
            this.btnColorDialog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnColorDialog.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnColorDialog.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnColorDialog.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btnColorDialog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnColorDialog.ForeColor = System.Drawing.Color.Black;
            this.btnColorDialog.Location = new System.Drawing.Point(32, 21);
            this.btnColorDialog.Name = "btnColorDialog";
            this.btnColorDialog.Size = new System.Drawing.Size(20, 20);
            this.btnColorDialog.TabIndex = 3;
            this.btnColorDialog.UseVisualStyleBackColor = false;
            this.btnColorDialog.Click += new System.EventHandler(this.btnColorDialog_Click);
            // 
            // cmbAlgotihms
            // 
            this.cmbAlgotihms.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAlgotihms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAlgotihms.FormattingEnabled = true;
            this.cmbAlgotihms.Items.AddRange(new object[] {
            "Брезенхема",
            "Средней точки",
            "Ур-ие в декартовой СК",
            "Ур-ие в параметрической форме",
            "Библиотечкая"});
            this.cmbAlgotihms.Location = new System.Drawing.Point(906, 242);
            this.cmbAlgotihms.Name = "cmbAlgotihms";
            this.cmbAlgotihms.Size = new System.Drawing.Size(204, 21);
            this.cmbAlgotihms.TabIndex = 7;
            // 
            // lblAlgorithms
            // 
            this.lblAlgorithms.AutoSize = true;
            this.lblAlgorithms.Location = new System.Drawing.Point(907, 226);
            this.lblAlgorithms.Name = "lblAlgorithms";
            this.lblAlgorithms.Size = new System.Drawing.Size(59, 13);
            this.lblAlgorithms.TabIndex = 10;
            this.lblAlgorithms.Text = "Алгоритм:";
            // 
            // btnImage
            // 
            this.btnImage.Location = new System.Drawing.Point(906, 269);
            this.btnImage.Name = "btnImage";
            this.btnImage.Size = new System.Drawing.Size(93, 23);
            this.btnImage.TabIndex = 11;
            this.btnImage.Text = "Нарисовать";
            this.btnImage.UseVisualStyleBackColor = true;
            this.btnImage.Click += new System.EventHandler(this.btnImage_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(1017, 269);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(93, 23);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "Очистить";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnCLear_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Location = new System.Drawing.Point(906, 298);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(93, 23);
            this.btnSettings.TabIndex = 13;
            this.btnSettings.Text = "Условие";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnTimeAnaliz
            // 
            this.btnTimeAnaliz.Location = new System.Drawing.Point(1017, 298);
            this.btnTimeAnaliz.Name = "btnTimeAnaliz";
            this.btnTimeAnaliz.Size = new System.Drawing.Size(93, 23);
            this.btnTimeAnaliz.TabIndex = 15;
            this.btnTimeAnaliz.Text = "Анализ времени";
            this.btnTimeAnaliz.UseVisualStyleBackColor = true;
            this.btnTimeAnaliz.Click += new System.EventHandler(this.btnTimeAnaliz_Click);
            // 
            // tcItems
            // 
            this.tcItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tcItems.Controls.Add(this.tpEllipse);
            this.tcItems.Controls.Add(this.tpCircle);
            this.tcItems.Controls.Add(this.tpCircleSpectre);
            this.tcItems.Controls.Add(this.tpEllipseSpectre);
            this.tcItems.Location = new System.Drawing.Point(906, 12);
            this.tcItems.Multiline = true;
            this.tcItems.Name = "tcItems";
            this.tcItems.SelectedIndex = 0;
            this.tcItems.Size = new System.Drawing.Size(356, 112);
            this.tcItems.TabIndex = 16;
            // 
            // tpEllipse
            // 
            this.tpEllipse.Controls.Add(this.lblEllipseHeight);
            this.tpEllipse.Controls.Add(this.lblEllipseCenterX);
            this.tpEllipse.Controls.Add(this.lblEllipseWidth);
            this.tpEllipse.Controls.Add(this.lblEllipseCenterY);
            this.tpEllipse.Controls.Add(this.nupEllipseCenterX);
            this.tpEllipse.Controls.Add(this.nupEllipseWidth);
            this.tpEllipse.Controls.Add(this.nupEllipseCenterY);
            this.tpEllipse.Controls.Add(this.nupEllipseHeight);
            this.tpEllipse.Location = new System.Drawing.Point(4, 22);
            this.tpEllipse.Name = "tpEllipse";
            this.tpEllipse.Padding = new System.Windows.Forms.Padding(3);
            this.tpEllipse.Size = new System.Drawing.Size(348, 86);
            this.tpEllipse.TabIndex = 0;
            this.tpEllipse.Text = "Эллипс";
            this.tpEllipse.UseVisualStyleBackColor = true;
            // 
            // lblEllipseHeight
            // 
            this.lblEllipseHeight.AutoSize = true;
            this.lblEllipseHeight.Location = new System.Drawing.Point(72, 42);
            this.lblEllipseHeight.Name = "lblEllipseHeight";
            this.lblEllipseHeight.Size = new System.Drawing.Size(48, 13);
            this.lblEllipseHeight.TabIndex = 20;
            this.lblEllipseHeight.Text = "Высота:";
            // 
            // lblEllipseCenterX
            // 
            this.lblEllipseCenterX.AutoSize = true;
            this.lblEllipseCenterX.Location = new System.Drawing.Point(3, 3);
            this.lblEllipseCenterX.Name = "lblEllipseCenterX";
            this.lblEllipseCenterX.Size = new System.Drawing.Size(51, 13);
            this.lblEllipseCenterX.TabIndex = 15;
            this.lblEllipseCenterX.Text = "Центр X:";
            // 
            // lblEllipseWidth
            // 
            this.lblEllipseWidth.AutoSize = true;
            this.lblEllipseWidth.Location = new System.Drawing.Point(3, 42);
            this.lblEllipseWidth.Name = "lblEllipseWidth";
            this.lblEllipseWidth.Size = new System.Drawing.Size(49, 13);
            this.lblEllipseWidth.TabIndex = 19;
            this.lblEllipseWidth.Text = "Ширина:";
            // 
            // lblEllipseCenterY
            // 
            this.lblEllipseCenterY.AutoSize = true;
            this.lblEllipseCenterY.Location = new System.Drawing.Point(72, 3);
            this.lblEllipseCenterY.Name = "lblEllipseCenterY";
            this.lblEllipseCenterY.Size = new System.Drawing.Size(51, 13);
            this.lblEllipseCenterY.TabIndex = 16;
            this.lblEllipseCenterY.Text = "Центр Y:";
            // 
            // nupEllipseCenterX
            // 
            this.nupEllipseCenterX.Location = new System.Drawing.Point(6, 19);
            this.nupEllipseCenterX.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupEllipseCenterX.Name = "nupEllipseCenterX";
            this.nupEllipseCenterX.Size = new System.Drawing.Size(63, 20);
            this.nupEllipseCenterX.TabIndex = 13;
            this.nupEllipseCenterX.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // nupEllipseWidth
            // 
            this.nupEllipseWidth.Location = new System.Drawing.Point(6, 58);
            this.nupEllipseWidth.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupEllipseWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nupEllipseWidth.Name = "nupEllipseWidth";
            this.nupEllipseWidth.Size = new System.Drawing.Size(63, 20);
            this.nupEllipseWidth.TabIndex = 17;
            this.nupEllipseWidth.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // nupEllipseCenterY
            // 
            this.nupEllipseCenterY.Location = new System.Drawing.Point(75, 19);
            this.nupEllipseCenterY.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupEllipseCenterY.Name = "nupEllipseCenterY";
            this.nupEllipseCenterY.Size = new System.Drawing.Size(63, 20);
            this.nupEllipseCenterY.TabIndex = 14;
            this.nupEllipseCenterY.Value = new decimal(new int[] {
            250,
            0,
            0,
            0});
            // 
            // nupEllipseHeight
            // 
            this.nupEllipseHeight.Location = new System.Drawing.Point(75, 58);
            this.nupEllipseHeight.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupEllipseHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nupEllipseHeight.Name = "nupEllipseHeight";
            this.nupEllipseHeight.Size = new System.Drawing.Size(63, 20);
            this.nupEllipseHeight.TabIndex = 18;
            this.nupEllipseHeight.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // tpCircle
            // 
            this.tpCircle.Controls.Add(this.lblCircleRadius);
            this.tpCircle.Controls.Add(this.nupCircleRadius);
            this.tpCircle.Controls.Add(this.lblCircleCenterX);
            this.tpCircle.Controls.Add(this.nupCircleCenterX);
            this.tpCircle.Controls.Add(this.nupCircleCenterY);
            this.tpCircle.Controls.Add(this.lblCircleCenterY);
            this.tpCircle.Location = new System.Drawing.Point(4, 22);
            this.tpCircle.Name = "tpCircle";
            this.tpCircle.Size = new System.Drawing.Size(348, 86);
            this.tpCircle.TabIndex = 2;
            this.tpCircle.Text = "Окружность";
            this.tpCircle.UseVisualStyleBackColor = true;
            // 
            // lblCircleRadius
            // 
            this.lblCircleRadius.AutoSize = true;
            this.lblCircleRadius.Location = new System.Drawing.Point(3, 42);
            this.lblCircleRadius.Name = "lblCircleRadius";
            this.lblCircleRadius.Size = new System.Drawing.Size(46, 13);
            this.lblCircleRadius.TabIndex = 11;
            this.lblCircleRadius.Text = "Радиус:";
            // 
            // nupCircleRadius
            // 
            this.nupCircleRadius.Location = new System.Drawing.Point(6, 58);
            this.nupCircleRadius.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupCircleRadius.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nupCircleRadius.Name = "nupCircleRadius";
            this.nupCircleRadius.Size = new System.Drawing.Size(63, 20);
            this.nupCircleRadius.TabIndex = 9;
            this.nupCircleRadius.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // lblCircleCenterX
            // 
            this.lblCircleCenterX.AutoSize = true;
            this.lblCircleCenterX.Location = new System.Drawing.Point(3, 3);
            this.lblCircleCenterX.Name = "lblCircleCenterX";
            this.lblCircleCenterX.Size = new System.Drawing.Size(51, 13);
            this.lblCircleCenterX.TabIndex = 2;
            this.lblCircleCenterX.Text = "Центр X:";
            // 
            // nupCircleCenterX
            // 
            this.nupCircleCenterX.Location = new System.Drawing.Point(6, 19);
            this.nupCircleCenterX.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupCircleCenterX.Name = "nupCircleCenterX";
            this.nupCircleCenterX.Size = new System.Drawing.Size(63, 20);
            this.nupCircleCenterX.TabIndex = 0;
            this.nupCircleCenterX.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // nupCircleCenterY
            // 
            this.nupCircleCenterY.Location = new System.Drawing.Point(75, 19);
            this.nupCircleCenterY.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupCircleCenterY.Name = "nupCircleCenterY";
            this.nupCircleCenterY.Size = new System.Drawing.Size(63, 20);
            this.nupCircleCenterY.TabIndex = 1;
            this.nupCircleCenterY.Value = new decimal(new int[] {
            250,
            0,
            0,
            0});
            // 
            // lblCircleCenterY
            // 
            this.lblCircleCenterY.AutoSize = true;
            this.lblCircleCenterY.Location = new System.Drawing.Point(72, 3);
            this.lblCircleCenterY.Name = "lblCircleCenterY";
            this.lblCircleCenterY.Size = new System.Drawing.Size(51, 13);
            this.lblCircleCenterY.TabIndex = 8;
            this.lblCircleCenterY.Text = "Центр Y:";
            // 
            // tpCircleSpectre
            // 
            this.tpCircleSpectre.Controls.Add(this.lblCircleSpectreRadiusStep);
            this.tpCircleSpectre.Controls.Add(this.nupCircleSpectreRadiusStep);
            this.tpCircleSpectre.Controls.Add(this.lblCircleSpectreMinRadius);
            this.tpCircleSpectre.Controls.Add(this.nupCircleSpectreMinRadius);
            this.tpCircleSpectre.Controls.Add(this.nupCircleSpectreMaxRadius);
            this.tpCircleSpectre.Controls.Add(this.lblCircleSpectreMaxRadius);
            this.tpCircleSpectre.Location = new System.Drawing.Point(4, 22);
            this.tpCircleSpectre.Name = "tpCircleSpectre";
            this.tpCircleSpectre.Padding = new System.Windows.Forms.Padding(3);
            this.tpCircleSpectre.Size = new System.Drawing.Size(348, 86);
            this.tpCircleSpectre.TabIndex = 1;
            this.tpCircleSpectre.Text = "Спектр окружностей";
            this.tpCircleSpectre.UseVisualStyleBackColor = true;
            // 
            // lblCircleSpectreRadiusStep
            // 
            this.lblCircleSpectreRadiusStep.AutoSize = true;
            this.lblCircleSpectreRadiusStep.Location = new System.Drawing.Point(3, 42);
            this.lblCircleSpectreRadiusStep.Name = "lblCircleSpectreRadiusStep";
            this.lblCircleSpectreRadiusStep.Size = new System.Drawing.Size(74, 13);
            this.lblCircleSpectreRadiusStep.TabIndex = 15;
            this.lblCircleSpectreRadiusStep.Text = "Шаг радиуса:";
            // 
            // nupCircleSpectreRadiusStep
            // 
            this.nupCircleSpectreRadiusStep.Location = new System.Drawing.Point(6, 58);
            this.nupCircleSpectreRadiusStep.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupCircleSpectreRadiusStep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nupCircleSpectreRadiusStep.Name = "nupCircleSpectreRadiusStep";
            this.nupCircleSpectreRadiusStep.Size = new System.Drawing.Size(63, 20);
            this.nupCircleSpectreRadiusStep.TabIndex = 14;
            this.nupCircleSpectreRadiusStep.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblCircleSpectreMinRadius
            // 
            this.lblCircleSpectreMinRadius.AutoSize = true;
            this.lblCircleSpectreMinRadius.Location = new System.Drawing.Point(3, 3);
            this.lblCircleSpectreMinRadius.Name = "lblCircleSpectreMinRadius";
            this.lblCircleSpectreMinRadius.Size = new System.Drawing.Size(72, 13);
            this.lblCircleSpectreMinRadius.TabIndex = 12;
            this.lblCircleSpectreMinRadius.Text = "Мин. радиус:";
            // 
            // nupCircleSpectreMinRadius
            // 
            this.nupCircleSpectreMinRadius.Location = new System.Drawing.Point(6, 19);
            this.nupCircleSpectreMinRadius.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupCircleSpectreMinRadius.Name = "nupCircleSpectreMinRadius";
            this.nupCircleSpectreMinRadius.Size = new System.Drawing.Size(63, 20);
            this.nupCircleSpectreMinRadius.TabIndex = 10;
            this.nupCircleSpectreMinRadius.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // nupCircleSpectreMaxRadius
            // 
            this.nupCircleSpectreMaxRadius.Location = new System.Drawing.Point(75, 19);
            this.nupCircleSpectreMaxRadius.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupCircleSpectreMaxRadius.Name = "nupCircleSpectreMaxRadius";
            this.nupCircleSpectreMaxRadius.Size = new System.Drawing.Size(63, 20);
            this.nupCircleSpectreMaxRadius.TabIndex = 11;
            this.nupCircleSpectreMaxRadius.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // lblCircleSpectreMaxRadius
            // 
            this.lblCircleSpectreMaxRadius.AutoSize = true;
            this.lblCircleSpectreMaxRadius.Location = new System.Drawing.Point(72, 3);
            this.lblCircleSpectreMaxRadius.Name = "lblCircleSpectreMaxRadius";
            this.lblCircleSpectreMaxRadius.Size = new System.Drawing.Size(78, 13);
            this.lblCircleSpectreMaxRadius.TabIndex = 13;
            this.lblCircleSpectreMaxRadius.Text = "Макс. радиус:";
            // 
            // tpEllipseSpectre
            // 
            this.tpEllipseSpectre.Controls.Add(this.label2);
            this.tpEllipseSpectre.Controls.Add(this.nupEllipseSpectreMinHeight);
            this.tpEllipseSpectre.Controls.Add(this.nupEllipseSpectreMaxHeight);
            this.tpEllipseSpectre.Controls.Add(this.label4);
            this.tpEllipseSpectre.Controls.Add(this.label1);
            this.tpEllipseSpectre.Controls.Add(this.nupEllipseSpectreStepWidth);
            this.tpEllipseSpectre.Controls.Add(this.lblMinA);
            this.tpEllipseSpectre.Controls.Add(this.nupEllipseSpectreMinWidth);
            this.tpEllipseSpectre.Controls.Add(this.nupEllipseSpectreMaxWidth);
            this.tpEllipseSpectre.Controls.Add(this.label3);
            this.tpEllipseSpectre.Location = new System.Drawing.Point(4, 22);
            this.tpEllipseSpectre.Name = "tpEllipseSpectre";
            this.tpEllipseSpectre.Size = new System.Drawing.Size(348, 86);
            this.tpEllipseSpectre.TabIndex = 3;
            this.tpEllipseSpectre.Text = "Спектр эллипсов";
            this.tpEllipseSpectre.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(161, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Шаг ширины:";
            // 
            // nupEllipseSpectreStepWidth
            // 
            this.nupEllipseSpectreStepWidth.Location = new System.Drawing.Point(164, 19);
            this.nupEllipseSpectreStepWidth.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupEllipseSpectreStepWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nupEllipseSpectreStepWidth.Name = "nupEllipseSpectreStepWidth";
            this.nupEllipseSpectreStepWidth.Size = new System.Drawing.Size(63, 20);
            this.nupEllipseSpectreStepWidth.TabIndex = 20;
            this.nupEllipseSpectreStepWidth.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblMinA
            // 
            this.lblMinA.AutoSize = true;
            this.lblMinA.Location = new System.Drawing.Point(5, 3);
            this.lblMinA.Name = "lblMinA";
            this.lblMinA.Size = new System.Drawing.Size(72, 13);
            this.lblMinA.TabIndex = 18;
            this.lblMinA.Text = "Мин. ширина";
            // 
            // nupEllipseSpectreMinWidth
            // 
            this.nupEllipseSpectreMinWidth.Location = new System.Drawing.Point(8, 19);
            this.nupEllipseSpectreMinWidth.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupEllipseSpectreMinWidth.Name = "nupEllipseSpectreMinWidth";
            this.nupEllipseSpectreMinWidth.Size = new System.Drawing.Size(63, 20);
            this.nupEllipseSpectreMinWidth.TabIndex = 16;
            this.nupEllipseSpectreMinWidth.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // nupEllipseSpectreMaxWidth
            // 
            this.nupEllipseSpectreMaxWidth.Location = new System.Drawing.Point(77, 19);
            this.nupEllipseSpectreMaxWidth.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupEllipseSpectreMaxWidth.Name = "nupEllipseSpectreMaxWidth";
            this.nupEllipseSpectreMaxWidth.Size = new System.Drawing.Size(63, 20);
            this.nupEllipseSpectreMaxWidth.TabIndex = 17;
            this.nupEllipseSpectreMaxWidth.Value = new decimal(new int[] {
            400,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Макс. ширина:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Мин. высота";
            // 
            // nupEllipseSpectreMinHeight
            // 
            this.nupEllipseSpectreMinHeight.Location = new System.Drawing.Point(8, 60);
            this.nupEllipseSpectreMinHeight.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupEllipseSpectreMinHeight.Name = "nupEllipseSpectreMinHeight";
            this.nupEllipseSpectreMinHeight.Size = new System.Drawing.Size(63, 20);
            this.nupEllipseSpectreMinHeight.TabIndex = 22;
            this.nupEllipseSpectreMinHeight.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // nupEllipseSpectreMaxHeight
            // 
            this.nupEllipseSpectreMaxHeight.Location = new System.Drawing.Point(77, 60);
            this.nupEllipseSpectreMaxHeight.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nupEllipseSpectreMaxHeight.Name = "nupEllipseSpectreMaxHeight";
            this.nupEllipseSpectreMaxHeight.Size = new System.Drawing.Size(63, 20);
            this.nupEllipseSpectreMaxHeight.TabIndex = 23;
            this.nupEllipseSpectreMaxHeight.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(74, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Макс. высота:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1314, 552);
            this.Controls.Add(this.tcItems);
            this.Controls.Add(this.btnTimeAnaliz);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnImage);
            this.Controls.Add(this.lblAlgorithms);
            this.Controls.Add(this.cmbAlgotihms);
            this.Controls.Add(this.gpbColors);
            this.Controls.Add(this.pictureBox);
            this.Name = "Form1";
            this.Text = "lab3";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.gpbColors.ResumeLayout(false);
            this.gpbColors.PerformLayout();
            this.tcItems.ResumeLayout(false);
            this.tpEllipse.ResumeLayout(false);
            this.tpEllipse.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseCenterX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseCenterY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseHeight)).EndInit();
            this.tpCircle.ResumeLayout(false);
            this.tpCircle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleCenterX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleCenterY)).EndInit();
            this.tpCircleSpectre.ResumeLayout(false);
            this.tpCircleSpectre.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleSpectreRadiusStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleSpectreMinRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupCircleSpectreMaxRadius)).EndInit();
            this.tpEllipseSpectre.ResumeLayout(false);
            this.tpEllipseSpectre.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseSpectreStepWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseSpectreMinWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseSpectreMaxWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseSpectreMinHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupEllipseSpectreMaxHeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ColorDialog clrDialog;
        private System.Windows.Forms.GroupBox gpbColors;
        private System.Windows.Forms.Button btnColorDialog;
        private System.Windows.Forms.ComboBox cmbAlgotihms;
        private System.Windows.Forms.Label lblAlgorithms;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnTimeAnaliz;
        private System.Windows.Forms.RadioButton rbColor;
        private System.Windows.Forms.RadioButton rbColorBackground;
        private System.Windows.Forms.TabControl tcItems;
        private System.Windows.Forms.TabPage tpEllipse;
        private System.Windows.Forms.Label lblEllipseHeight;
        private System.Windows.Forms.Label lblEllipseCenterX;
        private System.Windows.Forms.Label lblEllipseWidth;
        private System.Windows.Forms.Label lblEllipseCenterY;
        private System.Windows.Forms.NumericUpDown nupEllipseCenterX;
        private System.Windows.Forms.NumericUpDown nupEllipseWidth;
        private System.Windows.Forms.NumericUpDown nupEllipseCenterY;
        private System.Windows.Forms.NumericUpDown nupEllipseHeight;
        private System.Windows.Forms.TabPage tpCircle;
        private System.Windows.Forms.Label lblCircleRadius;
        private System.Windows.Forms.NumericUpDown nupCircleRadius;
        private System.Windows.Forms.Label lblCircleCenterX;
        private System.Windows.Forms.NumericUpDown nupCircleCenterX;
        private System.Windows.Forms.NumericUpDown nupCircleCenterY;
        private System.Windows.Forms.Label lblCircleCenterY;
        private System.Windows.Forms.TabPage tpCircleSpectre;
        private System.Windows.Forms.Label lblCircleSpectreRadiusStep;
        private System.Windows.Forms.NumericUpDown nupCircleSpectreRadiusStep;
        private System.Windows.Forms.Label lblCircleSpectreMinRadius;
        private System.Windows.Forms.NumericUpDown nupCircleSpectreMinRadius;
        private System.Windows.Forms.NumericUpDown nupCircleSpectreMaxRadius;
        private System.Windows.Forms.Label lblCircleSpectreMaxRadius;
        private System.Windows.Forms.Button btnImage;
        private System.Windows.Forms.TabPage tpEllipseSpectre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nupEllipseSpectreStepWidth;
        private System.Windows.Forms.Label lblMinA;
        private System.Windows.Forms.NumericUpDown nupEllipseSpectreMinWidth;
        private System.Windows.Forms.NumericUpDown nupEllipseSpectreMaxWidth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nupEllipseSpectreMinHeight;
        private System.Windows.Forms.NumericUpDown nupEllipseSpectreMaxHeight;
        private System.Windows.Forms.Label label4;
    }
}

