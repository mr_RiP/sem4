﻿using System;

static class LinearSystemSolver
{
    public static double[] Solve(int n, double[][] a, double[] b)
    {
        for (int k = 0; k < n; ++k) {
            int m = k;
            for (int i = m + 1; i < n; ++i) {
                if (Math.Abs(a[i][k]) > Math.Abs(a[m][k])) {
                    m = i;
                }
            }
            if (k != m)
            {
                double[] at = a[k];
                a[k] = a[m];
                a[m] = at;
                double bt = b[k];
                b[k] = b[m];
                b[m] = bt;
            }
            if (a[k][k] == 0.0)
            {
                return null;
            }
            for (int i = k + 1; i < n; ++i)
            {
                double f = a[i][k] / a[k][k];
                for (int j = k; j < n; ++j)
                {
                    a[i][j] -= a[k][j] * f;
                }
                b[i] -= b[k] * f;
            }
        }
        double[] x = new double[n];
        for (int i = n - 1; i >= 0; --i) {
            double t = b[i];
            for (int j = n - 1; j > i; --j) {
                t -= a[i][j] * x[j];
            }
            x[i] = t / a[i][i];
        }
        return x;
    }
};