﻿using System;

namespace Mash_graph_lab3
{
    public delegate double Function(double x);

    public struct SamplePoint
    {
        public double x, y, rho;
    }

    public static class PolynomialApproximator
    {
        private static double EvaluatePolynomial(double x, double[] c)
        {
            if (c.Length == 0) {
                throw new ArgumentException("c must have nonzero length.");
            }
            double y = 0.0;
            for (int i = c.Length - 1; i >= 0; --i) {
                y = y * x + c[i];
            }
            return y;
        }

        public static Function Approximate(int polynomialOrder, SamplePoint[] sample)
        {
            if (sample.Length + 1 < polynomialOrder)
            {
                return null;
            }
            int n = polynomialOrder + 1;
            double[][] a = new double[n][];
            double[] b = new double[n];
            for (int i = 0; i < n; ++i) {
                a[i] = new double[n];
                for (int j = 0; j < n; ++j) {
                    for (int k = 0; k < sample.Length; ++k) {
                        a[i][j] += sample[k].rho * Math.Pow(sample[k].x, i + j);
                        //a[i][j] += sample[k].rho * Math.Sin(sample[k].x*(i+j));//Math.Pow(sample[k].x, i + j);
                    }
                }
                for (int k = 0; k < sample.Length; ++k) {
                    b[i] += sample[k].rho * sample[k].y * Math.Pow(sample[k].x, i);
                    //b[i] += sample[k].rho * sample[k].y *Math.Sin(sample[k].x*i);
                }
            }
            double[] c = LinearSystemSolver.Solve(n, a, b);
            return x => EvaluatePolynomial(x, c);
        }
    }
}