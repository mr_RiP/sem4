﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mash_graph_lab3
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            Point[][] table = (Point[][])this.Tag;
            for (int j=0; j<4; j++)
            {
                var plotSeriesFunctionPoints = chrtPlot.Series[j].Points;
                plotSeriesFunctionPoints.Clear();
                SamplePoint[] points = new SamplePoint[200];
                for (int i=0; i<200; i++)
                {
                    points[i].x = table[j][i].X ;
                    points[i].y = table[j][i].Y;
                    points[i].rho = 1;
                }
                Function f = PolynomialApproximator.Approximate(2, points);
                for (int i=1; i<200; i++)
                    plotSeriesFunctionPoints.AddXY(i, f(i));
            }
            
        }
    }
}
