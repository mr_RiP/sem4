﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Mash_graph_lab3
{
    public partial class Form1 : Form
    {
        Graphics currentGraphics;
        Brush brushForUsual;
        BufferedGraphics buffer;    // класс буффера (чтобы рендерить)
        BufferedGraphicsContext bufferContext;     // класс для создания буффера
        Pen penForUsual;
        Brush brushForClear;


        public Form1()
        {
            InitializeComponent();
            currentGraphics = pictureBox.CreateGraphics();
            bufferContext = new BufferedGraphicsContext();
            buffer = bufferContext.Allocate(currentGraphics, pictureBox.ClientRectangle);
            brushForUsual = new SolidBrush(Color.Black);
            brushForClear = new SolidBrush(Color.White);
            penForUsual = new Pen(Color.Black);
            clearPB();
            cmbAlgotihms.SelectedIndex = 0;
        }

        public void clearPB()
        {
            buffer.Graphics.FillRectangle(brushForClear, 0, 0, 900, 550);
            buffer.Render(currentGraphics);
        }

        private void btnCLear_Click(object sender, EventArgs e)
        {
            buffer.Graphics.FillRectangle(brushForClear, pictureBox.ClientRectangle);
            buffer.Render(currentGraphics);
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Реализовать алгоритмы изображения эллипса и окружности. Размер прямоугольника 900*550 пикселей. OY вниз. Начало координат в (0,0)");
        }

        public void drawPoint(double xt, double yt)
        {
            buffer.Graphics.FillRectangle(brushForUsual, (int)Math.Round(xt, MidpointRounding.AwayFromZero), (int)Math.Round(yt, MidpointRounding.AwayFromZero), 1, 1);
        }

        public int drawEllipseParametric(Point center, int width, int height)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            double a = width / 2.0, b = height / 2.0;
            double xt = 0;
            double yt = 0;
            double d = 1.0 / Math.Max(a, b);
            for (double t = Math.PI / 2.0; t >= 0; t -= d)
            {

                xt = a * Math.Cos(t);
                yt = b * Math.Sin(t);
                timer.Stop();
                drawPoint(center.X + xt, center.Y + yt);
                drawPoint(center.X - xt, center.Y + yt);
                drawPoint(center.X + xt, center.Y - yt);
                drawPoint(center.X - xt, center.Y - yt);
                timer.Start();
            }
            timer.Stop();
            buffer.Render(currentGraphics);
            return (int)timer.ElapsedTicks;
        }

        public int drawEllipseEquation(Point center, int width, int height)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            double a = width / 2.0, b = height / 2.0;
            double a2 = a * a, b2 = b * b;
            double border = a * a * Math.Sqrt(1 / (a * a + b * b));
            double xt = 0, yt = b;
            for (; xt < border; xt += 1, yt = Math.Sqrt(a * a * b * b - xt * xt * b * b) / a)
            {
                timer.Stop();
                drawPoint(center.X + xt, center.Y + yt);
                drawPoint(center.X - xt, center.Y + yt);
                drawPoint(center.X + xt, center.Y - yt);
                drawPoint(center.X - xt, center.Y - yt);
                timer.Start();
            }
            for (; yt > 0; yt -= 1, xt = Math.Sqrt(a * a * b * b - yt * yt * a * a) / b)
            {
                timer.Stop();
                drawPoint(center.X + xt, center.Y + yt);
                drawPoint(center.X - xt, center.Y + yt);
                drawPoint(center.X + xt, center.Y - yt);
                drawPoint(center.X - xt, center.Y - yt);
                timer.Start();
            }
            timer.Stop();
            buffer.Render(currentGraphics);
            return (int)timer.ElapsedTicks;
        }

        public int drawEllipseMiddlePoint(Point center, int width, int height)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            double a = (width) / 2.0;
            double b = (height) / 2.0;
            double ab2 = a * a * b * b,
                a2 = a * a,
                b2 = b * b;
            double xt = 0;
            double yt = b;
            double delta = b2 * (xt + 1) * (xt + 1) + a2 * (yt - 0.5) * (yt - 0.5) - ab2;
            double border = a2 / Math.Sqrt(b2 + a2);
            while (xt <= border)
            {
                //delta = b2 * (xt + 1) * (xt + 1) + a2 * (yt - 0.5) * (yt - 0.5) - ab2;
                timer.Stop();
                drawPoint(center.X + xt, center.Y + yt);
                drawPoint(center.X - xt, center.Y + yt);
                drawPoint(center.X + xt, center.Y - yt);
                drawPoint(center.X - xt, center.Y - yt);
                timer.Start();
                xt++;

                if (delta > 0)
                {
                    
                    yt--;
                    delta += a2 * (-2 * yt);

                }
                delta += b2 * (2 * xt + 1);

            }
            delta += 0.75 * (a2 - b2) - (b2 * xt + a2 * yt);
            while (yt >= 0)
            {
                //delta = b2 * (xt + 0.5) * (xt + 0.5) + a2 * (yt - 1) * (yt - 1) - ab2;
                timer.Stop();
                drawPoint(center.X + xt, center.Y + yt);
                drawPoint(center.X - xt, center.Y + yt);
                drawPoint(center.X + xt, center.Y - yt);
                drawPoint(center.X - xt, center.Y - yt);
                timer.Start();
                yt--;
                if (delta < 0)
                {
                    
                    xt++;
                    delta += b2 * (2 * xt);
                   
                }
                delta += a2 * (-2 * yt + 1);
            }
            timer.Stop();
            buffer.Render(currentGraphics);
            return (int)timer.ElapsedTicks;
        }

        #region Брезенхейм и все с ним связанное
        private void gorizontalStep(ref int delta, ref int delta1, ref int delta2, int xt, int yt, int a2, int b2)
        {
            int d = b2 * (2 * xt + 1);
            delta += d;
            delta1 += d;
            delta2 += d;
        }

        private void verticalStep(ref int delta, ref int delta1, ref int delta2, int xt, int yt, int a2, int b2)
        {
            int d = a2 * (-2 * yt + 1);
            delta += d;
            delta1 += d;
            delta2 += d;
        }

        public delegate void Step(ref int delta, ref int delta1, ref int delta2, int xt, int yt, int a2, int b2);

        public int drawEllipseBrezenheim(Point center, int width, int height)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            double a = (width) / 2.0;
            double b = (height) / 2.0;
            double ab2 = a * a * b * b,
                a2 = a * a,
                b2 = b * b;
            double xt = 0;
            double yt = b;
            double delta = b2 + (b - 1) * (b - 1) * a2 - ab2,
                delta1 = b2 + b2 * a2 - ab2,
                delta2 = (b - 1) * (b - 1) * a2 - ab2;
            bool stepGorizontal, stepVertical;
            double d;
            while (yt >= 0)
            {
                timer.Stop();
                drawPoint(center.X + xt , center.Y + yt );
                drawPoint(center.X - xt, center.Y + yt );
                drawPoint(center.X + xt , center.Y - yt);
                drawPoint(center.X - xt, center.Y - yt );
                timer.Start();
                stepGorizontal = false;
                stepVertical = false;
                if (delta < 0)
                {
                    xt++;
                    stepGorizontal = true;
                    if (Math.Abs(delta1) - Math.Abs(delta) > 0)
                    {
                        yt--;
                        stepVertical = true;
                    }
                }
                else
                if (delta > 0)
                {
                    yt--;
                    stepVertical = true;
                    if (-Math.Abs(delta2) + Math.Abs(delta) < 0)
                    {
                        xt++;
                        stepGorizontal = true;
                    }
                }
                else
                    if (delta == 0)
                    {
                        xt++;
                        yt--;
                        stepVertical = true;
                        stepGorizontal = true;
                    }
                if (stepGorizontal == true)
                {
                    d = b2 * (2 * xt + 1);
                    delta += d;
                    delta1 += d;
                    delta2 += d - 2 * b2;
                }
                if (stepVertical == true)
                {
                    d = a2 * (-2 * yt + 1);
                    delta += d;
                    delta1 += d - 2 * a2;
                    delta2 += d;
                }
            }
            timer.Stop();
            buffer.Render(currentGraphics);
            return (int)timer.ElapsedTicks;
        }
        #endregion

        // панель цветов
        private void btnColorDialog_Click(object sender, EventArgs e)
        {
            clrDialog.Color = btnColorDialog.BackColor;
            clrDialog.ShowDialog();
            penForUsual.Color = clrDialog.Color;
            (brushForUsual as SolidBrush).Color = clrDialog.Color;
            btnColorDialog.ForeColor = clrDialog.Color;
            btnColorDialog.BackColor = clrDialog.Color;
            btnColorDialog.FlatAppearance.MouseOverBackColor = clrDialog.Color;
            btnColorDialog.FlatAppearance.MouseDownBackColor = clrDialog.Color;
            rbColor.Checked = true;
        }

        private void rbColorBackground_CheckedChanged(object sender, EventArgs e)
        {
            if (rbColorBackground.Checked == true)
            {
                penForUsual.Color = Color.White;
                (brushForUsual as SolidBrush).Color = Color.White;
            }
        }

        private void rbColor_CheckedChanged(object sender, EventArgs e)
        {
            if (rbColor.Checked == true)
            {
                penForUsual.Color = btnColorDialog.ForeColor;
                (brushForUsual as SolidBrush).Color = btnColorDialog.ForeColor;
            }
        }

        private void btnImage_Click(object sender, EventArgs e)
        {
            int x = 0, y = 0, width = 0, height = 0, minR = 0, maxR = 0, deltaR = 0, minWidth = 0, maxWidth = 0, minHeigth = 0, maxHeigth = 0, deltaWidth = 0; 
            try
            {
                switch (tcItems.SelectedIndex)
                {
                    case 0:
                        x = (int)nupEllipseCenterX.Value;
                        y = (int)nupEllipseCenterY.Value;
                        width = (int)nupEllipseWidth.Value;
                        height = (int)nupEllipseHeight.Value;
                        break;
                    case 1:
                        x = (int)nupCircleCenterX.Value;
                        y = (int)nupCircleCenterY.Value;
                        width = (int)nupCircleRadius.Value*2;
                        height = width;
                        break;
                    case 2:
                        minR = (int)nupCircleSpectreMinRadius.Value;
                        maxR = (int)nupCircleSpectreMaxRadius.Value;
                        deltaR = (int)nupCircleSpectreRadiusStep.Value;
                        break;
                    case 3:
                        minWidth = (int)nupEllipseSpectreMinWidth.Value;
                        maxWidth = (int)nupEllipseSpectreMaxWidth.Value;
                        minHeigth = (int)nupEllipseSpectreMinHeight.Value;
                        maxHeigth = (int)nupEllipseSpectreMaxHeight.Value;
                        deltaWidth = (int)nupEllipseSpectreStepWidth.Value;
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Неверные данные введены!");
            }
            switch (tcItems.SelectedIndex)
            {
                case 0:
                case 1:
                    if (width > 0 && height > 0)
                    {
                        switch (cmbAlgotihms.SelectedIndex)
                        {
                            case 0:
                                drawEllipseBrezenheim(new Point(x, y), width, height);
                                break;
                            case 1:
                                drawEllipseMiddlePoint(new Point(x, y), width, height);
                                break;
                            case 2:
                                drawEllipseEquation(new Point(x, y), width, height);
                                break;
                            case 3:
                                drawEllipseParametric(new Point(x, y), width, height);
                                break;
                            case 4:
                                buffer.Graphics.DrawEllipse(penForUsual, x - width / 2, y - height / 2, width, height);
                                buffer.Render(currentGraphics);
                                break;
                        }
                    }
                    break;
                case 2:
                    for (int i = minR; i < maxR; i += deltaR)
                    {
                        switch (cmbAlgotihms.SelectedIndex)
                        {
                            case 0:
                                drawEllipseBrezenheim(new Point(500, 250), i * 2, i * 2);
                                break;
                            case 1:
                                drawEllipseMiddlePoint(new Point(500, 250), i * 2, i * 2);
                                break;
                            case 2:
                                drawEllipseEquation(new Point(500, 250), i*2, i*2);
                                break;
                            case 3:
                                drawEllipseParametric(new Point(500, 250), i * 2, i * 2);
                                break;
                            case 4:
                                buffer.Graphics.DrawEllipse(penForUsual, 500 -  i, 250 - i, i*2, i*2);
                                buffer.Render(currentGraphics);
                                break;
                        }
                    }
                    break;
                case 3:
                    double deltaHeight = (deltaWidth+0.0)/(maxWidth-minWidth)*(maxHeigth-minHeigth);
                    for (double i = minWidth, j = minHeigth; i < maxWidth && j<maxHeigth; i += deltaWidth, j+=deltaHeight)
                    {
                        switch (cmbAlgotihms.SelectedIndex)
                        {
                            case 0:
                                drawEllipseBrezenheim(new Point(500, 250), (int)i, (int)j);
                                break;
                            case 1:
                                drawEllipseMiddlePoint(new Point(500, 250), (int)i, (int)j);
                                break;
                            case 2:
                                drawEllipseEquation(new Point(500, 250), (int)i, (int)j);
                                break;
                            case 3:
                                drawEllipseParametric(new Point(500, 250), (int)i, (int)j);
                                break;
                            case 4:
                                buffer.Graphics.DrawEllipse(penForUsual, 500 - (int)i, 250 - (int)i, (int)i, (int)j);
                                buffer.Render(currentGraphics);
                                break;
                        }
                    }
                    break;
            }

        }

        public delegate int DrawEllipseAlgorithm(Point center, int width, int height);

        private void btnTimeAnaliz_Click(object sender, EventArgs e)
        {
            Point[][] table = new Point[4][];
            DrawEllipseAlgorithm drawEllipse = null;
            for (int j = 0; j < 4; j++)
            {
                switch (j)
                {
                    case 0:
                        drawEllipse = drawEllipseBrezenheim;
                        break;
                    case 1:
                        drawEllipse = drawEllipseMiddlePoint;
                        break;
                    case 2:
                        drawEllipse = drawEllipseEquation;
                        break;
                }
                table[j] = new Point[200];
                for (int i = 1; i < 200; i++)
                {
                    table[j][i].X = i;
                    table[j][i].Y = 0;
                    for (int k = 0; k < 10; k++ )
                        table[j][i].Y += drawEllipse(new Point(500, 250), i * 2, i * 2);
                    table[j][i].Y /=10;
                }
            }
            Form2 frmGraph = new Form2();
            frmGraph.Tag = table;
            frmGraph.Show();
        }
    }
}
