﻿namespace Mash_graph_lab5
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.fillSortedEdgeListButton = new System.Windows.Forms.Button();
            this.deleteAllPointsButton = new System.Windows.Forms.Button();
            this.fillFloodButton = new System.Windows.Forms.Button();
            this.addPointButton = new System.Windows.Forms.Button();
            this.yLabel = new System.Windows.Forms.Label();
            this.xLabel = new System.Windows.Forms.Label();
            this.yNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.xNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.canvasPictureBox = new System.Windows.Forms.PictureBox();
            this.lblCursor = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.yNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // fillSortedEdgeListButton
            // 
            this.fillSortedEdgeListButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fillSortedEdgeListButton.Location = new System.Drawing.Point(724, 112);
            this.fillSortedEdgeListButton.Name = "fillSortedEdgeListButton";
            this.fillSortedEdgeListButton.Size = new System.Drawing.Size(150, 52);
            this.fillSortedEdgeListButton.TabIndex = 17;
            this.fillSortedEdgeListButton.Text = "Закрасить алгоритмом с упорядоченным списком ребер";
            this.fillSortedEdgeListButton.UseVisualStyleBackColor = true;
            this.fillSortedEdgeListButton.Click += new System.EventHandler(this.fillSortedEdgeListButton_Click);
            // 
            // deleteAllPointsButton
            // 
            this.deleteAllPointsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteAllPointsButton.Location = new System.Drawing.Point(724, 83);
            this.deleteAllPointsButton.Name = "deleteAllPointsButton";
            this.deleteAllPointsButton.Size = new System.Drawing.Size(150, 23);
            this.deleteAllPointsButton.TabIndex = 16;
            this.deleteAllPointsButton.Text = "Удалить все точки";
            this.deleteAllPointsButton.UseVisualStyleBackColor = true;
            this.deleteAllPointsButton.Click += new System.EventHandler(this.deleteAllPointsButton_Click);
            // 
            // fillFloodButton
            // 
            this.fillFloodButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fillFloodButton.Location = new System.Drawing.Point(724, 170);
            this.fillFloodButton.Name = "fillFloodButton";
            this.fillFloodButton.Size = new System.Drawing.Size(150, 35);
            this.fillFloodButton.TabIndex = 15;
            this.fillFloodButton.Text = "Закрасить затравкой";
            this.fillFloodButton.UseVisualStyleBackColor = true;
            this.fillFloodButton.Click += new System.EventHandler(this.fillFloodButton_Click);
            // 
            // addPointButton
            // 
            this.addPointButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addPointButton.Location = new System.Drawing.Point(724, 54);
            this.addPointButton.Name = "addPointButton";
            this.addPointButton.Size = new System.Drawing.Size(150, 23);
            this.addPointButton.TabIndex = 14;
            this.addPointButton.Text = "Добавить точку";
            this.addPointButton.UseVisualStyleBackColor = true;
            this.addPointButton.Click += new System.EventHandler(this.addPointButton_Click);
            // 
            // yLabel
            // 
            this.yLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.yLabel.AutoSize = true;
            this.yLabel.Location = new System.Drawing.Point(802, 12);
            this.yLabel.Name = "yLabel";
            this.yLabel.Size = new System.Drawing.Size(17, 13);
            this.yLabel.TabIndex = 13;
            this.yLabel.Text = "Y:";
            // 
            // xLabel
            // 
            this.xLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xLabel.AutoSize = true;
            this.xLabel.Location = new System.Drawing.Point(724, 12);
            this.xLabel.Name = "xLabel";
            this.xLabel.Size = new System.Drawing.Size(17, 13);
            this.xLabel.TabIndex = 12;
            this.xLabel.Text = "X:";
            // 
            // yNumericUpDown
            // 
            this.yNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.yNumericUpDown.Location = new System.Drawing.Point(802, 28);
            this.yNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.yNumericUpDown.Name = "yNumericUpDown";
            this.yNumericUpDown.Size = new System.Drawing.Size(72, 20);
            this.yNumericUpDown.TabIndex = 11;
            this.yNumericUpDown.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            // 
            // xNumericUpDown
            // 
            this.xNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xNumericUpDown.Location = new System.Drawing.Point(724, 28);
            this.xNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.xNumericUpDown.Name = "xNumericUpDown";
            this.xNumericUpDown.Size = new System.Drawing.Size(72, 20);
            this.xNumericUpDown.TabIndex = 10;
            this.xNumericUpDown.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // canvasPictureBox
            // 
            this.canvasPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.canvasPictureBox.BackColor = System.Drawing.Color.White;
            this.canvasPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.canvasPictureBox.Location = new System.Drawing.Point(12, 12);
            this.canvasPictureBox.Name = "canvasPictureBox";
            this.canvasPictureBox.Size = new System.Drawing.Size(700, 350);
            this.canvasPictureBox.TabIndex = 9;
            this.canvasPictureBox.TabStop = false;
            this.canvasPictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvasPictureBox_MouseMove);
            this.canvasPictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvasPictureBox_MouseUp);
            // 
            // lblCursor
            // 
            this.lblCursor.AutoSize = true;
            this.lblCursor.Location = new System.Drawing.Point(12, 370);
            this.lblCursor.Name = "lblCursor";
            this.lblCursor.Size = new System.Drawing.Size(0, 13);
            this.lblCursor.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 392);
            this.Controls.Add(this.lblCursor);
            this.Controls.Add(this.fillSortedEdgeListButton);
            this.Controls.Add(this.deleteAllPointsButton);
            this.Controls.Add(this.fillFloodButton);
            this.Controls.Add(this.addPointButton);
            this.Controls.Add(this.yLabel);
            this.Controls.Add(this.xLabel);
            this.Controls.Add(this.yNumericUpDown);
            this.Controls.Add(this.xNumericUpDown);
            this.Controls.Add(this.canvasPictureBox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.yNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button fillSortedEdgeListButton;
        private System.Windows.Forms.Button deleteAllPointsButton;
        private System.Windows.Forms.Button fillFloodButton;
        private System.Windows.Forms.Button addPointButton;
        private System.Windows.Forms.Label yLabel;
        private System.Windows.Forms.Label xLabel;
        private System.Windows.Forms.NumericUpDown yNumericUpDown;
        private System.Windows.Forms.NumericUpDown xNumericUpDown;
        private System.Windows.Forms.PictureBox canvasPictureBox;
        private System.Windows.Forms.Label lblCursor;

    }
}

