﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mash_graph_lab5
{
    public partial class Form1 : Form
    {
        List<Point> polygon;
        List<int> border;
        BufferedGraphics buffer;    // класс буффера (чтобы рендерить)
        BufferedGraphicsContext bufferContext;     // класс для создания буффера
        Graphics currentGraphics;
        Pen penForLines;
        Brush brushForClear;
        
        public Form1()
        {
            InitializeComponent();
            polygon = new List<Point>();
            border = new List<int>();
            currentGraphics = canvasPictureBox.CreateGraphics();
            bufferContext = new BufferedGraphicsContext();
            buffer = bufferContext.Allocate(currentGraphics, canvasPictureBox.ClientRectangle);
            penForLines = new Pen(Color.Black, 1);
            brushForClear = new SolidBrush(Color.White);
        }

        private Bitmap drawPolygon()
        {
            Bitmap bitmap = new Bitmap(canvasPictureBox.Width, canvasPictureBox.Height);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.FillRectangle(brushForClear, canvasPictureBox.ClientRectangle);
                int first = 0;
                for (int i = 0; i < polygon.Count-1; ++i)
                {
                    if (border.Contains(i))
                    {
                        graphics.DrawLine(Pens.Black, polygon[i], polygon[first]);
                        first = i+1;
                    }
                    else
                        graphics.DrawLine(Pens.Black, polygon[i], polygon[i+1]);
                }
                if (border.Contains(polygon.Count-1))
                {
                    graphics.DrawLine(Pens.Black, polygon[polygon.Count-1], polygon[first]);
                    first = 0;
                }
            }
            return bitmap;
        }


        private void canvasPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            lblCursor.Text = "Курсор: <" + e.X + "," + e.Y + ">";
        }

        private void deleteAllPointsButton_Click(object sender, EventArgs e)
        {
            polygon.Clear();
            border.Clear();
            canvasPictureBox.Image = drawPolygon();
        }

        private void addPointButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!polygon.Contains(new Point((int)xNumericUpDown.Value, (int)yNumericUpDown.Value)))
                    polygon.Add(new Point((int)xNumericUpDown.Value, (int)yNumericUpDown.Value));
                canvasPictureBox.Image =  drawPolygon();
            }
            catch (System.Exception ex)
            {

            }
        }

        struct ActiveEdge
        {
            public int y, x, deltaX, deltaY;
            public ActiveEdge(int y, int x, int deltaX, int deltaY)
            {
                this.y = y;
                this.x = x;
                this.deltaX = deltaX;
                this.deltaY = deltaY;
            }
        }

        private List<ActiveEdge> getActiveEdgeList()
        {
            List<ActiveEdge> res = new List<ActiveEdge>();
            int y, x, deltaX, deltaY;
            int max = polygon.Max(p => p.X);
            int fi, si;
            int first = 0;
            for (int i = 0; i < polygon.Count; i++)
            {
                if (!border.Contains(i))
                {
                    fi = i;
                    si = i + 1;
                    if (Math.Min(polygon[i].Y, polygon[i + 1].Y) == polygon[i + 1].Y)
                    {
                        fi = i + 1;
                        si = i;
                    }
                }
                else
                {
                    fi = i;
                    si = first;
                    if (Math.Min(polygon[i].Y, polygon[first].Y) == polygon[first].Y)
                    {
                        fi = first;
                        si = i;
                    }
                    first = border[border.BinarySearch(i)] + 1; ;

                }
                y = polygon[fi].Y;
                x = polygon[fi].X;
                deltaX = -polygon[fi].X + polygon[si].X;
                deltaY = -polygon[fi].Y + polygon[si].Y;
                res.Add(new ActiveEdge(y, x, deltaX, deltaY));
            }

            res.OrderBy(p1 => p1.y).ThenBy(p2 => p2.x);
            return res;
        }

        private bool isExtremum(Point p)
        {

            int findIndex = -1;
            int i;
            for (i = 0; i < polygon.Count && p != polygon[i]; i++) ;
            if (i != polygon.Count)
                findIndex = i;



            int pre = findIndex - 1, post = findIndex + 1;

            if (findIndex == 0)
                pre = border[0];

            int buf = border.BinarySearch(findIndex);
            if (buf>0)
            {
                post = border[buf - 1]+1;
            }
            if (buf == 0)
                post = 0;

            buf = border.BinarySearch(findIndex-1);
            if (buf>=0)
            {
                pre = border[buf + 1];
            }

            if (findIndex < 0)
                return false;

            if ((polygon[findIndex].Y > polygon[pre].Y && polygon[findIndex].Y > polygon[post].Y)
                || (polygon[findIndex].Y < polygon[pre].Y && polygon[findIndex].Y < polygon[post].Y))
                return true;

            return false;
        }

        private List<int> getIntersectionPoints(int lineY, List<ActiveEdge> actEdgeList)
        {
            List<int> res = new List<int>();
            for (int i = 0; i < actEdgeList.Count; i++)
                if (actEdgeList[i].deltaY != 0)
                {
                    double k = (lineY - actEdgeList[i].y + 0.0) / (actEdgeList[i].deltaY);
                    if (k <= 1 && k >= 0)
                    {
                        int interX = (int)Math.Round(actEdgeList[i].x + k * actEdgeList[i].deltaX);
                        Point bufP = new Point(interX, lineY);
                        if (!polygon.Contains(bufP))
                            res.Add(interX);
                        else
                            if (!res.Contains(interX))
                                res.Add(interX);
                            else
                                if (isExtremum(bufP))
                                    res.Add(interX);
                    }

                }
            return res;
        }

        private void fillSortedEdgeListButton_Click(object sender, EventArgs e)
        {
            try
            {
                canvasPictureBox.Image = drawPolygon();
                Bitmap bitmap = new Bitmap(canvasPictureBox.Width, canvasPictureBox.Height);
                List<ActiveEdge> actEdgeList = getActiveEdgeList();
                int max = polygon.Max(p => p.Y), min = polygon.Min(p => p.Y);
                List<int> intersectionList;

                bitmap = canvasPictureBox.Image as Bitmap;
                for (int i = min; i < max; i++)
                {
                    intersectionList = getIntersectionPoints(i, actEdgeList);
                    intersectionList.Sort();
                    for (int j = 0; j < intersectionList.Count; j += 2)
                    {
                        for (int k = intersectionList[j] + 1; k < intersectionList[j + 1]; k++)
                        {
                            bitmap.SetPixel(k, i, Color.Black);
                        }
                    }
                }
                canvasPictureBox.Image = bitmap;
                canvasPictureBox.Refresh();
            }
            catch (System.Exception ex)
            {

            }

        }

        private void fillFloodButton_Click(object sender, EventArgs e)
        {
            Stack<Point> stack = new Stack<Point>();
            canvasPictureBox.Image = drawPolygon();
            try
            {
                stack.Push(new Point((int)xNumericUpDown.Value, (int)yNumericUpDown.Value));

            }
            catch (System.Exception ex)
            {

            }
            Bitmap bitmap = canvasPictureBox.Image as Bitmap;
            Color borderColor = Color.Black;
            Color fillColor = Color.Red;
            Color backgroundColor = Color.White;
            bitmap = canvasPictureBox.Image as Bitmap;
            while (stack.Count != 0)
            {
                Point point = stack.Pop();
                int xl = point.X, xr = point.X;
                while (bitmap.GetPixel(xl, point.Y).ToArgb() != borderColor.ToArgb() && xl >0)
                {
                    //bitmap = canvasPictureBox.Image as Bitmap;
                    bitmap.SetPixel(xl, point.Y, fillColor);
                    //canvasPictureBox.Image = bitmap;
                    //canvasPictureBox.Refresh();
                    xl--;
                }
                xl++;
                while (bitmap.GetPixel(xr, point.Y).ToArgb() != borderColor.ToArgb() && xr < canvasPictureBox.Width-1)
                {
                    //bitmap = canvasPictureBox.Image as Bitmap;
                    bitmap.SetPixel(xr, point.Y, fillColor);
                    //canvasPictureBox.Image = bitmap;
                    //canvasPictureBox.Refresh();
                    xr++;
                }
                xr--;
                bool isColorChanged = true;
                bool canSet=true;
                if (point.Y>0)
                for (int i=xl; i<=xr; i++)
                {
                    isColorChanged = bitmap.GetPixel(i, point.Y - 1).ToArgb() != backgroundColor.ToArgb(); 
                    if (!isColorChanged && canSet)
                    {
                        stack.Push(new Point(i, point.Y - 1));
                        canSet = false;
                    }
                    if (isColorChanged)
                        canSet = true;
                }
                canSet = true;
                if (point.Y < canvasPictureBox.Height-1)
                for (int i = xl; i <= xr; i++)
                {
                    isColorChanged = bitmap.GetPixel(i, point.Y + 1).ToArgb() != backgroundColor.ToArgb();
                    if (!isColorChanged && canSet)
                    {
                        stack.Push(new Point(i, point.Y + 1));
                        canSet = false;
                    }
                    if (isColorChanged)
                        canSet = true;
                }
            }
            canvasPictureBox.Image = bitmap;
            canvasPictureBox.Refresh();
        }

        bool firstClick = false;
        bool doubleClick = false;
        Point prev;
        
        private void canvasPictureBox_MouseUp(object sender, MouseEventArgs e)
        {

            if (!polygon.Contains(new Point(e.X, e.Y)))
                polygon.Add(new Point(e.X, e.Y));
            
            if (prev.X == e.X && prev.Y == e.Y)
            {
                border.Add(polygon.Count-1);
            }

            canvasPictureBox.Image = drawPolygon();
            prev.X = e.X;
            prev.Y = e.Y;
        }

    }
}
