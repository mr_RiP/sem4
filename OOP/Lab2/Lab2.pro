TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    myset.h \
    mylistiterator.h \
    abstractlistiterator.h \
    mynode.h \
    mylist.h

