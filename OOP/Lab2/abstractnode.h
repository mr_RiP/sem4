#ifndef ABSTRACTNODE_H
#define ABSTRACTNODE_H

template <typename T>
class AbstractNode
{
public:
    virtual T& getData() = 0;
    virtual AbstractNode<T>* getNext() = 0;
    virtual void setData(const T& data) = 0;
    virtual void setNext(const AbstractNode<T>* next) = 0;
};



#endif // ABSTRACTNODE_H

