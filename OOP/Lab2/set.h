#ifndef SET_H
#define SET_H


template <typename DT>
class Set
{
public:
    Set();
    ~Set();

protected:
    template <typename DT>
    struct node
    {
        DT data;
        struct node <DT> *next;
    };


};

#endif // SET_H
