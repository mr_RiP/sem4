#ifndef MYNODE_H
#define MYNODE_H

#include <exception>

template <typename T>
class MyNode
{
public:
    MyNode();
    MyNode(const T& data, MyNode<T> *next = 0);
    MyNode(const MyNode<T>& node);
    ~MyNode();
    inline T getData() const;
    inline T* getDataPtr() const;
    inline MyNode<T>* getNext() const;
    inline void setData(const T& data);
    inline void setNext(MyNode<T> *next);
    inline void setDataPtr(T* ptr);

    class null_data : public std::exception
    {
        const char* what() const throw()
        {
            return "Trying to get data from null data pointer\n";
        }
    };

protected:
    T* _data;
    MyNode<T> *_next;
};

template <typename T>
MyNode<T>::MyNode()
{
    _data = 0;
    _next = 0;
}

template <typename T>
MyNode<T>::MyNode(const T& data, MyNode<T> *next)
{
    _next = next;
    _data = 0;

    _data = new T;
    *_data = data;
}

template <typename T>
MyNode<T>::MyNode(const MyNode<T>& node)
{
    _next = 0;
    _data = 0;

    _data = new T;
    *_data = *node.getDataPtr();
}

template <typename T>
MyNode<T>::~MyNode()
{
    if (_data != 0)
        delete _data;
}

template <typename T>
T MyNode<T>::getData() const
{
    if (_data == 0)
    {
        null_data error;
        throw error;
    }

    return *_data;
}

template <typename T>
MyNode<T> *MyNode<T>::getNext() const
{
    return _next;
}

template <typename T>
void MyNode<T>::setData(const T& data)
{
    if (_data == 0)
        _data = new T;
    _data = data;
}

template <typename T>
void MyNode<T>::setNext(MyNode<T> *next)
{
    _next = next;
}

template <typename T>
T* MyNode<T>::getDataPtr() const
{
    return _data;
}

template <typename T>
void MyNode<T>::setDataPtr(T *ptr)
{
    _data = ptr;
}

#endif // MYNODE_H
