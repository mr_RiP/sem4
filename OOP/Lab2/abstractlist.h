#ifndef ABSTRACTLIST_H
#define ABSTRACTLIST_H

#include "abstractnode.h"
#include "abstractlistiterator.h"

template <typename T>
class AbstractList
{
public:
    virtual void Delete(int index) = 0;
    virtual void Append(int index, const T& data) = 0;
    virtual void Insert(int index, const T& data) = 0;
    virtual T& Data(int index) = 0;
    virtual int Length() = 0;
    friend class AbstractListIterator<T>;
    virtual ~AbstractList() = 0;

    class request_to_null : public std::exception
    {
        const char* what() const
        {
            return "Trying to do something with element that not contained by the list\n";
        }
    };
};



#endif // ABSTRACTLIST_H

