#include <iostream>
#include <myset.h>

using namespace std;

int main()
{
    MySet<double> dset(41);
    dset += 12;
    dset += 13;

    MySet<double> ddset = dset;
    ddset -= 13;
    ddset += 42;
    ddset += 41;

    ddset = ddset+dset;

    cout << "set is = {41, 12, 13, 14, 15}" << endl;
    cout << "is 13 in? - ";
    if (dset.isIn(13))
        cout << "yes" << endl;
    else
        cout << "no" << endl;
    cout << "is 13 in? - ";
    if (ddset.isIn(13))
        cout << "yes" << endl;
    else
        cout << "no" << endl;

    return 0;
}

