#ifndef ABSTRACTLISTITERATOR_H
#define ABSTRACTLISTITERATOR_H

#include <exception>
#include "abstractlist.h"

template <typename T>
class AbstractListIterator
{
public:
    virtual void toHead() = 0;
    virtual void toTail() = 0;
    virtual void toNext() = 0;
    virtual void toNextEvenNull() = 0;
    virtual void toIndex(int index) = 0;

    virtual bool isTail() = 0;
    virtual bool isHead() = 0;
    virtual bool isListSet() = 0;
    virtual bool isListNull() = 0;
    virtual bool isPtrNull() = 0;

    virtual T& getData() const = 0;

    class null_pointer : public std::exception
    {
        const char* what() const
        {
            return "Trying to get data from null pointer\n";
        }
    };

    virtual void addToHead(const T& element) = 0;
    virtual void addToTail(const T& element) = 0;
    virtual void insertAfter(const T& element) = 0;
    virtual void insertBefore(const T& element) = 0;

    virtual void delFormTail() = 0;
    virtual void delFromHead() = 0;
    virtual void delAll() = 0;
    virtual void delCurrent() = 0;
};

#endif // ABSTRACTLISTITERATOR_H

