#ifndef MYLISTITERATOR_H
#define MYLISTITERATOR_H

#include "mylist.h"
#include "abstractlistiterator.h"

template <typename T>
class MyListIterator : public AbstractListIterator<T>
{
public:
    MyListIterator();
    MyListIterator(const AbstractList<T>* list);
    ~MyListIterator();

    void SetList(const AbstractList<T>* list);

    void toHead();
    void toTail();
    void toNext();
    void toNextEvenNull();
    void toIndex(int index);

    bool isTail();
    bool isHead();
    bool isListSet();
    bool isListNull();
    bool isPtrNull();

    class null_pointer : public std::exception
    {
        const char* what() const noexcept
        {
            return "Trying to get data from null pointer\n";
        }
    };

    T& getData() const;

    void addToHead(const T& element);
    void addToTail(const T& element);
    void insertAfter(const T& element);
    void insertBefore(const T& element);

    void delFormTail();
    void delFromHead();
    void delAll();
    void delCurrent();

protected:
    MyList<T> *_list;
    MyNode<T> *_ptr;
};

template <typename T>
MyListIterator<T>::MyListIterator()
{
    _list = 0;
    _ptr = 0;
}

template <typename T>
MyListIterator<T>::MyListIterator(const AbstractList<T> *list)
{
    _list = list;
    _ptr = _list->_head;
}

template <typename T>
void MyListIterator<T>::SetList(const AbstractList<T> *list)
{
    _list = list;
    _ptr = _list->_head;
}

template <typename T>
void MyListIterator<T>::toHead()
{
    if (_list != 0)
        _ptr = _list->_head;
}

template <typename T>
void MyListIterator<T>::toTail()
{
    if (_list != 0)
    {
        if (_list->_head != 0)
            for (_ptr = _list->_head; _ptr->next() != 0; _ptr = _ptr->next());
        else
            _ptr = 0;
    }
}

template <typename T>
void MyListIterator<T>::toNext()
{
    if ((_ptr != 0) && (_ptr->next() != 0))
        _ptr = _ptr->next();
}

template <typename T>
void MyListIterator<T>::toNextEvenNull()
{
    if (_ptr != 0)
        _ptr = _ptr->next();
}

template <typename T>
void MyListIterator<T>::toIndex(int index)
{
    if ((index >= 0) && (_list != 0))
    {
        _ptr = _list->_head;
        for (int i=0; (i<index)&&(_ptr != 0); i++)
            _ptr = _ptr->next();
    }
}

template <typename T>
bool MyListIterator<T>::isTail()
{
    return ((_ptr != 0) && (_ptr->next() == 0));
}

template <typename T>
bool MyListIterator<T>::isHead()
{
    return ((_ptr != 0) && (_ptr == _list->_head));
}

template <typename T>
bool MyListIterator<T>::isListSet()
{
    return (_list != 0);
}

template <typename T>
bool MyListIterator<T>::isListNull()
{
    return (_list != 0) && (_list->_head == 0);
}

template <typename T>
bool MyListIterator<T>::isPtrNull()
{
    return (_ptr == 0);
}

template <typename T>
T& MyListIterator<T>::getData() const
{
    if (_ptr != 0)
        return _ptr->data();
    else
    {
        MyListIterator<T>::null_pointer error;
        throw error;
    }
}



#endif // MYLISTITERATOR_H

