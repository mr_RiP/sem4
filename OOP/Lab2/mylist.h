#ifndef MYLIST_H
#define MYLIST_H

#include "mynode.h"
#include <exception>

template <typename T>
class MyList
{
protected:
    MyNode<T> *_head;
    MyNode<T> *FindByIndex(int index);
    void DeleteByPtr(MyNode<T> *ptr);
    void AppendByPtr(MyNode<T> *ptr, const T& data);
    void DeleteListFrom(MyNode<T> *ptr);

public:
    MyList();
    MyList(const MyList<T>& list);
    ~MyList();
    void Delete(int index);
    void Append(int index, const T& data);
    void Insert(int index, const T& data);
    T GetData(int index);
    void SetData(int index, const T& data);
    int GetLength();

    class bad_index : public std::exception
    {
        const char* what() const throw()
        {
            return "Trying to reguest to node that don't contained by the list\n";
        }
    };
};

template <typename T>
MyList<T>::MyList()
{
    _head = 0;
}

template <typename T>
MyList<T>::MyList(const MyList<T>& list)
{
    _head = 0;
    if (list._head != 0)
    {
        _head = new MyNode<T>(*list._head);
        MyNode<T> *list_ptr = list._head->getNext();
        if (list_ptr != 0)
        {
            MyNode<T> *this_ptr = _head;
            while (list_ptr != 0)
            {
                this_ptr->setNext(new MyNode<T>(*list_ptr));
                this_ptr = this_ptr->getNext();
                list_ptr = list_ptr->getNext();
            }
        }
    }
}

template <typename T>
MyList<T>::~MyList()
{
    DeleteListFrom(_head);
}

template <typename T>
void MyList<T>::Delete(int index)
{
    DeleteByPtr(FindByIndex(index));
}


template <typename T>
void MyList<T>::Append(int index, const T& data)
{
    if ((_head == 0) && (index == 0))
    {
        _head = new MyNode<T>(data);
        return;
    }

    AppendByPtr(FindByIndex(index), data);
}

template <typename T>
void MyList<T>::Insert(int index, const T& data)
{
    if ((_head == 0) && (index == 0))
    {
        _head = new MyNode<T>(data);
        return;
    }

    MyNode<T> *ptr = FindByIndex(index);
    AppendByPtr(ptr,data);
    T* swapdata = ptr->getDataPtr();
    ptr->setDataPtr(ptr->getNext()->getDataPtr());
    ptr->getNext()->setDataPtr(swapdata);
}

template <typename T>
T MyList<T>::GetData(int index)
{
    return FindByIndex(index)->getData();
}

template <typename T>
void MyList<T>::SetData(int index, const T& data)
{
    FindByIndex(index)->setData(data);
}

template <typename T>
int MyList<T>::GetLength()
{
    int i=0;
    for(MyNode<T>* ptr = _head; ptr != 0; ptr = ptr->getNext())
        ++i;
    return i;
}

template <typename T>
MyNode<T> *MyList<T>::FindByIndex(int index)
{
    MyNode<T> *ptr = _head;
    for (int i=0; (i < index)&&(ptr != 0); ++i)
        ptr = ptr->getNext();

    if (ptr == 0)
    {
        bad_index error;
        throw error;
    }

    return ptr;
}

template <typename T>
void MyList<T>::DeleteByPtr(MyNode<T> *ptr)
{
    if (ptr->getNext() != 0)
    {
        MyNode<T> *delptr = ptr->getNext();
        ptr->setNext(delptr->getNext());
        ptr->setDataPtr(delptr->getDataPtr());
        delete delptr;
    }
    else
    {
        if (ptr == _head)
        {
            _head = 0;
            delete ptr;
        }
        else
        {
            MyNode<T> *preptr = _head;
            for (; preptr->getNext() != ptr; preptr = preptr->getNext());
            preptr->setNext(0);
            delete ptr;
        }
    }
}

template <typename T>
void MyList<T>::DeleteListFrom(MyNode<T> *ptr)
{
    while (ptr != 0)
    {
        MyNode<T> *delptr = ptr;
        ptr = ptr->getNext();
        delete delptr;
    }
}

template <typename T>
void MyList<T>::AppendByPtr(MyNode<T> *ptr, const T& data)
{
    MyNode<T> *newptr = new MyNode<T>(data,ptr->getNext());
    ptr->setNext(newptr);

}



#endif // MYLIST_H

