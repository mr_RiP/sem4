#ifndef MYSET_H
#define MYSET_H

#include "mylist.h"

#define DEFAULT_SET_BASE_LEN 4
#define DEFAULT_SET_REALLOC_STEP 2

template <typename T>
class MySet
{
public:
    MySet();
    ~MySet();
    MySet(const MySet<T>& set);
    MySet(const T& first_element);

    MySet<T>& operator = (const MySet<T>& set);
    MySet<T>& operator += (const T& element);
    MySet<T>& operator += (const MySet<T>& set);
    MySet<T>& operator -= (const T& element);
    MySet<T>& operator -= (const MySet<T>& set);

    template <typename T1>
    friend bool operator == (const MySet<T>& set_a, const MySet<T>& set_b);
    template <typename T1>
    friend bool operator <= (const MySet<T>& set_a, const MySet<T>& set_b);
    template <typename T1>
    friend bool operator >= (const MySet<T>& set_a, const MySet<T>& set_b);
    template <typename T1>
    friend bool operator < (const MySet<T>& set_a, const MySet<T>& set_b);
    template <typename T1>
    friend bool operator > (const MySet<T>& set_a, const MySet<T>& set_b);

    template <typename T1>
    friend MySet<T> operator + (const MySet<T>& set_a, const MySet<T>& set_b);
    template <typename T1>
    friend MySet<T> operator + (const MySet<T>& set, const T& element);
    template <typename T1>
    friend MySet<T> operator + (const T& element, const MySet<T>& set);
    template <typename T1>
    friend MySet<T> operator - (const MySet<T>& set, const T& element);
    template <typename T1>
    friend MySet<T> operator - (const MySet<T>& set_a, const MySet<T>& set_b);

    inline int getNum();
    inline int getLen();

    inline bool isNull();
    bool isIn(const T& element);

    void setNull();
    void fitLen();

private:
    T **array;
    int num;
    int len;

    template <typename T1>
    friend bool isAContainsB(const MySet<T>& set_a, const MySet<T>& set_b);
    void AddMem();
};

template <typename T>
MySet<T>::MySet()
{
    array = 0;
    num = 0;
    len = 0;
}

template <typename T>
MySet<T>::~MySet()
{
    for (int i=0; i<num; i++)
        delete array[i];
    delete [] array;
}

template <typename T>
MySet<T>::MySet(const MySet<T>& set)
{
    len = 0;
    num = 0;

    array = new T*[set.num];
    len = set.num;

    for (; num<set.num; num++)
    {
        array[num] = new T;
        *(array[num]) = *(set.array[num]);
    }
}

template <typename T>
MySet<T>::MySet(const T& first_element)
{
    len = 0;
    num = 0;

    array = new T*[DEFAULT_SET_BASE_LEN];
    len = DEFAULT_SET_BASE_LEN;

    array[num] = new T;
    *(array[num]) = first_element;
    ++num;
}

template <typename T>
void MySet<T>::AddMem()
{
    if (len == 0)
    {
        array = new T*[DEFAULT_SET_BASE_LEN];
        len = DEFAULT_SET_BASE_LEN;
    }
    else
    {
        int new_len = DEFAULT_SET_REALLOC_STEP * len;
        T **new_arr = new T*[new_len];
        for (int i=0; i<num; i++)
            new_arr[i] = array[i];
        delete [] array;
        array = new_arr;
        len = new_len;
    }
}

template <typename T>
void MySet<T>::setNull()
{
    for (int i=0; i<num; i++)
        delete array[i];
    delete [] array;
    len = 0;
    num = 0;
}

template <typename T>
void MySet<T>::fitLen()
{
    if ((len > num) && (num > 0))
    {
        T **new_arr = new T*[num];
        for (int i=0; i<num; i++)
            new_arr[i] = array[i];
        delete [] array;
        array = new_arr;
    }
    else
        setNull();
}

template <typename T>
bool MySet<T>::isNull()
{
    return (len == 0);
}

template <typename T>
bool MySet<T>::isIn(const T& element)
{
    for (int i=0; i<num; i++)
        if (element == *(array[i]))
            return true;

    return false;
}

template <typename T>
int MySet<T>::getNum()
{
    return num;
}

template <typename T>
int MySet<T>::getLen()
{
    return len;
}

template <typename T>
MySet<T>& MySet<T>::operator = (const MySet<T>& set)
{
    if (&set == this)
        return *this;

    if (len > 0)
        setNull();

    array = new T*[set.num];
    len = set.num;

    for (; num<set.num; num++)
    {
        array[num] = new T;
        *(array[num]) = *(set.array[num]);
    }

    return *this;
}

template <typename T>
MySet<T>& MySet<T>::operator += (const T& element)
{
    if(!isIn(element))
    {
        if (num == len)
            AddMem();
        array[num] = new T;
        *(array[num]) = element;
        ++num;
    }
    return *this;
}

template <typename T>
MySet<T>& MySet<T>::operator += (const MySet<T>& set)
{
    for (int i=0; i<set.num; i++)
        this->operator +=(*(set.array[i]));
    return *this;
}

template <typename T>
MySet<T>& MySet<T>::operator -= (const T& element)
{
    for (int i=0; i<num; i++)
    {
        if (*(array[i]) == element)
        {
            delete array[i];
            array[i] = array[--num];
            return *this;
        }
    }
    return *this;
}

template <typename T>
MySet<T>& MySet<T>::operator -= (const MySet<T>& set)
{
    for (int i=0; i<set.num; i++)
        this->operator -= *(set.array[i]);
    return *this;
}

template <typename T>
MySet<T> operator + (const MySet<T>& set_a, const MySet<T>& set_b)
{
    if (&set_a == &set_b)
        return set_a;

    MySet<T> tmp(set_a);
    tmp += set_b;
    return tmp;
}

template <typename T>
MySet<T> operator + (const MySet<T>& set, const T& element)
{
    MySet<T> tmp(set);
    tmp += element;
    return tmp;
}

template <typename T>
MySet<T> operator + (const T& element, const MySet<T>& set)
{
    MySet<T> tmp(set);
    tmp += element;
    return tmp;
}

template <typename T>
MySet<T> operator - (const MySet<T>& set, const T& element)
{
    MySet<T> tmp(set);
    tmp -= element;
    return tmp;
}

template <typename T>
MySet<T> operator - (const MySet<T>& set_a, const MySet<T>& set_b)
{
    if (&set_a == &set_b)
    {
        MySet<T> tmp;
        return tmp;
    }

    MySet<T> tmp(set_a);
    tmp -= set_b;
    return tmp;
}

template <typename T>
bool isAContainsB(const MySet<T>& set_a, const MySet<T>& set_b)
{
    for (int i=0; i<set_b.num; i++)
    {
        bool fail = true;
        for (int j=0; (j<set_a.num)&&(fail); j++)
            if (*(set_b.array[i]) == *(set_a.array[j]))
                fail = false;
        if (fail)
            return false;
    }
    return true;
}

template <typename T>
bool operator == (const MySet<T>& set_a, const MySet<T>& set_b)
{
    if (set_a.num != set_b.num)
        return false;

    return isAContainsB(set_a,set_b);
}

template <typename T>
bool operator <= (const MySet<T>& set_a, const MySet<T>& set_b)
{
    if (set_a.num > set_b.num)
        return false;

    return isAContainsB(set_b,set_a);
}

template <typename T>
bool operator >= (const MySet<T>& set_a, const MySet<T>& set_b)
{
    if (set_a.num < set_b.num)
        return false;

    return isAContainsB(set_a,set_b);
}

template <typename T>
bool operator < (const MySet<T>& set_a, const MySet<T>& set_b)
{
    if (set_a.num >= set_b.num)
        return false;

    return isAContainsB(set_b,set_a);
}

template <typename T>
bool operator > (const MySet<T>& set_a, const MySet<T>& set_b)
{
    if (set_a.num <= set_b.num)
        return false;

    return isAContainsB(set_a,set_b);
}

#endif // MYSET_H
