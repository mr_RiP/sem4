#ifndef DRAW_H
#define DRAW_H

#include <QImage>
typedef QImage canvas_t;

#include "status.h"

// Оберточная функция для рисования линии
status_t DrawLine(canvas_t *canvas, int x1, int y1, int x2, int y2);

// Оберточная функция для рисования точки
status_t DrawPoint(canvas_t *canvas, int x, int y);

// Заливает холст черным цветом
status_t RefillCanvas(canvas_t *canvas);

#endif // DRAW_H

