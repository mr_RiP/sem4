#include "draw.h"
#include <QImage>
#include <QPainter>

status_t DrawLine(canvas_t *canvas, int x1, int y1, int x2, int y2)
{
    if (canvas->isNull())
        return ERROR_NO_CANVAS_SET;
    QPainter p(canvas);
    p.translate(canvas->width()/2, canvas->height()/2);
    p.setPen(Qt::white);
    p.drawLine(x1,y1,x2,y2);
    return NO_ERROR;
}

status_t DrawPoint(canvas_t *canvas, int x, int y)
{
    if (canvas->isNull())
        return ERROR_NO_CANVAS_SET;
    QPainter p(canvas);
    p.translate(canvas->width()/2, canvas->height()/2);
    p.setPen(Qt::red);
    p.drawPoint(x,y);
    return NO_ERROR;
}

status_t RefillCanvas(canvas_t *canvas)
{
    if (canvas->isNull())
        return ERROR_NO_CANVAS_SET;
    canvas->fill(Qt::black);
    QPainter p(canvas);
    int x_len = canvas->width()/2;
    int y_len = canvas->height()/2;
    p.translate(x_len, y_len);
    p.setPen(Qt::gray);
    p.drawLine(-x_len,0,x_len,0);
    p.drawLine(0,-y_len,0,y_len);
    return NO_ERROR;
}

status_t GetCanvasSize(int& width, int& height, canvas_t *canvas)
{
    if (canvas->isNull())
        return ERROR_NO_CANVAS_SET;

    width = canvas->width();
    height = canvas->height();

    return NO_ERROR;
}
