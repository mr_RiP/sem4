#ifndef STATUS_H
#define STATUS_H

typedef enum
{
    NO_ERROR,   // нет ошибки
    ERROR_INCORRECT_COMMAND,  // неопределенная команда
    ERROR_FILE_NOT_EXISTS,  // файла, из которого следует произвести загрузку, нет
    ERROR_FILE_CONTAINS_INCORRECT_DATA, // файл содержит некорректные данные
    ERROR_NOT_ENOUGH_MEMORY, // ошибка - недостаточно памяти
    ERROR_NO_MODEL_LOADED, // ошибка - модель не загружена
    ERROR_INVALID_ARGUMENTS,    // некорректные аргументы команды
    ERROR_CANT_WRITE_INTO_FILE, // ошибка во время записи в файл
    ERROR_CANT_OPEN_OR_CREATE_FILE, // ошибка - нельзя создать или отрыть файл
    ERROR_NO_POINTS_LOADED, // ошибка - точки не были загружены
    ERROR_NO_CANVAS_SET
}
status_t; // тип вывода результата выполнения комманды

#endif // STATUS_H

