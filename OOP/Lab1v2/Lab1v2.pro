#-------------------------------------------------
#
# Project created by QtCreator 2015-04-28T12:40:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab1v2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qscreen.cpp \
    entrypoint.cpp \
    draw.cpp \
    lineoperations.cpp \
    pointoperations.cpp \
    cameraoperations.cpp

HEADERS  += mainwindow.h \
    qscreen.h \
    entrypoint.h \
    status.h \
    draw.h \
    pointoperations.h \
    lineoperations.h \
    cameraoperations.h

FORMS    += mainwindow.ui
