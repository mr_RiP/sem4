#include "cameraoperations.h"
#include <cmath>

camera_t ClearCam(void)
{
    camera_t zero_cam = {{0,0,0},{0.f,0.f,0.f},1.f};
    return zero_cam;
}

status_t MoveCam(camera_t& cam, int x, int y, int z)
{
    cam.pos.x += x;
    cam.pos.y += y;
    cam.pos.z += z;
    return NO_ERROR;
}

status_t ScaleCam(camera_t& cam, float scale)
{
    if (scale <= 0.f)
        return ERROR_INVALID_ARGUMENTS;
    cam.scale = scale;
    return NO_ERROR;
}

status_t RotateCam(camera_t& cam, char axis, float angle)
{
    float *needed_angle;
    switch (axis)
    {
    case 'x':
        needed_angle = &cam.angle.x;
        break;
    case 'y':
        needed_angle = &cam.angle.y;
        break;
    case 'z':
        needed_angle = &cam.angle.z;
        break;
    default:
        return ERROR_INVALID_ARGUMENTS;
    }

    *needed_angle += angle;
    *needed_angle %= 2*M_PI;

    return NO_ERROR;
}
