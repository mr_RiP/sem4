#ifndef QSCREEN_H
#define QSCREEN_H

#include <QWidget>

class QScreen : public QWidget
{
    Q_OBJECT
public:
    explicit QScreen(QWidget *parent = 0);
    ~QScreen();

    void setImage(QImage *image);

    void update()
    {
        this->repaint();
    }

    int getWidth()
    {
        return this->width();
    }

    int getHeight()
    {
        return this->height();
    }

signals:

public slots:

private:
    QImage *img;
    void paintEvent(QPaintEvent *);
};

#endif // QSCREEN_H
