#ifndef LINEOPERATIONS_H
#define LINEOPERATIONS_H

#include "pointoperations.h"
#include "status.h"
#include "draw.h"

typedef
struct
{
    int a;
    int b;
}
line_t;

typedef
struct
{
    line_t *arr;
    int num;
}
line_arr_t;

// Загрузка массива отрезков из файла
status_t LoadLines(line_arr_t& lines, point_arr_t& points, const char *filename);

// Рисование линий
status_t DrawLines(canvas_t *canvas, line_arr_t& lines, point_arr_t& points);

// Сохранение линий в файл
status_t SaveLines(const char *filename, line_arr_t& lines);

// Освобождение и обнуление
status_t ClearLines(line_arr_t& lines);

#endif // LINEOPERATIONS_H

