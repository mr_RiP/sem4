#include "pointoperations.h"
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "draw.h"

status_t ClearPoints(point_arr_t& points)
{
    if (points.num == 0)
        return NO_ERROR;

    free(points.arr);
    points.arr = 0;
    points.num = 0;

    return NO_ERROR;
}

#define LP_BUF_SIZE 16
status_t LoadPoints(point_arr_t& points, const char *filename)
{
    FILE *f = fopen(filename,"r");
    if (f == 0)
        return ERROR_FILE_NOT_EXISTS;

    char buf[LP_BUF_SIZE] = "";
    int num = 0;
    while ((!feof(f)) && (strstr(buf,"points") != buf))
        fgets(buf,LP_BUF_SIZE,f);

    if ((feof(f)) || (sscanf(buf,"points %i\n",&num) != 1) || (num < 1))
    {
        fclose(f);
        return ERROR_FILE_CONTAINS_INCORRECT_DATA;
    }

    point_t *arr = (point_t*) malloc(num * sizeof(point_t));
    if (arr == NULL)
    {
        fclose(f);
        return ERROR_NOT_ENOUGH_MEMORY;
    }

    // А ЕСЛИ ТОЧКИ РАВНЫ?
    for (int i=0; i<num; i++)
        if (fscanf(f,"%f %f %f\n",&arr[i].x, &arr[i].y, &arr[i].z) != 3)
        {
            fclose(f);
            free(arr);
            return ERROR_FILE_CONTAINS_INCORRECT_DATA;
        }

    fclose(f);
    ClearPoints(points);
    points.arr = arr;
    points.num = num;

    return NO_ERROR;
}

status_t DrawPoints(canvas_t *canvas, point_arr_t& points)
{
    if (points.num <= 0)
        return ERROR_NO_POINTS_LOADED;

    if (canvas == 0)
        return ERROR_INVALID_ARGUMENTS;

    for (int i=0; i<points.num; i++)
    {
        status_t output = DrawPoint(canvas, round(points.arr[i].x), round(points.arr[i].y));
        if (output != NO_ERROR)
            return output;
    }

    return NO_ERROR;
}

status_t SavePoints(const char *filename, point_arr_t& points)
{
    if (points.num == 0)
        return ERROR_NO_POINTS_LOADED;

    FILE *fileptr = fopen(filename,"a");
    if (fileptr == 0)
        return ERROR_CANT_OPEN_OR_CREATE_FILE;

    int output = fprintf(fileptr,"points %i\n",points.num);
    for (int i=0; (i<points.num) && (output > 0); i++)
        output = fprintf(fileptr,"%f %f %f\n",points.arr[i].x,
                            points.arr[i].y, points.arr[i].z);
    fclose(fileptr);

    if (output > 0)
        return NO_ERROR;
    else
        return ERROR_CANT_WRITE_INTO_FILE;
}

status_t TurnPointsX(point_arr_t& points, float turn)
{
    if (points.num <= 0)
        return ERROR_NO_POINTS_LOADED;

    float cos_t = cos(turn);
    float sin_t = sin(turn);
    for (int i=0; i<points.num; i++)
    {
        points.arr[i].y = cos_t*points.arr[i].y + sin_t*points.arr[i].z;
        points.arr[i].z = cos_t*points.arr[i].z - sin_t*points.arr[i].y;
    }
    return NO_ERROR;
}

status_t TurnPointsY(point_arr_t& points, float turn)
{
    if (points.num <= 0)
        return ERROR_NO_POINTS_LOADED;

    float cos_t = cos(turn);
    float sin_t = sin(turn);
    for (int i=0; i<points.num; i++)
    {
        points.arr[i].x = cos_t*points.arr[i].x + sin_t*points.arr[i].z;
        points.arr[i].z = cos_t*points.arr[i].z - sin_t*points.arr[i].x;
    }

return NO_ERROR;
}

status_t TurnPointsZ(point_arr_t& points, float turn)
{
    if (points.num <= 0)
        return ERROR_NO_POINTS_LOADED;

    float cos_t = cos(turn);
    float sin_t = sin(turn);
    for (int i=0; i<points.num; i++)
    {
        points.arr[i].x = cos_t*points.arr[i].x + sin_t*points.arr[i].y;
        points.arr[i].y = cos_t*points.arr[i].y - sin_t*points.arr[i].x;
    }

    return NO_ERROR;
}

status_t ScalePoints(point_arr_t& points, float scale)
{
    if (points.num <= 0)
        return ERROR_NO_POINTS_LOADED;

    if (scale <= 0)
        return ERROR_INVALID_ARGUMENTS;

    for (int i=0; i<points.num; i++)
    {
        points.arr[i].x *= scale;
        points.arr[i].y *= scale;
        points.arr[i].z *= scale;
    }

    return NO_ERROR;
}

status_t MovePoints(point_arr_t& points, float x, float y, float z)
{
    if (points.num <= 0)
        return ERROR_NO_POINTS_LOADED;

    for (int i=0; i<points.num; i++)
    {
        points.arr[i].x += x;
        points.arr[i].y += y;
        points.arr[i].z += z;
    }

    return NO_ERROR;
}

status_t CenterPoint(point_t& midpoint , point_arr_t&  points)
{
    if (points.num <= 0)
        return ERROR_NO_POINTS_LOADED;

    point_t max = points.arr[0];
    point_t min = points.arr[0];

    for (int i=1; i<points.num; i++)
    {
        if (max.x > points.arr[i].x)
            max.x = points.arr[i].x;
        if (min.x < points.arr[i].x)
            min.x = points.arr[i].x;
        if (max.y > points.arr[i].y)
            max.y = points.arr[i].y;
        if (min.y < points.arr[i].y)
            min.y = points.arr[i].y;
        if (max.z > points.arr[i].z)
            max.z = points.arr[i].z;
        if (min.z < points.arr[i].z)
            min.z = points.arr[i].z;
    }

    midpoint.x = min.x + (max.x - min.x)/2;
    midpoint.y = min.y + (max.y - min.y)/2;
    midpoint.z = min.z + (max.z - min.z)/2;

    return NO_ERROR;
}
