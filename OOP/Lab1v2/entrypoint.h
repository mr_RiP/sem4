#ifndef ENTRYPOINT_H
#define ENTRYPOINT_H

#include "status.h"

typedef enum
{
    LOAD,
    ROTATE,
    SCALE,
    SAVE,
    DRAW,
    CLEAR,
    MOVE,
    CENTER
}
command_t; // перечень комманд

status_t EntryPoint(command_t command, ...);

#endif // ENTRYPOINT_H

