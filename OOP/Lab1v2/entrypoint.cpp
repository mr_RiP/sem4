#include "entrypoint.h"
#include "modeloperations.h"
#include <cstdarg>

static model_t model = {{0,0},{0,0}};

status_t EntryPoint(command_t command, ...)
{
    va_list vl;
    va_start(vl,command);
    switch (command)
    {
    case LOAD:
    {
        char *filename = va_arg(vl,char*);
        va_end(vl);
        return LoadModel(model,filename);
    }
    case SAVE:
    {
        char *filename = va_arg(vl,char*);
        va_end(vl);
        return SaveModel(model,filename);
    }
    case ROTATE:
    {
        char axis = va_arg(vl,int);
        float turn = va_arg(vl,double);
        va_end(vl);
        return RotateModel(model,axis,turn);
    }
    case SCALE:
    {
        float x = va_arg(vl,double);
        float y = va_arg(vl,double);
        float z = va_arg(vl,double);
        float scale_x = va_arg(vl,double);
        float scale_y = va_arg(vl,double);
        float scale_z = va_arg(vl,double);
        va_end(vl);
        return ScaleModel(model,x,y,z,scale_x,scale_y,scale_z);
    }
    case MOVE:
    {
        float x = va_arg(vl,double);
        float y = va_arg(vl,double);
        float z = va_arg(vl,double);
        va_end(vl);
        return MoveModel(model,x,y,z);
    }
    case DRAW:
    {
        canvas_t *canvas = va_arg(vl,canvas_t*);
        va_end(vl);
        return DrawModel(canvas,model);
    }
    case CLEAR:
    {
        va_end(vl);
        return ClearModel(model);
    }
    case CENTER:
    {
        va_end(vl);
        return CenterModel(model);
    }
    default:
        return ERROR_INCORRECT_COMMAND;
    }
}
