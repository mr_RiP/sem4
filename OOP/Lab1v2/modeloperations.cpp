#include "modeloperations.h"
#include "draw.h"
#include <cstdio>

status_t LoadModel(model_t& model, const char* filename)
{
    model_t workcopy = {{0,0},{0,0}};
    status_t output = NO_ERROR;
    output = LoadPoints(workcopy.points, filename);
    if (output != NO_ERROR)
        return output;
    output = LoadLines(workcopy.lines, workcopy.points, filename);
    if (output != NO_ERROR)
    {
        ClearPoints(workcopy.points);
        return output;
    }
    model = workcopy;
    return NO_ERROR;
}

status_t DrawModel(canvas_t *canvas, model_t& model)
{
    if (canvas == 0)
        return ERROR_INVALID_ARGUMENTS;

    status_t output = RefillCanvas(canvas);
    if (output != NO_ERROR)
        return output;
    output = DrawLines(canvas, model.lines, model.points);
    if (output != NO_ERROR)
        return output;
    return DrawPoints(canvas, model.points);
}

status_t SaveModel(model_t& model, const char* filename)
{
    char temp_filename[L_tmpnam];
    tmpnam(temp_filename);
    status_t output = SavePoints(temp_filename, model.points);
    if (output != NO_ERROR)
    {
        remove(temp_filename);
        return output;
    }
    output = SaveLines(temp_filename, model.lines);
    if (output != NO_ERROR)
    {
        remove(temp_filename);
        return output;
    }

    remove(filename);
    rename(temp_filename,filename);
    return NO_ERROR;

}

status_t RotateModel(model_t& model,
            char axis, float turn)
{
    switch (axis)
    {
    case 'x':
        return TurnPointsX(model.points, turn);
    case 'y':
        return TurnPointsY(model.points, turn);
    case 'z':
        return TurnPointsZ(model.points, turn);
    default:
        return ERROR_INVALID_ARGUMENTS;
    }
}

status_t ScaleModel(model_t& model, float scale)
{
    if ((scale_x <= 0) || (scale_y <= 0) || (scale_z <= 0))
        return ERROR_INVALID_ARGUMENTS;

    return ScalePoints(model.points, scale);
}

status_t MoveModel(model_t& model, float x, float y, float z)
{
    return MovePoints(model.points, x, y, z);
}

status_t ClearModel(model_t& model)
{
    ClearPoints(model.points);
    ClearLines(model.lines);
    return NO_ERROR;
}

status_t CenterModel(model_t& model)
{
    float x = 0;
    float y = 0;
    float z = 0;

    status_t output = CenterPoint(x,y,z,model.points);
    if (output != NO_ERROR)
        return output;

    return MovePoints(model.points,-x,-y,-z);
}
