#include "lineoperations.h"
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include "draw.h"

status_t ClearLines(line_arr_t& lines)
{
    if (lines.num == 0)
        return NO_ERROR;

    free(lines.arr);
    lines.arr = 0;
    lines.num = 0;

    return NO_ERROR;
}

status_t SaveLines(const char *filename, line_arr_t& lines)
{
    FILE *fileptr = fopen(filename,"a");
    if (fileptr == 0)
        return ERROR_CANT_OPEN_OR_CREATE_FILE;

    int output = fprintf(fileptr,"lines %i\n",lines.num);
    for (int i=0; (i<lines.num) && (output > 0); i++)
        output = fprintf(fileptr,"%i %i\n",lines.arr[i].a, lines.arr[i].b);

    fclose(fileptr);

    if (output > 0)
        return NO_ERROR;
    else
        return ERROR_CANT_WRITE_INTO_FILE;
}

#define LL_BUF_SIZE 16
status_t LoadLines(line_arr_t& lines, point_arr_t& points, const char *filename)
{
    if (points.num == 0)
        return ERROR_NO_MODEL_LOADED;

    FILE *f = fopen(filename,"r");
    if (f == 0)
        return ERROR_FILE_NOT_EXISTS;

    char buf[LL_BUF_SIZE] = "";
    int num = 0;
    while ((!feof(f)) && (strstr(buf,"lines") != buf))
        fgets(buf,LL_BUF_SIZE,f);

    if ((feof(f)) || (sscanf(buf,"lines %i\n",&num) != 1) || (num < 0))
    {
        fclose(f);
        return ERROR_FILE_CONTAINS_INCORRECT_DATA;
    }

    if (num == 0)
    {
        ClearLines(lines);
        return NO_ERROR;
    }

    line_t *arr = (line_t*) malloc(num * sizeof(line_t));
    if (arr == NULL)
    {
        fclose(f);
        return ERROR_NOT_ENOUGH_MEMORY;
    }

    for (int i=0; i<num; i++)
        if ((fscanf(f,"%i %i\n",&arr[i].a, &arr[i].b) != 2) ||
                (arr[i].a > points.num) || (arr[i].b > points.num) ||
                (arr[i].a < 0) || (arr[i].b < 0))
        {
            fclose(f);
            free(arr);
            return ERROR_FILE_CONTAINS_INCORRECT_DATA;
        }

    fclose(f);
    ClearLines(lines);
    lines.arr = arr;
    lines.num = num;

    return NO_ERROR;
}

status_t DrawLines(canvas_t *canvas, line_arr_t &lines, point_arr_t &points)
{
    if (canvas == 0)
        return ERROR_INVALID_ARGUMENTS;

    for (int i=0; i<lines.num; i++)
    {
        int x1 = round(points.arr[lines.arr[i].a].x);
        int y1 = round(points.arr[lines.arr[i].a].y);
        int x2 = round(points.arr[lines.arr[i].b].x);
        int y2 = round(points.arr[lines.arr[i].b].y);
        status_t output = DrawLine(canvas, x1, y1, x2, y2);
        if (output != NO_ERROR)
            return output;
    }

    return NO_ERROR;
}
