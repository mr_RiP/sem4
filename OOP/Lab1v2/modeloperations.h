#ifndef MODELOPERATIONS_H
#define MODELOPERATIONS_H

#include "status.h"
#include "pointoperations.h"
#include "lineoperations.h"
#include "draw.h"

// Модель
typedef
struct
{
    point_arr_t points; // массив точек
    line_arr_t lines;   // массив отрезков
}
model_t;

// загрузить модель из файла
status_t LoadModel(model_t& model, const char* filename);

// сохранить модель в файл
status_t SaveModel(model_t& model, const char* filename);

// повернуть модель
status_t RotateModel(model_t& model,  char axis, float turn);

// масштабировать модель
status_t ScaleModel(model_t& model,  float scale);

// очистка памяти
status_t ClearModel(model_t& model);

// Рисование модели
status_t DrawModel(canvas_t *canvas, model_t& model);

// Переместить модель
status_t MoveModel(model_t& model, float x, float y, float z);

// Разместить центр координат в центре модели
status_t CenterModel(model_t& model);

#endif // MODELOPERATIONS_H

