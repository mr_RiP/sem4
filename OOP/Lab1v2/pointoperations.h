#ifndef POINTOPERATIONS_H
#define POINTOPERATIONS_H

#include "status.h"
#include "draw.h"

typedef
struct
{
    float x;
    float y;
    float z;
}
point_t;

typedef
struct
{
    point_t *arr;
    int num;
}
point_arr_t;

// Загрузка точек из файла
status_t LoadPoints(point_arr_t& points, const char *filename);

// Освобождение и зануление массива точек
status_t ClearPoints(point_arr_t& points);

// Рисование точек
status_t DrawPoints(canvas_t *canvas, point_arr_t& points);

// Сохранение списка точек в файл
status_t SavePoints(const char *filename, point_arr_t& points);

// Поворот точечной структуры вокруг оси х
status_t TurnPointsX(point_arr_t& points, float turn);

// Поворот точечной структуры вокруг оси y
status_t TurnPointsY(point_arr_t& points, float turn);

// Поворот точечной структуры вокруг оси z
status_t TurnPointsZ(point_arr_t& points, float turn);

// Масштабирование точечной структуры
status_t ScalePoints(point_arr_t& points, float scale);

// Перемещение точечной структуры
status_t MovePoints(point_arr_t& points, float x, float y, float z);

// Получить значения точки центра модели
status_t CenterPoint(point_t &midpoint, point_arr_t&  points);

#endif // POINTOPERATIONS_H

