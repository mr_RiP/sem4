#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbLoad_clicked();

    void on_pbScale_clicked();

    void on_pbRotUp_clicked();

    void on_pbRotDown_clicked();

    void on_pbRotLeft_clicked();

    void on_pbRotRight_clicked();

private:
    Ui::MainWindow *ui;
    QImage *img;

};

#endif // MAINWINDOW_H
