#include "qscreen.h"
#include "QPainter"

QScreen::QScreen(QWidget *parent) : QWidget(parent)
{
    this->img = 0;
}

QScreen::~QScreen()
{
    if (this->img != 0)
        delete img;
}

void QScreen::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.fillRect(0,0,this->width(),this->height(),Qt::black);
    if (this->img != 0)
        p.drawImage(0,0,*this->img);
}

void QScreen::setImage(QImage *image)
{
    this->img = image;
    this->repaint();
}
