#ifndef CAMERAOPERATIONS_H
#define CAMERAOPERATIONS_H

#include "status.h"


typedef
struct
{
    struct
    {
        int x;
        int y;
        int z;
    } pos;

    struct
    {
        float x;
        float y;
        float z;
    } angle;

    float scale;
} camera_t;

camera_t ClearCam(void);

status_t MoveCam(camera_t& cam, int x, int y, int z);

status_t ScaleCam(camera_t& cam, float scale);

status_t RotateCam(camera_t& cam, char axis, float angle);



#endif // CAMERAOPERATIONS_H

