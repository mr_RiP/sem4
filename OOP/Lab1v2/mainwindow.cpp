#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qscreen.h"
#include <QFileDialog>
#include "entrypoint.h"
#include <QPainter>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    img = new QImage(500,500,QImage::Format_RGB16);
    img->fill(Qt::black);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete img;
}

void MainWindow::on_pbLoad_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
        tr("Открыть модель"), "", tr("Файл модели (*.mymdl)"));

    EntryPoint(LOAD,filename.toStdString().c_str());
    EntryPoint(DRAW,img);
    ui->screen->setImage(img);
}

void MainWindow::on_pbScale_clicked()
{
    float scale = ui->dsbScale->value();
    EntryPoint(SCALE,0.0,0.0,0.0,scale,scale,scale);
    EntryPoint(DRAW,this->img);
    ui->screen->update();
}

void MainWindow::on_pbRotUp_clicked()
{
    float rot = ui->dsbRotStep->value();
    EntryPoint(ROTATE,0.0,0.0,0.0,'x',rot);
    EntryPoint(DRAW,img);
    ui->screen->update();
}

void MainWindow::on_pbRotDown_clicked()
{
    float rot = ui->dsbRotStep->value();
    EntryPoint(ROTATE,0.0,0.0,0.0,'x',-rot);
    EntryPoint(DRAW,img);
    ui->screen->update();
}

void MainWindow::on_pbRotLeft_clicked()
{
    float rot = ui->dsbRotStep->value();
    EntryPoint(ROTATE,0.0,0.0,0.0,'y',rot);
    EntryPoint(DRAW,img);
    ui->screen->update();
}

void MainWindow::on_pbRotRight_clicked()
{
    float rot = ui->dsbRotStep->value();
    EntryPoint(ROTATE,0.0,0.0,0.0,'y',-rot);
    EntryPoint(DRAW,img);
    ui->screen->update();
}
