/* Оберточные функции, использующие библиотеку Qt для вывода данных */

#ifndef MY_INTERFACE_DRAW_H
#define MY_INTERFACE_DRAW_H

#include <cstdio>

// вывод оберточных функций
enum mi_draw_status
{
    NO_ERROR,       // нет ошибки
    NO_CANVAS,      // холст не задан
    TOO_BIG_X,      // x (x1) больше ширины холста
    NEGATIVE_X,     // x (x1) - отрицательное значение
    TOO_BIG_Y,      // y (y1) больше высоты холста
    NEGATIVE_Y,     // y (y1) - отрицательное значение
    TOO_BIG_X2,     // x2 больше ширины холста
    NEGATIVE_X2,    // x2 - отрицательное значение
    TOO_BIG_Y2,     // y2 больше высоты холста
    NEGATIVE_Y2,    // y2 - отрицательное значение
};
typedef enum mi_draw_status mi_draw_status;

// рисование точки
mi_draw_status mi_draw_point(void *canvas, int x, int y);

// рисование линии
mi_draw_status mi_draw_line(void *canvas, int x1, int y1, int x2, int y2);

#endif // MY_INTERFACE_DRAW_H

