#include "my_interface_draw.h"
#include <QPainter>
#include <QImage>

mi_draw_status mi_draw_point(void *canvas, int x, int y)
{
    if (canvas == NULL)
        return NO_CANVAS;

    QImage *img = canvas;

    if (x > img->width())
        return TOO_BIG_X;
    if (x < 0)
        return NEGATIVE_X;
    if (y > img->height())
        return TOO_BIG_Y;
    if (x < 0)
        return NEGATIVE_Y;

    QPainter painter(img);

    painter.setPen(QPen(Qt::black,1,Qt::SolidLine));
    painter.drawPoint(x,y);

    return NO_ERROR;
}

mi_draw_status mi_draw_line(void *canvas, int x1, int y1, int x2, int y2)
{
    if (canvas == NULL)
        return NO_CANVAS;

    QImage *img = canvas;

    if (x1 > img->width())
        return TOO_BIG_X;
    if (x1 < 0)
        return NEGATIVE_X;
    if (y1 > img->height())
        return TOO_BIG_Y;
    if (x1 < 0)
        return NEGATIVE_Y;

    if (x2 > img->width())
        return TOO_BIG_X2;
    if (x2 < 0)
        return NEGATIVE_X2;
    if (y2 > img->height())
        return TOO_BIG_Y2;
    if (x2 < 0)
        return NEGATIVE_Y2;

    QPainter painter(img);

    painter.setPen(QPen(Qt::black,1,Qt::SolidLine));
    painter.drawLine(x1,y1,x2,y2);

    return NO_ERROR;

}

