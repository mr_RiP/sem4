#ifndef FILEWORK_H
#define FILEWORK_H



// ошибки функций-действий
typedef enum fw_status {NO_ERROR,  // нет ошибки
                         BAD_ALLOC, // ошибка выделения памяти
                         NO_FILE,   // файл не существует
                         BAD_FILE   // файл содержит некорректные данные
                        } fw_status;

// загрузить изображение из файла
fw_status fw_open(void &canvas);

// сохранить изображение в файл
fw_status fw_save(void canvas);


#endif // ACTIONS_H

