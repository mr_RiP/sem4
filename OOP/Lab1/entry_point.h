#ifndef ENTRY_POINT_H
#define ENTRY_POINT_H

// возвращаемое значение - успех, неудача, ошибка
typedef enum ep_status {SUCCESS, FAILURE, ERROR} ep_status;

// перечень комманд
typedef enum ep_action {OPEN,  // загрузить изображение из файла
                        SAVE,   // сохранить изображение в файл
                        } ep_action;

// функция - точка входа
ep_status entry_point(void &canvas, ep_action action);

#endif // ENTRY_POINT_H
