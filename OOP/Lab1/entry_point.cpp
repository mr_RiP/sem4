#include "entry_point.h"
#include "filework.h"

ep_status entry_point(void &canvas, ep_action action)
{
    fw_status report;
    switch (action)
    {
    case OPEN:
        report = fw_open(canvas);
        break;
    case SAVE:
        report = fw_save(canvas);
        break;
    default:
        return ERROR;
    }

    if(report == NO_ERROR)
        return SUCCESS;
    else
        return FAILURE;
}
