#ifndef MODEL_TYPE_H
#define MODEL_TYPE_H

// примитив - точка
struct point_t
{
    int x;
    int y;
};
typedef struct point_t point_t;

// тип примитива
enum type_t {POINT, LINE};
typedef enum type_t type_t;

// примитив - линия
struct line_t
{
    point_t a;
    point_t b;
};
typedef struct line_t line_t;

// структура для хранения примитива
struct primitive_t
{
    type_t type;
    union
    {
        point_t point;
        line_t line;
    } primitive;
};
typedef struct primitive_t primitive_t;

// структура - массив примитивов
struct model_t
{
    int num;
    primitive_t **prims;
};
typedef struct model_t model_t;

#endif // MODEL_TYPE_H

