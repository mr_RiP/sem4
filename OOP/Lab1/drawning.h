#ifndef DRAWNING_H
#define DRAWNING_H

#include "model_type.h"

enum draw_status
{
    NO_ERROR,       // нет ошибки
    NO_MODEL,       // модель не задана
    BAD_MODEL       // модель нельзя построить
};

typedef enum draw_status draw_status;

draw_status draw_model(void *canvas, model_t &model);





#endif // DRAWNING_H

