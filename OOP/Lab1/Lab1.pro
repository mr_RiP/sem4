#-------------------------------------------------
#
# Project created by QtCreator 2015-03-31T15:57:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    entry_point.cpp \
    drawning.cpp \
    my_interface/my_interface_draw.cpp \
    actions.cpp

HEADERS  += mainwindow.h \
    entry_point.h \
    model_type.h \
    drawning.h \
    my_interface/my_interface_draw.h \
    actions.h \
    filework.h

FORMS    += mainwindow.ui
