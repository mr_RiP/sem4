#ifndef QPAINTERWIDGET_H
#define QPAINTERWIDGET_H

#include <QWidget>

class QPainterWidget : public QWidget
{
    Q_OBJECT
public:
    explicit QPainterWidget(QWidget *parent = 0);
    ~QPainterWidget();

signals:

public slots:
};

#endif // QPAINTERWIDGET_H
