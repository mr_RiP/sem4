#ifndef SCREEN_H
#define SCREEN_H

#include <QImage>
#include "status.h"

typedef QImage* screen_t;

status_t ScreenDrawPoint(screen_t screen, int x, int y);

status_t ScreenDrawLine(screen_t screen, int x1, int y1, int x2, int y2);

status_t ScreenReset(screen_t screen);


#endif // SCREEN_H

