#include "screen.h"
#include <QPainter>

status_t ScreenDrawPoint(screen_t screen, int x, int y)
{
    if (screen == 0)
        return ERROR_NO_SCREEN;
    if (screen->isNull())
        return ERROR_CANT_DRAW_ON_SCREEN;

    QPainter p(screen);
    p.translate(screen->width()/2.0, screen->height()/2.0);
    p.setPen(Qt::yellow);
    p.drawPoint(x,y);

    return NO_ERROR;
}

status_t ScreenDrawLine(screen_t screen, int x1, int y1, int x2, int y2)
{
    if (screen == 0)
        return ERROR_NO_SCREEN;
    if (screen->isNull())
        return ERROR_CANT_DRAW_ON_SCREEN;

    QPainter p(screen);
    p.translate(screen->width()/2.0, screen->height()/2.0);
    p.setPen(Qt::white);
    p.drawLine(x1,y1,x2,y2);

    return NO_ERROR;
}

status_t ScreenReset(screen_t screen)
{
    if (screen == 0)
        return ERROR_NO_SCREEN;
    if (screen->isNull())
        return ERROR_CANT_DRAW_ON_SCREEN;

    screen->fill(Qt::black);

    return NO_ERROR;
}
