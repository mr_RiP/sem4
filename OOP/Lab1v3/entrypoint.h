#ifndef ENTRYPOINT_H
#define ENTRYPOINT_H

#include "status.h"

typedef enum
{
    LOAD,
    FIT,
    ROTATE,
    SCALE,
    MOVE,
    CLEAR,
}
command_t; // перечень комманд

status_t EntryPoint(command_t command, ...);


#endif // ENTRYPOINT_H

