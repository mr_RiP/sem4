#-------------------------------------------------
#
# Project created by QtCreator 2015-05-16T14:02:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab1v3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    screen.cpp \
    entrypoint.cpp

HEADERS  += mainwindow.h \
    entrypoint.h \
    screen.h \
    status.h

FORMS    += mainwindow.ui
