#ifndef FUNCTABLE3D_H
#define FUNCTABLE3D_H
#include "matrix.h"
#include "functable.h"
#include <exception>

class FuncTable3D
{
public:
    FuncTable3D();
    ~FuncTable3D();

    typedef enum {NO_ERROR,BAD_INPUT,BAD_ALLOC} status;

    FuncTable3D::status SetTable3DFunc(double (*func)(double x, double y), double x0, double xn,
                             int x_count, double y0, double yn, int y_count);

    FuncTable3D::status Interpol3D(double &z, double x, double y, int nx, int ny);

    void Clear();




    class bad_input: public std::exception {};


protected:
    double *x;
    double *y;
    Matrix z;
    int m;
    int n;
    int SetN(double x, double n, double *arr, int num);

private:
    FuncTable3D::status AllocTable3D(int num_x, int num_y);
    void Set00(int &x0, int &y0, double x, double y, int nx, int ny);
};

#endif // FUNCTABLE3D_H
