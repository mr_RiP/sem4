#include "matrix.h"
#include <cstdlib>
#include <new>


Matrix::Matrix()
{
    this->matr = NULL;
    this->rows = 0;
    this->cols = 0;
}

Matrix::Matrix(size_t n, size_t m)
{
    if(this->AllocMatrix(n,m) == BAD_ALLOC)
    {
        std::bad_alloc ba;
        throw ba;
    }
}

Matrix::Matrix(double *matrix, size_t n, size_t m)
{
    this->SetMatrix(matrix,n,m);
}

Matrix::Matrix(Matrix &a)
{
    *this = a;
}

Matrix::status Matrix::AllocMatrix(size_t rows, size_t cols)
{
    double *a = NULL;
    try
    {
        a = new double[rows*cols];
    }
    catch(std::bad_alloc &ba)
    {
        throw ba;
        return BAD_ALLOC;
    }

    if(this->matr != NULL)
    {
        delete [] this->matr;
    }
    this->matr = a;
    this->rows = rows;
    this->cols = cols;
    return NO_ERROR;
}

Matrix::status Matrix::SetMatrix(double *matrix, size_t rows, size_t cols)
{
    if(this->AllocMatrix(rows,cols) == NO_ERROR)
    {
        for(int i=0; i<rows; i++)
            for(int j=0; j<cols; j++)
                this->matr[i*cols+j] = matrix[i*cols+j];
        return NO_ERROR;
    }
    else
    {
        std::bad_alloc ba;
        throw ba;
        return BAD_ALLOC;
    }
}

Matrix & Matrix::operator =(Matrix &a)
{
    if (this == &a)
        return *this;

    if (a.Cols() == 0)
    {
        delete [] this->matr;
        this->matr = NULL;
        this->cols = 0;
        this->rows = 0;
        return *this;
    }

    if (this->AllocMatrix(a.Rows(),a.Cols()) == BAD_ALLOC)
    {
        std::bad_alloc ba;
        throw ba;
        return *this;
    }

    for(int i=0; i<this->rows; i++)
        for(int j=0; j<this->cols; j++)
            this->matr[i*this->cols + j] = a(i,j);
    return *this;

}

Matrix &Matrix::operator +=(Matrix &a)
{
    if(a.Cols() == 0)
        return *this;

    if(this->cols == 0)
    {
        if(this->AllocMatrix(a.Rows(),a.Cols()) == BAD_ALLOC)
        {
            std::bad_alloc ba;
            throw ba;
            return *this;
        }
    }
    else if((a.Cols() != this->cols) || (a.Rows() != this->rows))
    {
        Matrix::bad_dimension bd;
        throw bd;
        return *this;
    }

    for(int i=0; i<this->rows; i++)
        for(int j=0; j<this->cols; j++)
            this->matr[i*this->cols + j] = a(i,j);
    return *this;

}

Matrix Matrix::operator +(Matrix &a)
{
    Matrix b;
    try
    {
        b = *this;
        b += a;
    }
    catch(std::bad_alloc &ba)
    {
        throw ba;
        return *this;
    }
    catch(Matrix::bad_dimension &bd)
    {
        throw bd;
        return *this;
    }
    return b;
}

Matrix & Matrix::operator *=(double a)
{
    for(int i=0; i<this->rows; i++)
        for(int j=0; j<this->cols; j++)
            this->matr[i*this->cols + j] *= a;
    return *this;
}

Matrix Matrix::operator *(double a)
{
    Matrix r;
    try
    {
        r = *this;
    }
    catch(std::bad_alloc &ba)
    {
        throw ba;
        return *this;
    }
    r *= a;
    return r;
}

Matrix Matrix::operator *(Matrix &a)
{
    if((this->cols == 0) || (a.Cols() == 0) || (this->cols != a.Rows()))
    {
        Matrix::bad_dimension bd;
        throw bd;
        return *this;
    }

    try
    {
        Matrix res(this->rows,a.Cols());
        double sum;
        for(int i=0; i<res.Rows(); i++)
            for(int j=0; j<res.Cols(); j++)
            {
                sum = 0.0;
                for(int k=0; k<this->cols; k++)
                    sum += (*this)(i,k) * a(k,j);
                res(i,j) = sum;
            }
        return res;
    }
    catch(std::bad_alloc &ba)
    {
        throw ba;
        return *this;
    }
}

void Matrix::Clear()
{
    delete [] this->matr;
    this->matr = NULL;
    this->cols = 0;
    this->rows = 0;
}

Matrix::~Matrix()
{
    delete [] this->matr;
}

