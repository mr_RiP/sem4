#ifndef MATH_FUNCS_H
#define MATH_FUNCS_H

double math_func_line(double x)
{
    return (2*x - 6);
}

double math_func_sin2(double x)
{
    return (sin(2*x)/2);
}

#endif // MATH_FUNCS_H

