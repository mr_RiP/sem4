#-------------------------------------------------
#
# Project created by QtCreator 2015-03-27T09:50:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab1v2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cubic_spline.cpp \
    functable.cpp \
    functable3d.cpp \
    matrix.cpp

HEADERS  += mainwindow.h \
    cubic_spline.h \
    functable.h \
    functable3d.h \
    matrix.h \
    math_funcs.h

FORMS    += mainwindow.ui
