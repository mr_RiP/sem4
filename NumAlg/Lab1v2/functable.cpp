#include "functable.h"
#include "cubic_spline.h"
#include <cstdlib>
#include <new>
#include <cstdio>

// #define DEBUG

void FuncTable::PrintTable()
{
    if(this->num > 0)
    {
        printf("     X     |     Y     \n");
        for(int i=0; i<this->num; i++)
            printf("%10.4f | %10.4f\n",this->x[i],this->y[i]);
    }
    else
        printf("Table is empty.\n");
}

FuncTable::FuncTable(double (*Func)(double x), double x0, double xn, int num)
{
    this->num = 0;
    FuncTable::status ret = this->SetTableFunc(Func,x0,xn,num);
    if (ret == BAD_ALLOC)
    {
        std::bad_alloc ba;
        throw (ba);
    }
    if (ret == BAD_INPUT)
    {
        FuncTable::bad_input bi;
        throw (bi);
    }
}

FuncTable::FuncTable(double **matrix, int num, FuncTable::storage_method pos)
{
    this->num = 0;
    FuncTable::status ret = this->SetTableMatrix(matrix, num, pos);
    if (ret == BAD_ALLOC)
    {
        std::bad_alloc ba;
        throw (ba);
    }
    if (ret == BAD_INPUT)
    {
        bad_input bi;
        throw (bi);
    }
}

FuncTable::FuncTable(double *array, int num, storage_method pos)
{
    this->num = 0;
    FuncTable::status ret = this->SetTableMatrix(array, num, pos);
    if (ret == BAD_ALLOC)
    {
        std::bad_alloc ba;
        throw (ba);
    }
    if (ret == BAD_INPUT)
    {
        bad_input bi;
        throw (bi);
    }
}

FuncTable::status FuncTable::NewtonInterpol(double &y, double x, int n)
{
    // ошибки ввода
    if ((n > this->num) || (n < 0))
        return BAD_INPUT;

    double **d = this->AllocPyramid(n);
    if(d == NULL)
        return BAD_ALLOC;

    int i0 = this->SetNFromX(x,n);
#ifdef DEBUG
    printf("    i0 = %i\n",i0);
#endif
    this->SetDividedDiffereces(d,i0,n);

    y = this->NewtonPolynom(i0,x,d,n);
    return NO_ERROR;
}

double **FuncTable::AllocPyramid(int n)
{
    double **d = NULL;
    try
    {
        d = new double*[n];
    }
    catch(std::bad_alloc)
    {
        return NULL;
    }

    try
    {
        for(int i=0; i<n; i++)
            d[i] = NULL;
        for(int i=0; i<n; i++)
            d[i] = new double [n-i];
    }
    catch(std::bad_alloc)
    {
        this->FreePyramyd(d,n);
        return NULL;
    }
    return d;
}

void FuncTable::FreePyramyd(double **&d, int n)
{
    if(d != NULL)
    {
        for(int i=0; i<n; i++)
            delete [] d[i];
        delete [] d;
        d = NULL;
    }
}


int FuncTable::SetNFromX(double x, int n)
{
    if ((x < this->x[0]) || (n == this->num))
        return 0;

    if (x > this->x[this->num-1])
        return this->num-n;

    int half = n/2;
    if ((half*2) != n)
        half++;
    int i;
    for (i=0; x>this->x[i]; i++);
    if (i < half)
        return 0;
    if ((n-half+i) > this->num)
        return this->num-n;
    return i-half;
}

void FuncTable::SetDividedDiffereces(double **d, int i0, int n)
{

    for (int i=0; i<n; i++)
    {
        d[0][i] = this->Y(i,i0);
#ifdef DEBUG
        printf("    d[0][%i] = %f\n",i,d[0][i]);
#endif
    }

    for (int i=1; i<n; i++)
    {
        for (int j=0; j<(n-i); j++)
        {
            d[i][j] = this->DividedDifference(this->X(j,i0),d[i-1][j],
                    this->X(j+i,i0),d[i-1][j+1]);
#ifdef DEBUG
            printf("    d[%i][%i] = %f\n",i,j,d[i][j]);
#endif
        }
    }
}

double FuncTable::DividedDifference(double xi, double yi, double xj, double yj)
{
    return (yi - yj)/(xi - xj);
}

double FuncTable::NewtonPolynom(int i0, double x, double **d, int n)
{
    double y = d[0][0];
    for (int i=1; i<n; i++)
    {
        double m = x - this->x[i0];
        for (int j=1; j<i; j++)
            m *= x - this->x[i0+j];
        m *= d[i][0];
        y += m;
    }
    return y;
}

FuncTable::FuncTable()
{
    this->x = NULL;
    this->y = NULL;
    this->num = 0;
}

FuncTable::~FuncTable()
{
    delete [] this->x;
    delete [] this->y;
    this->x = NULL;
    this->y = NULL;
    this->num = 0;
}

FuncTable::status FuncTable::SetTableMatrix(double **matrix, int num, storage_method pos)
{
    if(num <= 0)
        return BAD_INPUT;
    if(matrix == NULL)
        return BAD_INPUT;

    if(this->num != 0)
        this->~FuncTable();
    if(this->AllocTable(num) != 0)
    {
        this->~FuncTable();
        return BAD_ALLOC;
    }

    for(int i=0; i<num; i++)
    {
        if(pos == VERTICAL)
        {
            this->x[i] = matrix[i][0];
            this->y[i] = matrix[i][1];
        }
        if(pos == HORIZONTAL)
        {
            this->x[i] = matrix[0][i];
            this->y[i] = matrix[1][i];
        }
    }
    return NO_ERROR;
}

FuncTable::status FuncTable::SetTableMatrix(double *array, int num, FuncTable::storage_method pos)
{
    if(num <= 0)
        return BAD_INPUT;
    if(array == NULL)
        return BAD_INPUT;

    if(this->num != 0)
        this->~FuncTable();
    if(this->AllocTable(num) != 0)
    {
        this->~FuncTable();
        return BAD_ALLOC;
    }

    for(int i=0; i<num; i++)
    {
        if(pos == VERTICAL)
        {
            this->x[i] = array[i*2];
            this->y[i] = array[i*2+1];
        }
        if(pos == HORIZONTAL)
        {
            this->x[i] = array[i];
            this->y[i] = array[num+i];
        }
    }
    return NO_ERROR;
}

FuncTable::status FuncTable::AllocTable(int num)
{
    try
    {
        this->x = new double[num];
        this->y = new double[num];
    }
    catch(std::bad_alloc)
    {
        return BAD_ALLOC;
    }
    this->num = num;
    return NO_ERROR;
}

FuncTable::status FuncTable::SetTableFunc(double (*Func)(double x), double x0, double xn, int num)
{
    // проверки ввода
    if (num <= 0)
        return BAD_INPUT;
    if(x0 >= xn)
        return BAD_INPUT;

    // выделение памяти под массивы
    if(this->num != 0)
        this->~FuncTable();

    if(AllocTable(num) != 0)
    {
        this->~FuncTable();
        return BAD_ALLOC;
    }

    // вычисление шага
    double step = (xn - x0) / (num - 1);
    // заполнение таблицы
    for(int i=0; i < num; i++)
    {
        this->x[i] = x0;
        this->y[i] = (*Func)(x0);
        x0 += step;
    }

    return NO_ERROR;
}

FuncTable::status FuncTable::SplineInterpol(double &y, double x)
{
    cubic_spline spline;
    try
    {
        spline.build_spline(this->x,this->y,this->num);
    }
    catch(std::bad_alloc &ba)
    {
        throw ba;
        return BAD_ALLOC;
    }

    y = spline.f(x);
    return NO_ERROR;
}

#define EPS 0.00001
// #define KGR_MODE

double FuncTable::TheoreticalError(double (*Func)(double), double x, int n)
{
    if(this->num == 0)
    {
        FuncTable::bad_input bi;
        throw bi;
        return 0;
    }

    int i0 = this->SetNFromX(x,n);

    double max = this->DerivN(Func, this->x[i0], EPS, n+1);
    double mult = 1;
    for (int i=1; i<n; i++)
    {
        mult *= (x - this->x[i+i0]);
        double new_max = this->DerivN(Func, this->x[i0+i], EPS, n+1);
        if(new_max > max)
            max = new_max;
    }
    return (max / this->Fact(n+1)) * fabs(mult);
}

#ifdef KGR_MODE

double FuncTable::Deriv(double (*Func)(double), double x, double eps)
{
    return (8.0 * ((*Func)(x + eps) - (*Func)(x - eps)) +
            (*Func)(x - 2.0 * eps) - (*Func)(x + 2.0 * eps)) /
            (12.0 * eps);
}

double FuncTable::DerivN(double (*Func)(double), double x, double eps, int n)
{
    if (n <= 0)
        return (*Func)(x);
    else if (n == 1)
        return this->Deriv(Func, x, eps);
    else
        return (8.0 * (this->DerivN(Func, x + eps, eps, n - 1) -
                        this->DerivN(Func, x - eps, eps, n - 1)) +
                        this->DerivN(Func, x - 2.0 * eps, eps, n - 1) -
                        this->DerivN(Func, x + 2.0 * eps, eps, n - 1)) /
                        (12.0 * eps);
}

#else

double FuncTable::DerivN(double (*Func)(double), double x, double eps, int n)
{
    if (n <= 0)
        return (*Func)(x);
    else if (n == 1)
        return this->Deriv(Func, x, eps);
    else
    {
        double x0 = x-eps;
        double d = (DerivN(Func,x,eps,n-1) - DerivN(Func,x0,eps,n-1));
        d /= eps;
        return d;
    }
}

double FuncTable::Deriv(double (*Func)(double), double x, double eps)
{
    double x0 = x-eps;
    return ((*Func)(x) - (*Func)(x0)) / eps;
}

#endif

int FuncTable::Fact(int n)
{
    int a = 1;
    for(int i=1; i<=n; i++)
    {
        a *= i;
    }
    return a;
}
