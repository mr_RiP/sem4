#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "functable.h"
#include <QRegExp>
#include <QDoubleValidator>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_cbMethod_currentIndexChanged(int index);

    void on_pbSetTable_clicked();

    void on_cbFunctions_currentIndexChanged(int index);

    void on_pbCalculate_clicked();

private:
    double (*math_func)(double x);
    Ui::MainWindow *ui;
    FuncTable ft;
    QValidator *dval;
};

#endif // MAINWINDOW_H
