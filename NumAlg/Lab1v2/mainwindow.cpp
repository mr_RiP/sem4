#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "functable.h"
#include <QMessageBox>
#include <cmath>
#include "math_funcs.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QRegExp d("-?[0123456789]{1,5}.?[0123456789]{1,5}");
    this->dval = new QRegExpValidator(d);
    ui->leX->setValidator(dval);
    ui->leX0->setValidator(dval);
    ui->leXn->setValidator(dval);
    this->math_func = math_func_line;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete this->dval;
}

void MainWindow::on_cbMethod_currentIndexChanged(int index)
{
    switch(index)
    {
    case 0: ui->sbN->setEnabled(1);
        ui->leTheorError->setEnabled(1);
        break;
    case 1: ui->sbN->setEnabled(0);
        ui->leTheorError->setEnabled(0);
        break;
    }

}

void MainWindow::on_pbSetTable_clicked()
{
    double x0 = ui->leX0->text().toDouble();
    double xn = ui->leXn->text().toDouble();
    if (x0 >= xn)
    {
        QMessageBox::critical(this,"Ошибка ввода","Начальное значение x должно быть меньше конечного."
                                                  "\nПостроение таблицы невозможно.");
        return;
    }
    int rows = ui->sbRows->value();
    try
    {
        this->ft.SetTableFunc(this->math_func,x0,xn,rows);
        ui->tTable->clearContents();
        ui->tTable->setRowCount(rows);
        QTableWidgetItem *item;
        for(int i=0; i<rows; i++)
        {
            item = new QTableWidgetItem;
            item->setText(QString::number(this->ft.X(i,0)));
            ui->tTable->setItem(i,0,item);
            item = new QTableWidgetItem;
            item->setText(QString::number(this->ft.Y(i,0)));
            ui->tTable->setItem(i,1,item);
        }
    }
    catch(std::bad_alloc)
    {
        QMessageBox::critical(this,"Ошибка выделения памяти","Недостаточно оперативной памяти для построения таблицы."
                                                  "\nПостроение таблицы невозможно.");
        this->ft.~FuncTable();
        return;
    }
    ui->sbN->setMaximum(rows);
    ui->pbCalculate->setEnabled(1);
}

void MainWindow::on_cbFunctions_currentIndexChanged(int index)
{
    switch(index)
    {
    case 0: this->math_func = math_func_line;
        break;
    case 1: this->math_func = math_func_sin2;
        break;
    }
    ui->pbCalculate->setDisabled(1);
}

void MainWindow::on_pbCalculate_clicked()
{
    double y;
    double x = ui->leX->text().toDouble();
    if((x > this->ft.X(ui->tTable->rowCount()-1,0)) || (x < this->ft.X(0,0)))
        QMessageBox::warning(this,"Предупреждение", "Точность значений, лежащих за пределами таблицы, не гарантируется.");
    try
    {
        switch(ui->cbMethod->currentIndex())
        {
        case 0:
        {
            int n = ui->sbN->value();
            this->ft.NewtonInterpol(y,x,n);
            double theor_err = this->ft.TheoreticalError(this->math_func,x,n);
            ui->leTheorError->setText(QString::number(theor_err));
        }
        break;
        case 1:
            this->ft.SplineInterpol(y,x);
        break;
        }
    }
    catch(std::bad_alloc)
    {
        QMessageBox::critical(this,"Ошибка выделения памяти","Недостаточно оперативной памяти для выполнения интерполяции.");
        return;
    }
    ui->leY->setText(QString::number(y));
    double true_y = (*this->math_func)(x);
    ui->leError->setText(QString::number(fabs(true_y-y)));
}
