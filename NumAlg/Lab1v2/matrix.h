#ifndef MATRIX_H
#define MATRIX_H
#include <cstdlib>
#include <exception>

class Matrix
{
public:
    Matrix(size_t n, size_t m);
    Matrix(double *matrix, size_t n, size_t m);
    Matrix(Matrix &a);
    Matrix();
    ~Matrix();

    typedef enum {NO_ERROR,BAD_INPUT,BAD_ALLOC} status;

    size_t Rows()
    {
        return this->rows;
    }
    size_t Cols()
    {
        return this->cols;
    }

    Matrix & operator =(Matrix &a);
    Matrix & operator +=(Matrix &a);
    Matrix operator +(Matrix &a);
    Matrix & operator *=(double a);
    Matrix operator *(double a);
    Matrix operator *(Matrix &a);
    double & operator() (size_t i, size_t j)
    {
        return this->matr[i*this->cols + j];
    }
    Matrix::status AllocMatrix(size_t rows, size_t cols);
    Matrix::status SetMatrix(double *matrix, size_t rows, size_t cols);
    void Clear();
    class bad_dimension : public std::exception {};

protected:
    double *matr;
    size_t rows, cols;

};

#endif // MATRIX_H
