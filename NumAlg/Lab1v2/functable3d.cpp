#include "functable3d.h"
#include "functable.h"
#include "matrix.h"
#include <new>
#include <cstdlib>

// #define DEBUG

#ifdef DEBUG
    #include <cstdio>
#endif

#define METHOD_YZ_NEWTON
#define METHOD_XY_NEWTON

FuncTable3D::FuncTable3D()
{
    this->x = NULL;
    this->y = NULL;
    this->n = 0;
    this->m = 0;
}

FuncTable3D::~FuncTable3D()
{
    this->z.Clear();
    delete [] this->x;
    delete [] this->y;
}

void FuncTable3D::Clear()
{
    if(this->n != 0)
    {
        this->z.Clear();
        delete [] this->x;
        delete [] this->y;
        this->x = NULL;
        this->y = NULL;
        this->n = 0;
        this->m = 0;
    }
}

FuncTable3D::status FuncTable3D::SetTable3DFunc(double (*func)(double, double),
                                      double x0, double xn, int x_count,
                                      double y0, double yn, int y_count)
{
    if((x_count < 1) || (y_count < 1) || (x0 > xn) || (y0 > yn))
    {
        FuncTable3D::bad_input bi;
        throw bi;
        return BAD_INPUT;
    }

    if(this->n != 0)
        this->Clear();

    try
    {
        this->x = new double[x_count];
        this->y = new double[y_count];
        this->z.AllocMatrix(x_count,y_count);
        this->n = x_count;
        this->m = y_count;
    }
    catch(std::bad_alloc &ba)
    {
        this->Clear();
        throw ba;
        return (FuncTable3D::status) BAD_ALLOC;
    }

    double x_step = (xn - x0) / (x_count - 1);
    double y_step = (yn - y0) / (y_count - 1);

    for(int i=0; i<x_count; i++, x0+=x_step)
    {
        this->x[i] = x0;
#ifdef DEBUG
        printf("x[%i] = %f\n",i,this->x[i]);
#endif
    }
    for(int i=0; i<y_count; i++, y0+=y_step)
    {
        this->y[i] = y0;
#ifdef DEBUG
        printf("y[%i] = %f\n",i,this->y[i]);
#endif
    }


    for(int i=0; i<x_count; i++)
    {
        for(int j=0; j<y_count; j++)
        {
            this->z(i,j) = (*func)(this->x[i],this->y[j]);
#ifdef DEBUG
            printf("[%i,%i]=%6.4f\n",i,j,this->z(i,j));
#endif
        }
    }
    return (FuncTable3D::status) NO_ERROR;
}

FuncTable3D::status FuncTable3D::Interpol3D(double &z, double x, double y, int nx, int ny)
{
    if((nx > this->n) || (ny > this->m) || (nx < 1) || (ny < 1))
    {
        FuncTable3D::bad_input ba;
        throw ba;
        return BAD_INPUT;
    }

#ifdef DEBUG
    printf("Interpol3D: input ok\n");
#endif
    int x0 = 0, y0 = 0;
    double *yi = NULL;
    double **table = NULL;
    this->Set00(x0, y0, x, y, nx, ny);
    try
    {
        yi = new double [nx];
        table = new double* [2];
        FuncTable fx;
        table[0] = &this->y[y0];

#ifdef DEBUG
        printf("Array table[0]:\n");
        for(int i=0; i<ny; i++)
            printf("    [%i] = %f\n",i,table[0][i]);
#endif

        for(int i=0; i<nx; i++)
        {
            table[1] = &this->z(x0+i,y0);

#ifdef DEBUG
            printf("Array table[1], %i iteration:\n",i+1);
            for(int j=0; j<ny; j++)
                printf("    [%i] = %f\n",j,table[1][j]);
#endif

            fx.SetTableMatrix(table,ny,(FuncTable::storage_method) 1);
#ifdef DEBUG
            fx.PrintTable();
#endif
#ifdef METHOD_YZ_NEWTON
            fx.NewtonInterpol(yi[i],y,ny);
#endif
        }

#ifdef DEBUG
        printf("Array yi:\n");
        for(int i=0; i<nx; i++)
        {
            printf("    [%i] = %f\n",i,yi[i]);
        }
#endif
        table[0] = &this->x[x0];
        table[1] = yi;
        fx.SetTableMatrix(table,nx,(FuncTable::storage_method) 1);
#ifdef METHOD_XY_NEWTON
        fx.NewtonInterpol(z,x,nx);
#endif
        delete [] yi;
        delete [] table;
    }
    catch(std::bad_alloc &ba)
    {
        delete [] yi;
        delete [] table;
        throw ba;
        return BAD_ALLOC;
    }

    return NO_ERROR;
}

void FuncTable3D::Set00(int &x0, int &y0, double x, double y, int nx, int ny)
{
    x0 = this->SetN(x,nx,this->x,this->n);
    y0 = this->SetN(y,ny,this->y,this->m);

#ifdef DEBUG
    printf("x0 = %i\ny0 = %i\n",x0,y0);
#endif

}

int FuncTable3D::SetN(double x, double n, double *arr, int num)
{
    if ((x < arr[0]) || (n == num))
        return 0;

    if (x > arr[num-1])
        return num-n;

    int half = n/2;
    if ((half*2) != n)
        half++;
    int i;
    for (i=0; x>arr[i]; i++);
    if (i < half)
        return 0;
    if ((n-half+i) > num)
        return num-n;
    return i-half;
}
