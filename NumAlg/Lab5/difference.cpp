#include <difference.h>
#include <cmath>

double A_Xi(double x)
{
    return x;
}

double A_Eta(double x)
{
    if (x <= 0.0)
        return 0.0;

    return log(x);
}

void diff1(Matrix& t, int c)
{
    int n = t.getRows() - 1;
    int i = 0;
    int j = 1;
    for (; i < n; i++, j++)
        t(i,c) = (t(j,1) - t(i,1)) / (t(j,0) - t(i,0));
}

void diff2(Matrix& t, int c)
{
    int n = t.getRows() - 1;
    int k = 0;
    int j = 2;
    for (int i = 1; i < n; i++, j++, k++)
        t(i,c) = (t(j,1) - t(k,1)) / (t(j,0) - t(k,0));
}

double Runge_Xi(Matrix& t, int i, double r, double h)
{
    if (i < 2)
        return (t(i+(int)r, 1) - t(i,1)) / (r * h);

    if (i > t.getRows() - 3)
        return (t(i,1) - t(i-(int)r,1)) / (r * h);

    return (t(i+(int)r,1) - t(i-(int)r,1)) / (2.0 * r * h);
}

void diff3(Matrix& t, int c, double r)
{
    double h = t(1,0) - t(0,0);
    double c0;
    double c1;

    for (int i = 0; i < t.getRows(); i++)
    {
        c0 = Runge_Xi(t, i, 1, h);
        c1 = Runge_Xi(t, i, r, h);

        if ((i >= 2) || (i <= t.getRows() - 3))
            t(i,c) = c0 + (c0 - c1) / (r - 1.0);
        else
            t(i,c) = c0 + (c0 - c1) / (r * r - 1.0);
    }
}

void diff4(Matrix& t, int c)
{
    t(0,c) = (-3.0 * t(0,1) + 4.0 * t(1,1) - t(2,1)) / (t(2,0) - t(0,0));
    double tmp1 = 3.0 * t(t.getRows()-1,1) - 4.0 * t(t.getRows()-2,1) + t(t.getRows()-3,1);
    double tmp2 = t(t.getRows()-1,0) - t(t.getRows()-3,0);
    t(t.getRows() - 1,c) = tmp1 / tmp2;
}

void diff5(Matrix& t, int c)
{
    int n = t.getRows();

    Matrix at(n, 3);

    for (int i=0; i<n; i++)
    {
        at(i,0) = A_Xi(t(i,0));
        at(i,1) = A_Eta(t(i,1));
    }

    diff2(at, 2);

    --n;
    for (int i=1; i<n; i++)
        t(i,c) = at(i,2) * t(i,1);
}
