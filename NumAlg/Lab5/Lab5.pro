#-------------------------------------------------
#
# Project created by QtCreator 2015-06-01T23:04:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab5
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    difference.cpp \
    entrypoint.cpp \
    matrix.cpp

HEADERS  += mainwindow.h \
    difference.h \
    entrypoint.h \
    matrix.h

FORMS    += mainwindow.ui
