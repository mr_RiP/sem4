#ifndef DIFFERENCE_H
#define DIFFERENCE_H

#include <matrix.h>

void diff1(Matrix& t, int c);
void diff2(Matrix& t, int c);
void diff3(Matrix& t, int c, double r);
void diff4(Matrix& t, int c);
void diff5(Matrix& t, int c);

#endif // DIFFERENCE_H

