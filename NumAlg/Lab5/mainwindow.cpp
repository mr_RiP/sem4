#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "entrypoint.h"
#include <QMessageBox>
#include <cmath>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->leX0->setValidator(&val);
    ui->leXn->setValidator(&val);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbCalculate_clicked()
{
    double a = locale().toDouble(ui->leX0->text());
    double b = locale().toDouble(ui->leXn->text());

    if (b <= a)
    {
        QMessageBox::critical(this,"Ошибка","Начальное значение Х должно быть меньше конечного");
        return;
    }

    int n = ui->sbN->value();
    Matrix matr;

    Calculate(matr,a,b,n);

    ui->tTable->clearContents();
    ui->tTable->setRowCount(n);
    int m = ui->tTable->columnCount();
    for (int i=0; i<n; i++)
        for (int j=0; j<m; j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem;
            item->setText(locale().toString(matr(i,j),'f',6));
            ui->tTable->setItem(i,j,item);
        }
}
