#include <entrypoint.h>
#include <cmath>
#include <difference.h>


#define MATRIX_COLS 7

void FillMatrix(Matrix& matr, int n)
{
    matr.alloc(n,MATRIX_COLS);
    for (int i=0; i<n; i++)
        for (int j=0; j<MATRIX_COLS; j++)
            matr(i,j) = NAN;

}

double func(double x)
{
    return exp(x);
}

void SetXY(Matrix& matr, double a, double b, int n)
{
    double x = a;
    double step = (b - a) / (n - 1);

    for (int i=0; i<n; i++, x += step)
    {
        matr(i,0) = x;
        matr(i,1) = func(x);
    }
}


void Calculate(Matrix& matr, double a, double b, int n)
{
    FillMatrix(matr,n);

    SetXY(matr,a,b,n);

    diff1(matr, 2);
    diff2(matr, 3);
    diff3(matr, 4, 2);
    diff4(matr, 5);
    diff5(matr, 6);
}

