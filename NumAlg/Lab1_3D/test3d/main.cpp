#include <iostream>
#include "../matrix.h"
#include "../functable3d.h"

double func(double x, double y)
{
    return 2*x*y;
}

using namespace std;

int main()
{
    FuncTable3D ft;
    double arr[100];
    try
    {
        ft.SetTable3DFunc(func,1,5,5,1,5,5);
        cout << "Print x, y\n";
        double x, y;
        cin >> x >> y;
        cout << "x = " << x << "\ny = " << y << endl;
        cout << "Print Nx, Ny\n";
        int nx, ny;
        cin >> nx >> ny;
       //for(int i=0; i<100; i++)
       // {
            double z;
            ft.Interpol3D(z,x,y,nx,ny);
            cout << "z = " << z << "\nReal z = " << func(x,y) << endl;
      //      arr[i] = z;
        //}
       // for(int i=0; i<100; i++)
       // {
         //   printf("arr[%i] = %f\n",i,arr[i]);
       // }
    }
    catch(std::bad_alloc)
    {
        cout << "Bad alloc\n";
    }
    return 0;
}

