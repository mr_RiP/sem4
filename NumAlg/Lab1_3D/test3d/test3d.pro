TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ../cubic_spline.cpp \
    ../functable.cpp \
    ../functable3d.cpp \
    ../matrix.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    ../cubic_spline.h \
    ../functable.h \
    ../functable3d.h \
    ../math_func.h \
    ../matrix.h

