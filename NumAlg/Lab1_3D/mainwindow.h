#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "functable3d.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbSetTable_clicked();

    void on_cbFunction_currentIndexChanged(int index);

    void on_pbInterpolate_clicked();

private:
    Ui::MainWindow *ui;
    FuncTable3D ft;
    double (*Func)(double x, double y);
    void SetTable(void);
};

#endif // MAINWINDOW_H
