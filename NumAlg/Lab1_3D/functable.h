#ifndef FUNCTABLE_H
#define FUNCTABLE_H

#include <exception>

class FuncTable
{
public:
    typedef enum {VERTICAL,HORIZONTAL} storage_method;
    // конструктор и деструктор
    FuncTable();
    FuncTable(double (*Func)(double x), double x0, double xn, int num);
    FuncTable(double **matrix, int num, storage_method pos);
    FuncTable(double *array, int num, storage_method pos);
    ~FuncTable();
    typedef enum {NO_ERROR,BAD_INPUT,BAD_ALLOC} status;

    // Создание таблицы
    status SetTableFunc(double (*Func)(double x), double x0, double xn, int num);
    status SetTableMatrix(double **matrix, int num, storage_method pos);
    status SetTableMatrix(double *array, int num, storage_method pos);

    // Интерполяция
    status NewtonInterpol(double &y, double x, int n);
    status SplineInterpol(double &y, double x);

    double TheoreticalError(double (*Func)(double x), double x, int n);

    // получение х при счете массива от 1 до num
    double X(int i, int mov=-1)
    {
        return this->x[i+mov];
    }
    // получение y при счете массива от 1 до num
    double Y(int i, int mov=-1)
    {
        return this->y[i+mov];
    }

    void PrintTable(void);

    class bad_input : public std::exception {};

protected:
    double *x;
    double *y;
    int num;

private:
    int SetNFromX(double x, int n);
    status AllocTable(int num);
    double DividedDifference(double xi, double yi, double xj, double yj);
    double **AllocPyramid(int n);
    void SetDividedDiffereces(double **d, int i0, int n);
    double NewtonPolynom(int i0, double x, double **d, int n);
    void FreePyramyd(double **&d, int n);
    double DerivN(double (*Func)(double x), double x, double eps, int n);
    double Deriv(double (*Func)(double x), double x, double eps);
    int Fact(int n);
};

#endif // FUNCTABLE_H
