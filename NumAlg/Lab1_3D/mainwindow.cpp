#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "functable3d.h"
#include <QMessageBox>
#include "math_func.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->Func = mf_2xy;
    ui->pbInterpolate->setDisabled(1);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbSetTable_clicked()
{
    double x0 = ui->dsbX0->value();
    double xn = ui->dsbXN->value();
    int n = ui->sbN->value();
    if(x0 >= xn)
    {
        QMessageBox::critical(this,"Ошибка ввода","Начальное значение x должно быть меньше конечного."
                                                  "\nПостроение таблицы невозможно.");
        return;
    }

    double y0 = ui->dsbY0->value();
    double yn = ui->dsbYM->value();
    int m = ui->sbM->value();
    if(y0 >= yn)
    {
        QMessageBox::critical(this,"Ошибка ввода","Начальное значение y должно быть меньше конечного."
                                                  "\nПостроение таблицы невозможно.");
        return;
    }

    try
    {
        this->ft.SetTable3DFunc(this->Func,x0,xn,n,y0,yn,m);
        ui->sbNumX->setMaximum(n);
        ui->sbNumY->setMaximum(m);
        ui->twTable->setRowCount(n+1);
        ui->twTable->setColumnCount(m+1);
        this->SetTable();
        ui->pbInterpolate->setEnabled(1);
    }
    catch(std::bad_alloc)
    {
        QMessageBox::critical(this,"Ошибка выделения памяти","Недостаточно оперативной памяти"
                                                             " для построения таблицы."
                                                             "\nПостроение таблицы невозможно.");
        this->ft.Clear();
        ui->twTable->clearContents();
        return;
    }
}

#define DECIMALS 4

void MainWindow::SetTable()
{
    QTableWidgetItem *item;
    QFont font;
    font.setBold(1);
    ui->twTable->clearContents();
    for(int i=0; i<this->ft.N(); i++)
    {
        item = new QTableWidgetItem();
        item->setText(QString::number(this->ft.X(i),'g',DECIMALS));
        item->setFont(font);
        ui->twTable->setItem(i+1,0,item);
        for(int j=0; j<this->ft.M(); j++)
        {
            if(i==0)
            {
                item = new QTableWidgetItem();
                item->setText(QString::number(this->ft.Y(j),'g',DECIMALS));
                item->setFont(font);
                ui->twTable->setItem(0,j+1,item);
            }
            item = new QTableWidgetItem();
            item->setText(QString::number(this->ft.Z(i,j),'g',DECIMALS));
            ui->twTable->setItem(i+1,j+1,item);
        }
    }
}

void MainWindow::on_cbFunction_currentIndexChanged(int index)
{
    switch(index)
    {
    case 0:
        this->Func = mf_2xy;
        break;
    case 1:
        this->Func = mf_x2py2;
        break;
    case 2:
        this->Func = mf_cosxpy;
        break;
    }
    ui->pbInterpolate->setDisabled(1);
}

void MainWindow::on_pbInterpolate_clicked()
{
    double x = ui->dsbInterpolX->value()+1;
    double y = ui->dsbInterpolY->value()+1;
    if  ((x < this->ft.X(0)) || (x > this->ft.X(this->ft.N()-1)) ||
         (y < this->ft.Y(0)) || (y > this->ft.Y(this->ft.M()-1)))
    {
         QMessageBox::warning(this,"Предупреждение", "Точность значений, "
                              "лежащих за пределами таблицы, не гарантируется.");
    }
    double z=0;
    int nx = ui->sbNumX->value();
    int ny = ui->sbNumY->value();
    if(this->ft.N()==0)
    {
        QMessageBox::critical(this,"Внутренняя ошибка","Таблица значений функции повреждена");
        return;
    }
    FuncTable3D::status err_code = this->ft.Interpol3D(z,x,y,nx,ny);
    if(err_code != 0)
    {
        QMessageBox::critical(this,"Ошибка выделения памяти","Недостаточно оперативной памяти"
                                                             " для построения таблицы."
                                                             "\nПостроение таблицы невозможно.");
        return;
    }
    double err = fabs((*this->Func)(x,y) - z);
    ui->dsbZ->setValue(z);
    ui->dsbError->setValue(err);
}
