#ifndef MATH_FUNC_H
#define MATH_FUNC_H
#include <math.h>

double mf_2xy(double x, double y)
{
    return 2*x*y;
}

double mf_x2py2(double x, double y)
{
    return x*x + y*y;
}

double mf_cosxpy(double x, double y)
{
    return cos(x) + y;
}

#endif // MATH_FUNC_H

