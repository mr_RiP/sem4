#include <cmath>

static inline double sqr (double x)
{
    return x * x;
}

double Func (double tau, double omega, double phi)
{
    static const double eps = 1e-5;

    double sinO = sin (omega);
    double cosO = cos (omega);

    double denom = 1 - sqr (sinO * cos (phi));

    if (denom < eps)
        return cosO * sinO;

    double expVal = exp (-2 * tau * cosO / denom);
    return (1 - expVal) * cosO * sinO;
}
