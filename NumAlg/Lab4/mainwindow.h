#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDoubleValidator>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbCalculate_clicked();

private:
    Ui::MainWindow *ui;
    typedef double (*ComputeFunc) (double , int, int);
    QDoubleValidator validator;
};

#endif // MAINWINDOW_H
