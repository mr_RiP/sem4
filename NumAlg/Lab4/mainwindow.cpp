#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "l4integral.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    validator.setDecimals(6);
    ui->leInput->setValidator(&validator);
    ui->leResultTrap->setValidator(&validator);
    ui->leResultSpots->setValidator(&validator);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbCalculate_clicked()
{
    double val = locale().toDouble(ui->leInput->text());;
    int out = ui->sbOut->value();
    int in = ui->sbIn->value();


    if ((val < 0) || (in < 2) || (out < 2))
        QMessageBox::critical(this,"Ошибка","Неправильные входные данные!");

    double res_trap;
    double res_spots;
    try
    {
        res_spots = GetIntSpots(val,out,in);
        res_trap = GetIntTrapezium(val,out,in);
    }
    catch (...)
    {
        QMessageBox::critical(this,"Ошибка","Найти результат не удалось!");
        return;
    }

    ui->leResultTrap->setText(locale().toString(res_trap,'f',6));
    ui->leResultSpots->setText(locale().toString(res_spots,'f',6));
}
