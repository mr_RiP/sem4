#-------------------------------------------------
#
# Project created by QtCreator 2015-05-18T17:16:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab4
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    func.cpp \
    l4integral.cpp

HEADERS  += mainwindow.h \
    func.h \
    l4integral.h

FORMS    += mainwindow.ui
