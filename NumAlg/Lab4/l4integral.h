#ifndef INTEGRAL_H_INCLUDED
#define INTEGRAL_H_INCLUDED

double GetIntTrapezium(double tau, int out, int in);
double GetIntSpots(double tau, int out, int in);

#endif // INTEGRAL_H_INCLUDED
