#include "func.h"
#define _USE_MATH_DEFINES
#include <cmath>

static double GetTrapezium (double* arr, double step, int n)
{
    double res = 0;
    for (int i = 1; i < n; i++)
        res += (arr[i] + arr[i - 1]);
    return res * step / 2;
}

double GetIntTrapezium(double tau, int out, int in)
{
    double outStep = M_PI / 2.0 / (double)(out - 1);
    double inStep = M_PI / 2.0 / (double)(in - 1);

    double* inValues = new double[in];
    double* outValues = new double[out];

    double phi = 0;

    for (int i = 0; i < out; i++, phi += outStep)
    {
        double omega = 0;
        for (int j = 0; j < in; j++, omega += inStep)
            inValues[j] = Func (tau, omega, phi);
        outValues[i] = GetTrapezium (inValues, inStep, in);
    }

    delete[] inValues;
    double res = GetTrapezium (outValues, outStep, out);
    delete[] outValues;

    return 4 * res / M_PI;
}

double GetIntSpots(double tau, int out, int in)
{
    double outStep = M_PI / 2.0 / (double)(out - 1);
    double inStep = M_PI / 2.0 / (double)(in - 1);

    double sum = 0;
    double beta = outStep;
    double alpha = 0;

    for (int i=0; i < (out-1); i++, beta += outStep, alpha += outStep)
    {
        double y = (beta+alpha)/2.0;
        double a = 0;
        double b = inStep;
        for (int j=0; j < (in-1); j++, a += inStep, b += inStep)
        {
            double x = (a+b)/2.0;
            double s = (b-a)*(beta-alpha);
            sum += Func(tau, x, y) * s;
        }
    }
    return 4.0 * sum / M_PI;
}
