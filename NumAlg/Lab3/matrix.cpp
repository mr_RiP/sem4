#include "matrix.h"
#include <cstdlib>
#include <new>
#include <cstdio>

Matrix::Matrix()
{
    this->matr = NULL;
    this->rows = 0;
    this->cols = 0;
}

Matrix::Matrix(int n, int m)
{
    this->matr = NULL;
    this->rows = 0;
    this->cols = 0;
    this->alloc(n,m);
}

Matrix::Matrix(double *matrix, int n, int m)
{
    this->matr = NULL;
    this->rows = 0;
    this->cols = 0;
    this->setMatrix(matrix,n,m);
}

Matrix::Matrix(const Matrix &a)
{
    this->matr = NULL;
    this->rows = 0;
    this->cols = 0;
    this->alloc(a.rows,a.cols);
    for (int i=0; i < this->rows; i++)
        for (int j=0; j < this->cols; j++)
            this->matr[i*this->cols + j] = a.matr[i*a.cols + j];
}

void Matrix::alloc(int rows, int cols)
{
    if ((rows == 0) && (cols == 0))
    {
        this->clear();
        return;
    }

    if ((rows <= 0) || (cols <= 0))
    {
        Matrix::bad_input bi;
        throw bi;
    }

    double *tmp = new double[rows*cols];
    if (this->matr != NULL)
        this->clear();
    this->matr = tmp;
    this->rows = rows;
    this->cols = cols;
}

void Matrix::setMatrix(double *matrix, int rows, int cols)
{
    this->alloc(rows,cols);
    for (int i=0; i<rows; i++)
        for (int j=0; j<cols; j++)
            this->matr[i*cols + j] = matrix[i*cols +j];
}

Matrix& Matrix::operator = (const Matrix& a)
{
    if (this == &a)
        return *this;

    this->alloc(a.rows,a.cols);
    for (int i=0; i<this->rows; i++)
        for (int j=0; j<this->cols; j++)
            this->matr[i*this->cols + j] = a.matr[i*a.cols + j];
    return *this;
}

Matrix& Matrix::operator += (const Matrix &a)
{
    if ((this->cols != a.cols) || this->rows != a.rows)
    {
        Matrix::bad_dimension bd;
        throw bd;
    }

    for (int i=0; i<this->rows; i++)
        for (int j=0; j<this->cols; j++)
            this->matr[i*this->cols + j] += a.matr[i*a.cols + j];
    return *this;
}

Matrix& Matrix::operator *= (const double &a)
{
    for (int i=0; i < this->rows; i++)
        for (int j=0; j < this->cols; j++)
            this->matr[i*this->cols + j] *= a;
    return *this;
}

Matrix operator + (const Matrix& a, const Matrix& b)
{    
    Matrix copy = a;
    return copy += b;
}

Matrix operator * (const double& a, const Matrix& b)
{
    Matrix copy = b;
    copy *= a;
    return copy;
}

Matrix operator * (const Matrix& a, const double& b)
{
    Matrix copy = a;
    copy *= b;
    return copy;
}

Matrix operator * (const Matrix &a, const Matrix &b)
{
    if ((a.cols == 0) || (b.cols == 0) || (a.cols != b.rows))
    {
        Matrix::bad_dimension bd;
        throw bd;
    }
    Matrix res(a.rows,b.cols);
    double sum;
    for (int i=0; i<res.rows; i++)
        for (int j=0; j<res.cols; j++)
        {
            sum = 0.0;
            for (int k=0; k<a.cols; k++)
                sum += a.matr[i*a.cols + k] * b.matr[k*b.cols +j];
            res(i,j) = sum;
        }
    return res;
}

void Matrix::clear()
{
    delete [] this->matr;
    this->matr = NULL;
    this->cols = 0;
    this->rows = 0;
}

Matrix::~Matrix()
{
    delete [] this->matr;
}

void Matrix::printConsole(const char *format)
{
    if ((this->rows == 0) || (this->cols == 0))
        printf("There is no matrix.\n");

    for (int i=0; i<this->rows; i++)
    {
        for (int j=0; j<this->cols; j++)
        {
            printf(format,this->matr[i*this->cols+j]);
        }
        printf("\n");
    }
}

bool operator == (const Matrix& a, const Matrix& b)
{
    if ((a.cols != b.cols) || (a.rows != b.cols))
        return 0;

    int n = a.rows;
    int m = a.cols;
    for (int i=0; i<n; i++)
        for (int j=0; j<m; j++)
            if (a.matr[i*n+j] != b.matr[i*n+j])
                return 0;
    return 1;
}

bool operator != (const Matrix& a, const Matrix& b)
{
    return !(a == b);
}

Matrix Matrix::getLinearSystem()
{
    if(this->cols != (this->rows+1))
    {
        Matrix::bad_dimension bd;
        throw bd;
    }

    int n = this->rows;
    Matrix x(n,1);
    Matrix a = *this;
    a.setTriangleView();

    x(n-1) = a(n-1,n);
    for (int i=n-2; i>=0; i--)
    {
        x(i) = a(i,n);
        for (int j=i+1; j<n; j++)
            x(i) -= a(i,j) * x(j);
    }

    return x;
}

Matrix Concat(const Matrix& a, const Matrix& b)
{
    if (a.rows != b.rows)
    {
        Matrix::bad_dimension bd;
        throw bd;
    }

    Matrix c(a.rows, a.cols + b.cols);
    for (int i=0; i<c.rows; i++)
    {
        for (int j=0; j<a.cols; j++)
            c(i,j) = a.matr[i*a.cols + j];
        for (int j=0; j<b.cols; j++)
            c(i,j+a.cols) = b.matr[i*b.cols + j];
    }

    return c;
}

void Matrix::setTriangleView()
{
    for (int i=0; i<this->rows; i++)
    {
        double tmp = this->matr[i*this->cols + i];
        for (int j=this->rows; j>=i; j--)
            this->matr[i*this->cols + j] /= tmp;
        for (int j=i+1; j<this->rows; j++)
        {
            tmp = this->matr[j*this->cols + i];
            for (int k=this->rows; k>=i; k--)
                this->matr[j*this->cols + k] -= tmp * this->matr[i*this->cols + k];
        }
    }
}
