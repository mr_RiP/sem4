#ifndef INTEGRAL_H
#define INTEGRAL_H

#include <exception>
#include "matrix.h"

class Integral
{
public:
    Integral();
    ~Integral();
    Integral(double (*function)(double), int n, double eps);
    Integral(double alpha);

    // Задаем интеграл
    void setIntegral(double (*function)(double), int n, double eps);
    // Получаем интеграл
    double getIntegral(double a, double b);
    // Интеграл на промежутке -1..1
    double getIntegralStd(void);
    // Проверка задана ли функция
    bool isSet()
    {
        return is_set;
    }
    void clear();

    // Exception для некорректного ввода
    class bad_input : public std::exception {};
    // Exception для попытки вывести результат при незаданной функции
    class func_not_set: public std::exception {};

    // Нахождение интеграла
    static double Integrate(double (*function)(double),
                            double a, double b, int n, double eps);

    // Возвращает значение полинома Лежандра n степени от x
    static double LegendrePolynomial(double x, int n);

    // Находит корни полинома Лежандра n степени
    static Matrix LegendrePolynomialRoots(int n, double eps);

    // Стандартная функция ошибок
    static double ErrFunc(double x);

protected:
    bool is_set;
    double (*func)(const double x);
    Matrix legendre_roots;
    Matrix a_factors;
};

#endif // INTEGRAL_H
