#ifndef MATRIX_H
#define MATRIX_H

#include <exception>

// Класс для представления матрицы
class Matrix
{
public:
    // Конструкторы
    Matrix(int n, int m);
    Matrix(double *matrix, int n, int m);
    Matrix(const Matrix &a);
    Matrix();
    ~Matrix();

    // Информация о размерности матрицы
    int getRows()
    {
        return this->rows;
    }
    int getCols()
    {
        return this->cols;
    }
    int getCount()
    {
        return this->cols * this->rows;
    }

    // Проверка на пустоту
    bool isEmpty(void)
    {
        return (this->cols == 0)? 1: 0;
    }

    // Унарные операторы операторы
    Matrix& operator += (const Matrix &a);
    Matrix& operator *= (const double &a);
    Matrix& operator *= (Matrix &a);

    // Оператор присваивания
    Matrix& operator = (const Matrix &a);

    // Бинарные операторы
    friend Matrix operator + (const Matrix&, const Matrix&);
    friend Matrix operator * (const Matrix&, const Matrix&);
    friend Matrix operator * (const double&, const Matrix&);
    friend Matrix operator * (const Matrix&, const double&);
    friend bool operator == (const Matrix&, const Matrix&);
    friend bool operator != (const Matrix&, const Matrix&);

    // Конкатенация
    friend Matrix Concat(const Matrix& a, const Matrix& b);

    // Оператор для доступа к элементу матрицы
    double & operator () (int i, int j=0)
    {
        return this->matr[i*this->cols + j];
    }

    // Приведение к треугольному виду
    void setTriangleView();

    // Решение СЛАУ методом Гаусса
    // матрица должна иметь вид Ab, где b - столбец свободных членов
    Matrix getLinearSystem();

    // (Пере)выделение памяти под матрицу (данные не сохраняются)
    void alloc(int rows, int cols);
    // Задание матрицы из массива
    void setMatrix(double *matrix, int rows, int cols);
    // Печать матрицы в консоль
    void printConsole(const char *format);
    // Очистка памяти
    void clear();

    // Некорректное соотношение размерностей матрицы (используется в умножении матриц)
    class bad_dimension : public std::exception {};
    // Некорректный ввод
    class bad_input : public std::exception {};


protected:
    double *matr;       // Матрица
    int rows, cols;     // Размерности

};

#endif // MATRIX_H
