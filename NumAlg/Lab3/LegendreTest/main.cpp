#include <iostream>
#include "../matrix.h"
#include "../integral.h"
#include <cmath>
#include <cstdio>

double legendre3(double x)
{
    return (5*pow(x,3) - 3*x)/2;
}

double legendre5(double x)
{
    return (63*pow(x,5) - 70*pow(x,3) + 15*x)/8;
}

double legendre10(double x)
{
    return (46189*pow(x,10) -
            109395*pow(x,8) +
            90090*pow(x,6) -
            30030*pow(x,4) +
            3465*pow(x,2) - 63) / 256;
}


int main()
{
    double eps = 0.00001;
    Matrix roots;
    int n;
    double leg, real_leg;

    n = 3;
    printf("n = %i\n",n);
    roots = Integral::LegendrePolynomialRoots(n,eps);
    for (int i=0; i<n; i++)
    {
        printf("Root[%i] = %f\n",i,roots(i));
        leg = Integral::LegendrePolynomial(roots(i),n);
        real_leg = legendre3(roots(i));
        printf("    Calc = %15.10f\n",leg);
        printf("    Real = %15.10f\n",real_leg);
    }
    printf("\n");

    n = 5;
    printf("n = %i\n",n);
    roots = Integral::LegendrePolynomialRoots(n,eps);
    for (int i=0; i<n; i++)
    {
        printf("Root[%i] = %f\n",i,roots(i));
        leg = Integral::LegendrePolynomial(roots(i),n);
        real_leg = legendre5(roots(i));
        printf("    Calc = %15.10f\n",leg);
        printf("    Real = %15.10f\n",real_leg);
    }
    printf("\n");

    n = 10;
    printf("n = %i\n",n);
    roots = Integral::LegendrePolynomialRoots(n,eps);
    for (int i=0; i<n; i++)
    {
        printf("Root[%i] = %f\n",i,roots(i));
        leg = Integral::LegendrePolynomial(roots(i),n);
        real_leg = legendre10(roots(i));
        printf("    Calc = %15.10f\n",leg);
        printf("    Real = %15.10f\n",real_leg);
    }
    printf("\n");

    return 0;
}

