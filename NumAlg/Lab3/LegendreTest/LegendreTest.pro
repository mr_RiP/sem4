TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ../integral.cpp \
    ../matrix.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    ../integral.h \
    ../matrix.h

