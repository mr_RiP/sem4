#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "integral.h"
#include <cmath>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_pbCalculate_clicked()
{
    Integral s(Integral::ErrFunc,this->ui->sbN->value(),this->ui->dsbEps1->value());

    double a=-1.0, b=1.0, step=2.0;
    double alpha = ui->dsbAlpha->value();
    double ya = s.getIntegral(0,a) - alpha;
    double yb = s.getIntegral(0,b) - alpha;
    while ((ya*yb) >= 0)
    {
        a *= step;
        ya = s.getIntegral(0,a) - alpha;
        b *= step;
        yb = s.getIntegral(0,b) - alpha;
    }

    double t = (b-a)/2 + a;
    double yt = s.getIntegral(0,t) - alpha;
    double y_past = 0;
    double eps = ui->dsbEps2->value();
    while (fabs((y_past - yt)/yt) > eps)
    {
        if ((ya*yt) < 0)
        {
            b = t;
            yb = yt;
        }
        else
        {
            a = t;
            ya = yt;
        }
        y_past = yt;
        t = (b-a)/2 + a;
        yt = s.getIntegral(0,t) - alpha;
    }

    ui->dsbX->setValue(t);

}


