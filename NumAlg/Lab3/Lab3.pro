#-------------------------------------------------
#
# Project created by QtCreator 2015-04-22T15:42:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    matrix.cpp \
    integral.cpp

HEADERS  += mainwindow.h \
    matrix.h \
    integral.h

FORMS    += mainwindow.ui
