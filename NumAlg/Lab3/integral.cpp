#include "integral.h"
#include <cmath>
#include "matrix.h"

Integral::Integral()
{
    this->is_set = 0;
}

Integral::~Integral()
{

}

Integral::Integral(double (*function)(double), int n, double eps)
{
    this->setIntegral(function,n,eps);
}


void Integral::setIntegral(double (*function)(double), int n, double eps)
{
    if ((function == 0) || (n <= 0))
    {
        Integral::bad_input bi;
        throw bi;
    }

    this->func = function;
    this->legendre_roots = Integral::LegendrePolynomialRoots(n,eps);

    Matrix linear_sys(n, n+1);
    for (int k=0; k<n; k++)
    {
        for (int i=0; i<n; i++)
            linear_sys(k,i) = pow(this->legendre_roots(i),k);
        linear_sys(k,n) = ((k%2) == 0)? (2.0/(k + 1.0)): 0.0;
    }

    this->a_factors = linear_sys.getLinearSystem();

    this->is_set = 1;
}

double Integral::Integrate(double (*function)(double), double a, double b, int n, double eps)
{
    Integral s(function,n,eps);
    return s.getIntegral(a,b);
}

double Integral::ErrFunc(double x)
{
    return exp(x*x / -2.0) / sqrt(2.0 * M_PI);
}

double Integral::LegendrePolynomial(double x, int n)
{
    if (n < 0)
    {
        Integral::bad_input bi;
        throw bi;
    }

    switch (n)
    {
    case 0: return 1;
    case 1: return x;
    default :
        double past_lp = 1;
        double present_lp = x;
        double new_lp;
        for (int i=2; i<=n; i++)
        {
           new_lp = ((2*i-1)*x*present_lp - ((i-1)*past_lp)) / i;
           past_lp = present_lp;
           present_lp = new_lp;
        }
        return present_lp;
        // Рекусивное решение
        // return ((2*n-1)*x*Integral::LegendrePolynomial(x,n-1) -
           //     ((n-1)*Integral::LegendrePolynomial(x,n-2))) / n;

    }
}

Matrix Integral::LegendrePolynomialRoots(int n, double eps)
{
    if ((n < 0) || (eps <= 0))
    {
        Integral::bad_input bi;
        throw bi;
    }

    Matrix roots;
    if (n == 0)
        return roots;

    roots.alloc(n,1);
    int m = 5*n;
    double step = 2.0 / m;
    double a = -1;
    double b = a + step;
    for (int i=0, j=0; i<m; i++)
    {        
        double ta = a;
        double tb = b;
        double ay = Integral::LegendrePolynomial(ta,n);
        double by = Integral::LegendrePolynomial(tb,n);
        double past_y = 0;
        bool root_is_finded = 0;
        while ((!root_is_finded) && ((ay*by) < 0))
        {
            double t = ((tb-ta) / 2) + ta;
            double ty = Integral::LegendrePolynomial(t,n);
            if (fabs((past_y - ty) / ty) > eps)
            {
                if ((ay*ty) < 0)
                {
                    tb = t;
                    by = ty;
                }
                else
                {
                    ta = t;
                    ay = ty;
                }
                past_y = ty;
            }
            else
            {
                root_is_finded = true;
                roots(j++) = t;
            }
        }
        a += step;
        b += step;
    }
    return roots;
}

double Integral::getIntegral(double a, double b)
{
    if (this->is_set == false)
    {
        Integral::func_not_set fns;
        throw fns;
    }
    double ba_sum = (b + a) / 2.0;
    double ba_diff = (b - a) / 2.0;
    double result = 0;
    for (int i=0; i<this->legendre_roots.getCount(); i++)
    {
        double xi = ba_diff * this->legendre_roots(i) + ba_sum;
        result += this->a_factors(i) * (*this->func)(xi);
    }
    result *= ba_diff;
    return result;
}

double Integral::getIntegralStd(void)
{
    if (this->is_set == false)
    {
        Integral::func_not_set fns;
        throw fns;
    }
    double result = 0;
    for (int i=0; i<this->legendre_roots.getCount(); i++)
        result += this->a_factors(i) * (*this->func)(this->legendre_roots(i));
    return result;
}

void Integral::clear()
{
    this->legendre_roots.clear();
    this->a_factors.clear();
    this->func = 0;
    this->is_set = 0;
}
