#include <iostream>
#include "../integral.h"
#include "../matrix.h"
#include <cmath>

double x2p3xp4(double x)
{
    return (x*x + 3*x + 4);
}

double xm2p40(double x)
{
    return (2*x + 40);
}

double sin3x(double x)
{
    return (sin(3*x));
}


using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    cout << "Please input N and Eps\n" << endl;
    int n;
    double eps;
    cin >> n >> eps;

    cout << "Function: x^2 + 3*x + 4" << endl;
    Integral a(x2p3xp4,n,eps);
    double ans = a.getIntegral(0,1);
    cout << "(0..1) = " << ans << endl;
    ans = a.getIntegral(1,5);
    cout << "(1..5) = " << ans << endl;
    ans = a.getIntegral(-1,1);
    cout << "(-1..1) = " << ans << endl;
    ans = a.getIntegralStd();
    cout << "   Std  = " << ans << endl;

    cout << "Function: 2*x + 40" << endl;
    a.setIntegral(xm2p40,n,eps);
    ans = a.getIntegral(0,1);
    cout << "(0..1) = " << ans << endl;
    ans = a.getIntegral(1,5);
    cout << "(1..5) = " << ans << endl;
    ans = a.getIntegral(-1,1);
    cout << "(-1..1) = " << ans << endl;
    ans = a.getIntegralStd();
    cout << "   Std  = " << ans << endl;
    ans = a.getIntegral(-1,0.25);
    cout << "(-1..0.25) = " << ans << endl;

    cout << "Function: sin(3*x)" << endl;
    a.setIntegral(sin3x,n,eps);
    ans = a.getIntegral(0,1);
    cout << "(0..1) = " << ans << endl;
    ans = a.getIntegral(1,5);
    cout << "(1..5) = " << ans << endl;
    ans = a.getIntegral(-1,1);
    cout << "(-1..1) = " << ans << endl;
    ans = a.getIntegralStd();
    cout << "   Std  = " << ans << endl;

    cout << "Function: err" << endl;
    a.setIntegral(Integral::ErrFunc,n,eps);
    ans = a.getIntegral(0,1);
    cout << "(0..1) = " << ans << endl;
    ans = a.getIntegral(0,5);
    cout << "(0..5) = " << ans << endl;
    ans = a.getIntegral(-1,1);
    cout << "(-1..1) = " << ans << endl;
    ans = a.getIntegralStd();
    cout << "   Std  = " << ans << endl;

    return 0;
}

