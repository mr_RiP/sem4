#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"
#include "matrix.h"
#include "ols.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbSetTable_clicked();

    void on_cbFunc_currentIndexChanged(int index);

    void on_pbGo_clicked();

private:
    Ui::MainWindow *ui;
    double (*func)(double x);
    Matrix createMatrix();
    void drawGraphic(Matrix& table, OLS& ols);
};

#endif // MAINWINDOW_H
