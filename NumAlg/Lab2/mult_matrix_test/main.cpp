#include <iostream>
#include "../matrix.h"
using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    Matrix a(3,3), b(3,1), x;
    for (int i=0; i<3; i++)
    {
        for (int j=0; j<3; j++)
            cin >> a(i,j);
        cin >> b(i);
    }
    cout << "Before" << endl << "A:" << endl;
    a.printConsole("%4.2f ");
    cout << "B:" << endl;
    b.printConsole("%4.2f ");


    a = Concat(a,b);
    b = a;
    b.setTriangleView();

    cout << "Concat:" << endl;
    a.printConsole("%4.2f ");
    cout << "Triangle:" << endl;
    b.printConsole("%4.2f ");

    x = a.getLinearSystem();

    cout << "After" << endl << "Matrix:" << endl;
    a.printConsole("%4.2f ");
    cout << "X:" << endl;
    x.printConsole("%4.2f ");

    return 0;
}

