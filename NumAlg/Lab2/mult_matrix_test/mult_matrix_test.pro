TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++98

SOURCES += main.cpp \
    ../matrix.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    ../matrix.h

