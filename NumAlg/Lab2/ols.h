#ifndef OLS_H
#define OLS_H

#include "matrix.h"
#include <exception>

// Метод наименьших квадратов
class OLS
{
public:
    OLS();
    ~OLS();
    OLS(Matrix &table, int n);

    // Задаем массив а из таблицы значений (вертикальной)
    void setFactors(Matrix &table, int n);

    // Получаем значение y
    double getFunc(double x);

    class bad_input : public std::bad_exception {};
    class not_set : public std::bad_exception {};
protected:
    Matrix a;
};

#endif // OLS_H
