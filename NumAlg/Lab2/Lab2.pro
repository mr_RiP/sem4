#-------------------------------------------------
#
# Project created by QtCreator 2015-04-11T14:51:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Lab2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    matrix.cpp \
    qcustomplot.cpp \
    ols.cpp

HEADERS  += mainwindow.h \
    matrix.h \
    qcustomplot.h \
    func.h \
    ols.h

FORMS    += mainwindow.ui
