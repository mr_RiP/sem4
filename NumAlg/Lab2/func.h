#ifndef FUNC_H
#define FUNC_H

double func0(double x)
{
    return x + sin(x);
}

double func1(double x)
{
    return x*x;
}

double func2(double x)
{
    return 3*x*x*x + 2*x*x + 5;
}

#endif // FUNC_H

