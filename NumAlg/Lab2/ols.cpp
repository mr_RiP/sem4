#include "ols.h"
#include "matrix.h"
#include <cmath>

OLS::OLS()
{
}

OLS::~OLS()
{
}

OLS::OLS(Matrix &table, int n)
{
    this->setFactors(table,n);
}

void OLS::setFactors(Matrix &table, int n)
{
    if ((n < 0) || (table.getCols() != 3) || (table.getRows() < 2))
    {
        OLS::bad_input bi;
        throw bi;
    }

    ++n;
    Matrix lin_eq(n, n+1);

    for (int k=0; k<n; k++)
    {
        // Цикл счета a[k][m]
        for (int m=0; m<n; m++)
        {
            lin_eq(k,m) = 0;
            for (int i=0; i<table.getRows(); i++)
                lin_eq(k,m) += table(i,2) * pow(table(i,0),k) * pow(table(i,0),m);
        }

        // Цикл счета b[k]
        lin_eq(k,n) = 0;
        for (int i=0; i<table.getRows(); i++)
            lin_eq(k,n) += table(i,2) * table(i,1) * pow(table(i,0),k);
    }

    // Решение СЛАУ методом Гаусса
    this->a = lin_eq.getLinearSystem();
}

double OLS::getFunc(double x)
{
    if (this->a.isEmpty())
    {
        OLS::not_set ns;
        throw ns;
    }

    double y = 0;
    for (int k=0; k<this->a.getRows(); k++)
        y += this->a(k) * pow(x,k);
    return y;
}
