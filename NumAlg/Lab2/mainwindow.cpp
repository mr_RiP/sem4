#include "mainwindow.h"
#include "ols.h"
#include "matrix.h"
#include "ui_mainwindow.h"
#include "func.h"
#include "qcustomplot.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->on_cbFunc_currentIndexChanged(this->ui->cbFunc->currentIndex());
}

MainWindow::~MainWindow()
{
    this->ui->twTable->clear();
    this->ui->cpGraphic->clearGraphs();
    delete ui;
}

void MainWindow::on_pbSetTable_clicked()
{
    int n = this->ui->sbNum->value();
    double x_start = this->ui->dsbXStart->value();
    double x_fin = this->ui->dsbXFin->value();
    if (x_start >= x_fin)
    {
        QMessageBox::critical(this,"Ошибка","Начальное значение x должно быть меньше конечного.");
        return;
    }
    double step = (x_fin - x_start)/(n-1);
    this->ui->twTable->clearContents();
    this->ui->twTable->setRowCount(n);
    for (int i=0; i<n; x_start += step, i++)
    {
        this->ui->twTable->setItem(i,0,new QTableWidgetItem(QString::number(x_start)));
        this->ui->twTable->setItem(i,1,new QTableWidgetItem(QString::number((*this->func)(x_start))));
        this->ui->twTable->setItem(i,2,new QTableWidgetItem(QString::number(1)));
    }
}

void MainWindow::on_cbFunc_currentIndexChanged(int index)
{
    switch (index)
    {
    case 0: this->func = func0;
        break;
    case 1: this->func = func1;
        break;
    case 2: this->func = func2;
        break;
    }
}

Matrix MainWindow::createMatrix()
{
    Matrix table(this->ui->twTable->rowCount(),this->ui->twTable->columnCount());
    for(int i=0; i<table.getRows(); i++)
        for(int j=0; j<table.getCols(); j++)
            table(i,j) = this->ui->twTable->item(i,j)->text().toDouble();
    return table;
}

#define MULT 10

void MainWindow::drawGraphic(Matrix& table, OLS &ols)
{
    this->ui->cpGraphic->clearGraphs();

    QVector<double> x(table.getRows()), y(table.getRows());
    double ymin = table(0,1);
    double ymax = ymin;
    for (int i=0; i<table.getRows(); i++)
    {
        x[i] = table(i,0);
        y[i] = table(i,1);
        if (y[i] > ymax)
            ymax = y[i];
        if (y[i] < ymin)
            ymin = y[i];
    }
    this->ui->cpGraphic->xAxis->setRange(table(0,0),table(table.getRows()-1,0));
    this->ui->cpGraphic->yAxis->setRange(ymin,ymax);
    this->ui->cpGraphic->addGraph();
    this->ui->cpGraphic->graph(0)->setData(x,y);
    this->ui->cpGraphic->graph(0)->setPen(QPen(Qt::red));

    QVector<double> xi(table.getRows()*MULT), yi(table.getRows()*MULT);

    double x_start = this->ui->dsbXStart->value();
    double x_fin = this->ui->dsbXFin->value();
    int n = this->ui->sbNum->value();
    double step = (x_fin - x_start)/((n*MULT)-1);

    for (int i=0; i<(table.getRows()*MULT); i++)
    {
        xi[i] = x_start;
        x_start += step;
        yi[i] = ols.getFunc(xi[i]);
    }
    this->ui->cpGraphic->addGraph();
    this->ui->cpGraphic->graph(1)->setData(xi,yi);
    this->ui->cpGraphic->graph(1)->setPen(QPen(Qt::blue));
    this->ui->cpGraphic->replot();
}

void MainWindow::on_pbGo_clicked()
{
    if (this->ui->twTable->rowCount() != 0)
    {
        //QMessageBox::information(this,"Начало","Процедура запущена");
        Matrix table = this->createMatrix();
        //QMessageBox::information(this,"Шаг 1","Таблица построена");
        //table.printConsole("%-6.2f ");
        OLS ols(table,this->ui->sbN->value());
        //QMessageBox::information(this,"Шаг 2","Коэффициенты найдены");
        this->drawGraphic(table,ols);
    }
    else
        QMessageBox::critical(this,"Ошибка","Таблица не задана.\n"
                                            "Прежде чем аппроксимировать функцию,"
                                            " необходимо получить таблицу её значений.");
}
