#-------------------------------------------------
#
# Project created by QtCreator 2015-05-22T21:39:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab6G
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    System.cpp \
    Data.cpp

HEADERS  += mainwindow.h \
    System.h \
    Data.h

FORMS    += mainwindow.ui
