#ifndef DATA_H
#define DATA_H

#include <stdlib.h>
#include <stdio.h>

#define e 2.7182818284590452353602874713527

double Qi(double T,int I);
double Ei(int I);
double nt(double T,double P);
int Zi(int I);
#endif // DATA_H
