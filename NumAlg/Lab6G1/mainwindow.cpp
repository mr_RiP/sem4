#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "System.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    double T = ui->T->value();
    double P = ui->P->value();
    double ne = 0;
    double n[4];
    double Eps = pow(10,-4);
    double Delta = 0;
    ResolveSystem(n,&ne,&Delta,T,P,Eps);
    ui->n1->setText(locale().toString(n[0],'g',6));
    ui->n2->setText(locale().toString(n[1],'g',6));
    ui->n3->setText(locale().toString(n[2],'g',6));
    ui->n4->setText(locale().toString(n[3],'g',6));
    ui->ne->setText(locale().toString(ne,'g',6));
    ui->Delta->setText(locale().toString(Delta,'g',6));
    return;
}
