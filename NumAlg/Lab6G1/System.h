#ifndef SYSTEM_H
#define SYSTEM_H

#include <math.h>

void ResolveSystem(double *n, double *ne, double *Delta, double T, double P, double Eps);

#endif // SYSTEM_H
