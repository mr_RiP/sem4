#include "System.h"
#include "Data.h"

void SystemBCD(double G,double *DE,double *K,double *alpha,double T)
{
    double Part;
    double Qt,Qt1;
    for (int I = 0; I<4;I++)
    {
        Part = ((1+pow(Zi(I+1),2)*G/2)*(1+G/2))/(1+pow(Zi(I),2)*G/2);
        DE[I] = 8.61*pow(10,-5)*T*log(Part);
        Qt = Qi(T,I);
        Qt1 = Qi(T,I+1);
        Part = -(Ei(I)-DE[I])*(11603/T);
        K[I] = 4.83*pow(10,-3)*(Qt1/Qt)*pow(T,1.5)*exp(Part);
    }
    (*alpha) = 0.285*(double)pow(10,-11)*pow((G*T),3);
    return;
}

double FindG(double *n,double ne,double T,double G)
{
    double Res = 0;
    double Sum = 0;
    for (int I = 1;I<4;I++)
        Sum+=(n[I]*pow(Zi(I),2))/(1+Zi(I)*(G/2));
    Res = pow(G,2)-((5.87*pow(10,10))/pow(T,3))*((ne/(1+G/2))+Sum);
    return Res;
}

void InstallMatrix(double A[5][6],double *K,double *Xprev,double Vprev,double alpha,double P,double T)
{
    double c =((double)(P*7242)/T) ;
    A[0][0] = -1;   //  X1
    A[0][1] = 1;    //  X2
    A[0][2] = 0;    //  X3
    A[0][3] = 0;    //  X4
    A[0][4] = 1;    //  V
    A[0][5] = log(K[0])-Vprev - Xprev[1]+Xprev[0];  //  B
    A[1][0] = 0;
    A[1][1] = -1;
    A[1][2] = 1;
    A[1][3] = 0;
    A[1][4] = 1;
    A[1][5] = log(K[1])-Vprev - Xprev[2]+Xprev[1];
    A[2][0] = 0;
    A[2][1] = 0;
    A[2][2] = -1;
    A[2][3] = 1;
    A[2][4] = 1;
    A[2][5] = log(K[2])-Vprev - Xprev[3]+Xprev[2];
    A[3][0] = 0;
    A[3][1] = -Zi(1)*exp(Xprev[1]);
    A[3][2] = -Zi(2)*exp(Xprev[2]);
    A[3][3] = -Zi(3)*exp(Xprev[3]);
    A[3][4] = exp(Vprev);
    A[3][5] = -(exp(Vprev) - Zi(1)*exp(Xprev[1]) - Zi(2)*exp(Xprev[2])-Zi(3)*exp(Xprev[3]));
    A[4][0] = -exp(Xprev[0]);
    A[4][1] = -exp(Xprev[1]);
    A[4][2] = -exp(Xprev[2]);
    A[4][3] = -exp(Xprev[3]);
    A[4][4] = -exp(Vprev);
    A[4][5] = -(((double)(P*7242)/T) + alpha - exp(Vprev) - exp(Xprev[0])-exp(Xprev[1]
            -exp(Xprev[2])-exp(Xprev[3])));
    return;
}

void Gauss(double A[5][6],double *Res,int n)
{
    double Work;
    for (int I=0;I<n;I++)
    {
        if (A[I][I] != 0)
            Work = A[I][I];
            for (int J = I; J<(n+1); J++)
                A[I][J] /= Work;
            for (int J = I + 1; J<n; J++)
            {
                Work = A[J][I];
                for(int T = I + 1; T < n+1; T++)
                    A[J][T] -= Work * A[I][T];
            }
    }
    Res[n-1] = A[n-1][n];
    for (int I = n-2; I >= 0; I--)
    {
        Work = 0;
        for (int J=I+1;J<n;J++)
        {
            Work +=A[I][J]*Res[J];
        }
        Res[I]=(A[I][n]-Work)/A[I][I];
    }
    return;
}


double ResultG(double Eps,double T,double ne,double *n)
{
    double Left = 0;
    double Rigth = 4;
    double Center = (Left+Rigth)/2;
    double FG = 0;
    double LastFG = 10;
    while (fabs(FG - LastFG)>Eps)
    {
        if (FindG(n,ne,T,Left)*FindG(n,ne,T,Rigth) <= 0)
            Rigth = Center;
        else
            Left = Center;
        Center = (Left+Rigth)/2;
        LastFG = FG;
        FG = FindG(n,ne,T,Center);
    }
    return Center;
}

void Start(double *n,double *K,double *ne, double P)
{
    double T = 2000;
    double X1,X2;
    n[3] = exp(-100);
    n[2] = exp(-100);
    X1 = K[0]*(-1-sqrt((7242*P)/(T*K[0])));
    X2 = K[0]*(-1+sqrt((7242*P)/(T*K[0])));
    if (X1 >= 0)
        n[1] = X1;
    else
        n[1] = X2;
    n[0] = pow(n[1],2)/K[0];
    (*ne) = n[1];
    return;
}

void SystemA(double *n,double *ne,double *K,double alpha,double T,double P)
{
    double Max = 1000;
    double V;
    double X[4];
    double Vprev = log((*ne));
    double Xprev[4];
    for(int I = 0; I<4; I++)
        Xprev[I] = log(n[I]);
//    for (int i = 0; i < 4; i++)
//        printf("%20.20lf",Xprev[i]);

    printf("\n\n");
    double A[5][6];
    double Res[5];
    double Work;
    double Eps = pow(10,-4);
    while (Max>=Eps)
    {
        InstallMatrix(A,K,Xprev,Vprev,alpha,P,T);
//        for (int i = 0; i < 5; i++)
//        {
//            for(int j = 0; j < 6; j++)
//                printf("%20.20lf ", A[i][j]);
//            printf("\n");
//        }

        printf("\n\n");
        Gauss(A,Res,5);
        for (int I = 0; I < 4; I++)
            X[I] = Res[I]+Xprev[I];
        V = Res[4] + Vprev;
        Max = fabs((exp(V)-exp(Vprev))/exp(V));
        for (int I = 0; I<4; I++)
        {
            Work = fabs((exp(X[I])-exp(Xprev[I]))/exp(X[I]));
            if (Work>Max)
                Max = Work;
        }
        for (int I = 0;I<4; I++)
            Xprev[I] = X[I];
        Vprev = V;
    }
    (*ne) = 0;
    for (int I = 0; I<4; I++)
    {
        n[I] = exp(X[I]);
        (*ne) += n[I]*Zi(I);
    }
    return;
}

void ResolveSystem(double *n,double *ne,double *Delta, double T, double P, double Eps)
{
    double K[3];

    double alpha;
    double DE[4];
    double G = 0;
    SystemBCD(G,DE,K,&alpha,T); // OK
    Start(n,K,ne,P);    // OK

//    for (int i = 0; i < 4; i++)
//    {
//            printf("%120.120lf ", K[i]);
//        printf("\n");
//    }

//    for (int i = 0; i < 4; i++)
//    {
//            printf("%120.120lf ", n[i]);
//        printf("\n");
//    }

    SystemA(n,ne,K,alpha,T,P);
    for (int i = 0; i < 4; i++)
        printf("%120.120lf ", n[i]);

    G = ResultG(Eps,T,(*ne),n);
    SystemBCD(G,DE,K,&alpha,T);


    Start(n,K,ne,P);
    SystemA(n,ne,K,alpha,T,P);
    for (int I = 0; I<4;I++)
        n[I] *= pow(10,18);
    (*ne) *=  pow(10,18);
    (*Delta) = (*ne);
    for (int I = 0; I<4; I++)
        (*Delta) -= Zi(I)*n[I];
    (*Delta)= fabs((*Delta));
    return;
}
