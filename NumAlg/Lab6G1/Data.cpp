#include "Data.h"

void InitMatrix(double Matrix[13][4],int *Flag)
{
    FILE *F = fopen("Qi.txt","r");
    for (int I=0; I<13;I++)
        for(int J = 0;J<4;J++)
            fscanf(F,"%lf",&Matrix[I][J]);
    fclose(F);
    (*Flag) = 1;
    return;
}

double Ei(int I)
{
    switch (I)
    {
        case 0: return 12.13;
        case 1: return 20.98;
        case 2: return 31;
        case 3: return 45;
        default: return -1;
    }
}

double Qi(double T, int I)
{
    static int Flag = 0;
    static double Matrix[13][4];
    if (Flag == 0)
        InitMatrix(Matrix,&Flag);
    int IndexT = T/2000 - 1;
    return Matrix[IndexT][I];
}

int Zi(int I)
{
    switch (I)
    {
        case 0:return 0;
        case 1:return 1;
        case 2:return 2;
        case 3:return 3;
        default:
            return -1;
    }
}
