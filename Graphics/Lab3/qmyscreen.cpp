#include "qmyscreen.h"
#include <QPainter>
#include <QMessageBox>

QMyScreen::QMyScreen(QWidget *parent) : QWidget(parent)
{
    this->img = 0;
    this->line_color = Qt::black;
    this->background_color = Qt::white;
}

QMyScreen::~QMyScreen()
{
    delete this->img;
}

void QMyScreen::paintEvent(QPaintEvent *)
{
    if(this->img != 0)
    {
        QPainter p(this);
        p.drawImage(0,0,*this->img);
    }
    else
    {
        this->img = new QImage(this->maximumSize(),QImage::Format_RGB32);
        this->img->fill(this->background_color);
        this->repaint();
    }
}

void QMyScreen::drawLineStandard(int x_start, int y_start, int x_fin, int y_fin)
{
    this->img->fill(this->background_color);
    QPainter p(this->img);
    p.drawLine(x_start,y_start,x_fin,y_fin);
    this->repaint();
}
