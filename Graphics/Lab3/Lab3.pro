#-------------------------------------------------
#
# Project created by QtCreator 2015-04-19T11:46:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qmyscreen.cpp

HEADERS  += mainwindow.h \
    qmyscreen.h

FORMS    += mainwindow.ui
