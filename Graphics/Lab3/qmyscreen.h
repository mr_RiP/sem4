#ifndef QMYSCREEN_H
#define QMYSCREEN_H

#include <QWidget>
#include <QPainter>

class QMyScreen : public QWidget
{
    Q_OBJECT
public:
    explicit QMyScreen(QWidget *parent = 0);
    ~QMyScreen();
    void setBackgroundColorRGB(int red, int green, int blue);
    void setLineColorRGB(int red, int green, int blue);
    /*
    void drawLineBresenham(int x_start, int y_start, int x_fin, int y_fin);
    void drawLineBresenham(double x_start, double y_start, double x_fin, double y_fin);
    */
    void drawLineStandard(int x_start, int y_start, int x_fin, int y_fin);

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *);
    QImage *img;
    bool line_is_set;
    QColor line_color;
    bool background_is_set;
    QColor background_color;

};

#endif // QMYSCREEN_H
