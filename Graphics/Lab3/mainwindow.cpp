#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbDraw_clicked()
{
    int x_start = ui->leXStart->text().toInt();
    int x_fin = ui->leXFin->text().toInt();
    int y_start = ui->leYStart->text().toInt();
    int y_fin = ui->leYFin->text().toInt();
    ui->msScreen->drawLineStandard(x_start,y_start,x_fin,y_fin);
}
