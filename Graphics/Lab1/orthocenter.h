#ifndef OTHOCENTER_H
#define OTHOCENTER_H

#include <QPointF>

QPointF Orthocenter(QPointF& ra, QPointF& rb, QPointF& rc);

#endif // OTHOCENTER_H

