#include "orthocenter.h"
#include <cmath>
//#include <iostream>

qreal Lab1Exercise(int &a_index, int &b_index, int &c_index, QPointF *arr, int num)
{
    a_index = 0;
    b_index = 1;
    c_index = 2;
    qreal max = 0;

    for (int i=0; i<(num-2); i++)
        for (int j=i+1; j<(num-1); j++)
            for (int k=j+1; k<num; k++)
            {
                QPointF ort = Orthocenter(arr[i],arr[j],arr[k]);
                qreal ortsum = fabs(ort.x()) + fabs(ort.y());
                if (ortsum > max)
                {
                    max = ortsum;
                    a_index = i;
                    b_index = j;
                    c_index = k;
                }
                //std::cout << i << " " << j << " " << k << " " << ortsum << std::endl;
            }

    return max;
}
