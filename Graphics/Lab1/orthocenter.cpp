#include "orthocenter.h"
#include <cmath>
//#include <iostream>

qreal VectorLen(const QPointF& a, const QPointF& b)
{
    return sqrt(pow(b.x()-a.x(),2) + pow(b.y()-a.y(),2));
}

qreal STriangle(qreal a, qreal b, qreal c)
{
    qreal p = (a + b + c)/2;
    return sqrt(p*(p-a)*(p-b)*(p-c));
}

qreal ScalarMult(const QPointF& a, const QPointF& b)
{
    return (a.x() * b.x()) + (a.y() * b.y());
}

QPointF OuterCircleO(QPointF& ra, QPointF& rb, QPointF& rc)
{
    qreal a = VectorLen(rb,rc);
    qreal b = VectorLen(ra,rc);
    qreal c = VectorLen(ra,rb);

    //std::cout << "~ " << a << " " << b << " " << c << std::endl;

    qreal S = STriangle(a,b,c);

    //std::cout << "~~ " << S << std::endl;

    S *= S * 8.0;

    a *= a;
    b *= b;
    c *= c;

    qreal alpha_a = (a/S) * ScalarMult(ra-rb,ra-rc);
    qreal alpha_b = (b/S) * ScalarMult(rb-ra,rb-rc);
    qreal alpha_c = (c/S) * ScalarMult(rc-ra,rc-rb);

    return alpha_a * ra + alpha_b * rb + alpha_c * rc;
}

QPointF Orthocenter(QPointF& ra, QPointF& rb, QPointF& rc)
{
    QPointF ro = OuterCircleO(ra,rb,rc);
    QPointF OA = ra + ro;
    QPointF OB = rb + ro;
    QPointF OC = rc + ro;
    return (OA + OB + OC) - ro;
}

