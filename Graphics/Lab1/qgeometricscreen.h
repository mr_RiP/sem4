#ifndef QGEOMETRICSCREEN_H
#define QGEOMETRICSCREEN_H

#include <QWidget>
#include <exception>

class QGeometricScreen : public QWidget
{
    Q_OBJECT
public:
    explicit QGeometricScreen(QWidget *parent = 0);
    ~QGeometricScreen();
    void setTriangle(QPointF &a, QPointF& b, QPointF& c);
    void Scale(qreal scale);
    void ScaleFit(void);
    void ScaleDefault(void);
    bool OutOfScreen(void)
    {
        return out_of_screen;
    }
    bool InvalidFigure(void);
    qreal getScale(void)
    {
        return scale;
    }

    class bad_scale : public std::exception {};

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *);

    QPointF triangle[3];
    qreal scale;
    QPointF focus;
    bool triangle_is_set;
    bool out_of_screen;
};

#endif // QGEOMETRICSCREEN_H
