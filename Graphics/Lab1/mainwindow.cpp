#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qgeometricscreen.h"
#include <QMessageBox>
#include "labexercise.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lePointX->setValidator(&validator);
    ui->lePointY->setValidator(&validator);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbCalculate_clicked()
{
    if (ui->twPoints->rowCount() < 3)
    {
        QMessageBox::critical(this,"Ошибка","Недостаточно точек для построения треугольника");
        return;
    }

    int n = ui->twPoints->rowCount();
    QPointF *arr = new QPointF[n];
    int a=0, b=1, c=2;
    for (int i=0; i<n; i++)
    {
        for (int j=0; j<2; j++)
            ui->twPoints->item(i,j)->setTextColor(Qt::black);
        arr[i].rx() = this->locale().toDouble(ui->twPoints->item(i,0)->text());
        arr[i].ry() = this->locale().toDouble(ui->twPoints->item(i,1)->text());
    }
    Lab1Exercise(a,b,c,arr,n);
    ui->gsScreen->setTriangle(arr[a],arr[b],arr[c]);
    for (int i=0; i<2; i++)
    {
        ui->twPoints->item(a,i)->setTextColor(Qt::red);
        ui->twPoints->item(b,i)->setTextColor(Qt::red);
        ui->twPoints->item(c,i)->setTextColor(Qt::red);
    }
    delete [] arr;

    ui->gsScreen->ScaleDefault();
    ui->dsbScale->setValue(1);
    if (ui->gsScreen->OutOfScreen())
        QMessageBox::warning(this,"Оповещение","Часть изображения находится за границами печати.");
    if (ui->gsScreen->InvalidFigure())
        QMessageBox::warning(this,"Оповещение","Искомая фигура является вырожденной.");
}

void MainWindow::on_dsbScale_valueChanged(double arg1)
{
    ui->gsScreen->Scale(arg1);
}

void MainWindow::on_pbAddNewPoint_clicked()
{
    if (ui->lePointX->text().isEmpty() || ui->lePointY->text().isEmpty())
    {
        QMessageBox::critical(this,"Ошибка","Для добавления точки необходимо указать обе координаты.");
        return;
    }
    int n = ui->twPoints->rowCount();
    ui->twPoints->setRowCount(n+1);
    ui->twPoints->setItem(n,0,new QTableWidgetItem(ui->lePointX->text()));
    ui->twPoints->setItem(n,1,new QTableWidgetItem(ui->lePointY->text()));
    ui->sbRowToDel->setMaximum(n+1);
    if (n == 0)
    {
        ui->sbRowToDel->setMinimum(1);
        ui->pbDel->setEnabled(true);
        ui->sbRowToDel->setEnabled(true);
        ui->pbReplace->setEnabled(true);
    }
}

void MainWindow::on_pbDel_clicked()
{
    ui->twPoints->removeRow(ui->sbRowToDel->value()-1);
    ui->sbRowToDel->setMaximum(ui->twPoints->rowCount());
    if (ui->twPoints->rowCount() == 0)
    {
        ui->pbDel->setEnabled(false);
        ui->pbReplace->setEnabled(false);
        ui->sbRowToDel->setEnabled(false);
    }
}

void MainWindow::on_twPoints_clicked(const QModelIndex &index)
{
    int selected = 0;
    for (int i=0; (i<ui->twPoints->rowCount()) && (selected == 0); i++)
        if (ui->twPoints->item(i,0)->isSelected())
        {
            selected = i+1;
            ui->sbRowToDel->setValue(selected);
        }
}

void MainWindow::on_pbReplace_clicked()
{
    if (ui->lePointX->text().isEmpty() || ui->lePointY->text().isEmpty())
    {
        QMessageBox::critical(this,"Ошибка","Для добавления точки необходимо указать обе координаты.");
        return;
    }
    int n = ui->sbRowToDel->value()-1;
    ui->twPoints->item(n,0)->setText(ui->lePointX->text());
    ui->twPoints->item(n,1)->setText(ui->lePointY->text());
}

void MainWindow::on_sbRowToDel_valueChanged(int arg1)
{
    ui->twPoints->selectRow(arg1-1);
}
