#-------------------------------------------------
#
# Project created by QtCreator 2015-04-18T22:06:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qgeometricscreen.cpp \
    orthocenter.cpp \
    labexercise.cpp

HEADERS  += mainwindow.h \
    qgeometricscreen.h \
    orthocenter.h \
    labexercise.h

FORMS    += mainwindow.ui
