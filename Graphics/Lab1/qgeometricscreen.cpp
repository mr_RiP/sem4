#include "qgeometricscreen.h"
#include <QPainter>
#include <cmath>

QGeometricScreen::QGeometricScreen(QWidget *parent) : QWidget(parent)
{
    triangle_is_set = false;
    out_of_screen = false;
    scale = 1;
    focus.setX(0);
    focus.setY(0);
}

void QGeometricScreen::setTriangle(QPointF &a, QPointF& b, QPointF& c)
{
    triangle_is_set = true;
    triangle[0] = a;
    triangle[0].ry() *= -1.0;
    triangle[1] = b;
    triangle[1].ry() *= -1.0;
    triangle[2] = c;
    triangle[2].ry() *= -1.0;

    focus.setX(0);
    focus.setY(0);
    for (int i=0; i<3; i++)
    {
        if (fabs(triangle[i].x()) > focus.x())
            focus.setX(triangle[i].x());
        if (fabs(triangle[i].y()) > focus.y())
            focus.setX(triangle[i].y());
    }
    focus /= 2;
}

void QGeometricScreen::Scale(qreal scale)
{
    if (scale <= 0)
        throw new QGeometricScreen::bad_scale;

    this->scale = scale;

    this->repaint();
}


QGeometricScreen::~QGeometricScreen()
{

}

void QGeometricScreen::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.fillRect(0,0,this->width(),this->height(),Qt::white);

    if (triangle_is_set)
    {       
        p.setPen(Qt::red);
        out_of_screen = false;
        QPointF print_p[3];
        for (int i=0; i<3; i++)
        {
            print_p[i] = (triangle[i] + focus)*scale - focus;
            if (triangle[i].x > 0)

        for (int i=0; i<3; i++)
        {
            QPointF a = print_p[i];
            QPointF b = print_p[(i+1)%3];
            p.drawLine(a,b);
            if ((fabs(a.x()) > x_len) || (fabs(a.y()) > y_len))
                out_of_screen = true;
        }
    }
}

void QGeometricScreen::ScaleFit(void)
{
    qreal max_x = triangle[0].x();
    qreal max_y = triangle[0].y();
    for (int i=1; i<3; i++)
    {
        if (fabs(triangle[i].x()) > max_x)
            max_x = triangle[i].x();
        if (fabs(triangle[i].y()) > max_y)
            max_y = triangle[i].y();
    }

    qreal x_len = (double) this->width() / 2.0;
    qreal y_len = (double) this->height() / 2.0;

    x_len /= max_x;
    y_len /= max_y;

    scale = (x_len < y_len)? y_len: x_len;

    this->repaint();
}

void QGeometricScreen::ScaleDefault(void)
{
    scale = 1;
    this->repaint();
}

bool QGeometricScreen::InvalidFigure(void)
{
    if (((triangle[0].x() == triangle[1].x()) && (triangle[2].x() == triangle[1].x())) ||
            ((triangle[0].y() == triangle[1].y()) && (triangle[2].y() == triangle[1].y())))
        return true;
    else
    {
        qreal k = (triangle[0].y() - triangle[1].y()) / (triangle[0].x() - triangle[1].x());
        qreal b = triangle[0].y() - k * triangle[0].x();
        qreal y = k * triangle[2].x() + b;
        if (y == triangle[2].y())
            return true;
        else
            return false;
    }
}
