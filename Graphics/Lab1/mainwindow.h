#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QValidator>
#include <QLocale>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbCalculate_clicked();

    void on_dsbScale_valueChanged(double arg1);

    void on_pbAddNewPoint_clicked();

    void on_pbDel_clicked();

    void on_twPoints_clicked(const QModelIndex &index);

    void on_pbReplace_clicked();

    void on_sbRowToDel_valueChanged(int arg1);

private:
    Ui::MainWindow *ui;
    QDoubleValidator validator;
};

#endif // MAINWINDOW_H
